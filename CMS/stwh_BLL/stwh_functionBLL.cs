﻿using System;
using System.Data;
using System.Collections.Generic;
using stwh_Common;
using stwh_Model;
using stwh_IDAL;
using stwh_DALFactory;

namespace stwh_BLL
{
	/// <summary>
	/// stwh_function
	/// </summary>
	public partial class stwh_functionBLL
	{
        private readonly Istwh_functionDAL dal = DataAccess.CreateIDAL("stwh_functionDAL") as Istwh_functionDAL;
		public stwh_functionBLL()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int stwh_fid)
		{
			return dal.Exists(stwh_fid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(stwh_Model.stwh_function model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(stwh_Model.stwh_function model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int stwh_fid)
		{
			
			return dal.Delete(stwh_fid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string stwh_fidlist )
		{
			return dal.DeleteList(stwh_fidlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public stwh_Model.stwh_function GetModel(int stwh_fid)
		{
            return dal.GetModel(stwh_fid) as stwh_function;
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public stwh_Model.stwh_function GetModelByCache(int stwh_fid)
		{
			
			string CacheKey = "stwh_functionModel-" + stwh_fid;
			object objModel = stwh_Common.DataCache.GetMCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(stwh_fid);
                    if (objModel != null) stwh_Common.DataCache.AddMCache(CacheKey, objModel);
				}
				catch{}
			}
			return (stwh_Model.stwh_function)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_function> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<stwh_Model.stwh_function> GetModelList(string strWhere,int flag)
        {
            DataSet ds = dal.GetList(strWhere,flag);
            return DataTableToList(ds.Tables[0]);
        }
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_function> DataTableToList(DataTable dt)
		{
			List<stwh_Model.stwh_function> modelList = new List<stwh_Model.stwh_function>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				stwh_Model.stwh_function model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = dal.DataRowToModel(dt.Rows[n]) as stwh_function;
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		#endregion  BasicMethod
	}
}

