﻿using System;
using System.Data;
using System.Collections.Generic;
using stwh_Common;
using stwh_Model;
using stwh_IDAL;
using stwh_DALFactory;

namespace stwh_BLL
{
    /// <summary>
    /// stwh_config
    /// </summary>
    public partial class stwh_configBLL
    {
        private readonly Istwh_configDAL dal = DataAccess.CreateIDAL("stwh_configDAL") as Istwh_configDAL;
        public stwh_configBLL()
        { }
        #region  BasicMethod
        /// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(stwh_config model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string stwh_cfidlist)
        {
            return dal.DeleteList(stwh_cfidlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public stwh_config GetModel()
        {
            return dal.GetModel();
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<stwh_config> DataTableToList(DataTable dt)
        {
            List<stwh_config> modelList = new List<stwh_config>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                stwh_config model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]) as stwh_config;
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }
        #endregion  BasicMethod
    }
}

