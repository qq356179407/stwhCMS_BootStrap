﻿using System;
using System.Data;
using System.Collections.Generic;
using stwh_Common;
using stwh_Model;
using stwh_IDAL;
using stwh_DALFactory;

namespace stwh_BLL
{
	/// <summary>
	/// stwh_buyuser
	/// </summary>
	public partial class stwh_buyuserBLL
	{
        private readonly Istwh_buyuserDAL dal = DataAccess.CreateIDAL("stwh_buyuserDAL") as Istwh_buyuserDAL;
		public stwh_buyuserBLL()
		{}
		#region  BasicMethod
        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount, flag);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string stwh_bumobile,int stwh_buid)
		{
			return dal.Exists(stwh_bumobile,stwh_buid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(stwh_Model.stwh_buyuser model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(stwh_Model.stwh_buyuser model)
		{
			return dal.Update(model);
		}

        /// <summary>
        /// 批量修改会员状态
        /// </summary>
        /// <param name="stwh_buidlist">会员id集合</param>
        /// <param name="stwh_bustatus">状态（0,1）</param>
        /// <returns></returns>
        public bool Update(string stwh_buidlist, int stwh_bustatus)
        {
            return dal.Update(stwh_buidlist, stwh_bustatus);
        }

        /// <summary>
        /// 修改会员登录密码
        /// </summary>
        /// <param name="stwh_bupwd">新密码</param>
        /// <param name="stwh_buid">会员ID</param>
        /// <returns></returns>
        public bool UpdatePwd(string stwh_bupwd, int stwh_buid)
        {
            return dal.UpdatePwd(stwh_bupwd, stwh_buid);
        }

        /// <summary>
        /// 修改会员登录密码(用户会员找回密码)
        /// </summary>
        /// <param name="stwh_bumobile">会员手机</param>
        /// <param name="stwh_bupwd">新密码</param>
        /// <returns></returns>
        public bool UpdatePwd(string stwh_bumobile, string stwh_bupwd)
        {
            return dal.UpdatePwd(stwh_bumobile, stwh_bupwd);
        }

        /// <summary>
        /// 修改会员手机
        /// </summary>
        /// <param name="oldphone">旧手机号码</param>
        /// <param name="newphone">新手机号码</param>
        /// <returns></returns>
        public bool UpdatePhone(string oldphone, string newphone)
        {
            return dal.UpdatePhone(oldphone, newphone);
        }

        /// <summary>
        /// 更新会员基本信息
        /// </summary>
        /// <param name="model">更新对象</param>
        /// <returns></returns>
        public bool UpdateInfo(stwh_Model.stwh_buyuser model)
        {
            return dal.UpdateInfo(model);
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int stwh_buid)
		{
			return dal.Delete(stwh_buid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string stwh_bumobile,int stwh_buid)
		{
			return dal.Delete(stwh_bumobile,stwh_buid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string stwh_buidlist )
		{
			return dal.DeleteList(stwh_buidlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public stwh_Model.stwh_buyuser GetModel(int stwh_buid)
		{
            return dal.GetModel(stwh_buid) as stwh_buyuser;
		}

        /// <summary>
        /// 根据手机号码和密码获取对象
        /// </summary>
        /// <param name="stwh_bumobile">手机号码</param>
        /// <param name="stwh_bupwd">登录密码</param>
        /// <returns></returns>
        public stwh_Model.stwh_buyuser GetModel(string stwh_bumobile, string stwh_bupwd)
        {
            return dal.GetModel(stwh_bumobile, stwh_bupwd);
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public stwh_Model.stwh_buyuser GetModelByCache(int stwh_buid)
		{
			
			string CacheKey = "stwh_buyuserModel-" + stwh_buid;
			object objModel = stwh_Common.DataCache.GetMCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(stwh_buid);
                    if (objModel != null) stwh_Common.DataCache.AddMCache(CacheKey, objModel);
				}
				catch{}
			}
			return (stwh_Model.stwh_buyuser)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_buyuser> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_buyuser> DataTableToList(DataTable dt)
		{
			List<stwh_Model.stwh_buyuser> modelList = new List<stwh_Model.stwh_buyuser>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				stwh_Model.stwh_buyuser model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = dal.DataRowToModel(dt.Rows[n]) as stwh_buyuser;
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		#endregion  BasicMethod
	}
}

