﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_questions接口层
    /// </summary>
    public interface Istwh_questionsDAL : IBaseDAL
    {
         /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <param name="isview">是否查询视图（默认false，不查询视图，true查询视图）</param>
        /// <returns></returns>
        DataSet GetList(string strWhere, bool isview);
    }
}
