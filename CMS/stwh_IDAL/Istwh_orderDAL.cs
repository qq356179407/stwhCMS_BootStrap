﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_order接口层
    /// </summary>
    public interface Istwh_orderDAL : IBaseDAL
    {
        /// <summary>
        /// 获取会员最新订单商品总价
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <returns></returns>
        decimal GetOrderTotalPrice(int stwh_buid, ref string stwh_orddid);

        /// <summary>
        /// 根据订单编号获取会员订单商品总价
        /// </summary>
        /// <param name="stwh_orddid">订单编号</param>
        /// <returns></returns>
        decimal GetOrderTotalPrice(string stwh_orddid);

        /// <summary>
        /// 分页获取会员订单信息和订单详细信息
        /// </summary>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示多少条</param>
        /// <param name="pageNumber">页码</param>
        /// <returns></returns>
        DataSet GetOrder(string If, int pageSize, int pageNumber);

        /// <summary>
        /// 更新订单支付状态
        /// </summary>
        /// <param name="stwh_orddid">订单编号</param>
        /// <param name="stwh_orstatus">订单状态</param>
        /// <returns></returns>
        bool Update(string stwh_orddid, int stwh_orstatus);

        /// <summary>
        /// 根据订单单号删除订单
        /// </summary>
        /// <param name="stwh_orddid">订单编号</param>
        /// <returns></returns>
        bool Delete(string stwh_orddid);
    }
}
