﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_files接口层
    /// </summary>
    public interface Istwh_filesDAL : IBaseDAL
    {
        /// <summary>
        /// 批量修改文件审核状态
        /// </summary>
        /// <param name="stwh_flidlist">文件id</param>
        /// <param name="stwh_flissh">状态（0,1）</param>
        /// <returns></returns>
        bool Update(string stwh_flidlist, int stwh_flissh);
    }
}
