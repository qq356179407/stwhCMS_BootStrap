﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using stwh_Model;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_buyuser接口层
    /// </summary>
    public interface Istwh_buyuserDAL : IBaseDAL
    {
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        bool Exists(string stwh_bumobile, int stwh_buid);

        /// <summary>
        /// 批量修改会员状态
        /// </summary>
        /// <param name="stwh_buidlist">会员id</param>
        /// <param name="stwh_bustatus">状态（0,1）</param>
        /// <returns></returns>
        bool Update(string stwh_buidlist, int stwh_bustatus);

        /// <summary>
        /// 修改会员登录密码
        /// </summary>
        /// <param name="stwh_bupwd">新密码</param>
        /// <param name="stwh_buid">会员ID</param>
        /// <returns></returns>
        bool UpdatePwd(string stwh_bupwd, int stwh_buid);

        /// <summary>
        /// 修改会员登录密码(用户会员找回密码)
        /// </summary>
        /// <param name="stwh_bumobile">会员手机</param>
        /// <param name="stwh_bupwd">新密码</param>
        /// <returns></returns>
        bool UpdatePwd(string stwh_bumobile, string stwh_bupwd);

        /// <summary>
        /// 修改会员手机
        /// </summary>
        /// <param name="oldphone">旧手机号码</param>
        /// <param name="newphone">新手机号码</param>
        /// <returns></returns>
        bool UpdatePhone(string oldphone, string newphone);

        /// <summary>
        /// 更新会员基本信息
        /// </summary>
        /// <param name="model">更新对象</param>
        /// <returns></returns>
        bool UpdateInfo(stwh_buyuser model);

        /// <summary>
        /// 根据手机号码删除数据
        /// </summary>
        /// <param name="stwh_bumobile"></param>
        /// <param name="stwh_buid"></param>
        /// <returns></returns>
        bool Delete(string stwh_bumobile, int stwh_buid);

        /// <summary>
        /// 根据手机号码和密码获取对象（启用）
        /// </summary>
        /// <param name="stwh_bumobile">手机号码</param>
        /// <param name="stwh_bupwd">登录密码</param>
        /// <returns></returns>
        stwh_buyuser GetModel(string stwh_bumobile, string stwh_bupwd);
    }
}
