﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_producttype_pv接口层
    /// </summary>
    public interface Istwh_producttype_pvDAL : IBaseDAL
    {
        /// <summary>
        /// 删除一条数据
        /// </summary>
        bool Delete(int id, int flag);
    }
}
