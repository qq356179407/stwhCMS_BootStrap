﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using stwh_Model;
using System.Data;

namespace stwh_IDAL
{
    /// <summary>
    /// 接口层基类
    /// </summary>
    public interface IBaseDAL
    {
        /// <summary>
        /// 得到最大ID
        /// </summary>
        int GetMaxId();

        /// <summary>
        /// 根据id查询数据是否存在
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Exists(int id);

        /// <summary>
        /// 新增一条数据
        /// </summary>
        /// <param name="model">待添加数据对象</param>
        /// <returns></returns>
        int Add(BaseModel model);

        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <param name="model">待更新数据对象</param>
        /// <returns></returns>
        bool Update(BaseModel model);

        /// <summary>
        /// 根据id删除一条数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int id);

        /// <summary>
        /// 根据id集合删除数据（id格式：1,2,3,4,4）
        /// </summary>
        /// <param name="idlist">id集合（id格式：1,2,3,4,4）</param>
        /// <returns></returns>
        bool DeleteList(string idlist);

        /// <summary>
        /// 根据id获取数据对象
        /// </summary>
        /// <param name="id">数据对象的id</param>
        /// <returns></returns>
        BaseModel GetModel(int id);

        /// <summary>
        /// 将DataRow转换为数据对象
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        BaseModel DataRowToModel(DataRow row);

        /// <summary>
        /// 根据where条件获取数据列表
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        DataSet GetList(string where);

        /// <summary>
        /// 根据where条件获取前几条数据
        /// </summary>
        /// <param name="Top">前几条数据</param>
        /// <param name="strWhere">查询条件</param>
        /// <param name="filedOrder">排序字段</param>
        /// <returns></returns>
        DataSet GetList(int Top, string strWhere, string filedOrder);

        /// <summary>
        /// 根据where条件获取查询记录总数
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        int GetRecordCount(string strWhere);
        
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <param name="orderby">排序字段</param>
        /// <param name="startIndex">开始索引</param>
        /// <param name="endIndex">结束索引</param>
        /// <returns></returns>
        DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex);

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount);

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag);
    }
}
