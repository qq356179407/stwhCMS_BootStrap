﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_product接口层
    /// </summary>
    public interface Istwh_productDAL : IBaseDAL
    {
        /// <summary>
        /// 批量修改数据审核状态
        /// </summary>
        /// <param name="stwh_pidlist">id集合</param>
        /// <param name="stwh_pissh">状态（0,1）</param>
        /// <returns></returns>
        bool Update(string stwh_pidlist, int stwh_pissh);
    }
}
