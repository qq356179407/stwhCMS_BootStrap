﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using stwh_Model;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_config接口层
    /// </summary>
    public interface Istwh_configDAL : IBaseDAL
    {
        /// <summary>
        /// 增加一条数据
        /// </summary>
        bool Add(stwh_config model);

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        stwh_config GetModel();
    }
}
