﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using stwh_Model;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_userinfo接口层
    /// </summary>
    public interface Istwh_userinfoDAL : IBaseDAL
    {
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        /// <param name="stwh_uilname">登录名</param>
        /// <param name="stwh_uipwd">登录密码</param>
        /// <returns></returns>
        stwh_userinfo GetModel(string stwh_uilname, string stwh_uipwd);
    }
}
