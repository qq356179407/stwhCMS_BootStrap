﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_staff接口层
    /// </summary>
    public interface Istwh_staffDAL : IBaseDAL
    {
        /// <summary>
        /// 修改员工状态
        /// </summary>
        /// <param name="flag">0 转正状态，1签订合同状态，2离职状态</param>
        /// <param name="status">值为0 | 1</param>
        /// <param name="sidlist">员工id</param>
        /// <returns></returns>
        bool Update(int flag, int status, string sidlist);
    }
}
