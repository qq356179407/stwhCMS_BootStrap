﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_menuinfo接口层
    /// </summary>
    public interface Istwh_menuinfoDAL : IBaseDAL
    {
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetList(int stwh_rid);
    }
}
