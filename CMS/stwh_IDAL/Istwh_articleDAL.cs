﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_article接口层
    /// </summary>
    public interface Istwh_articleDAL : IBaseDAL
    {
        /// <summary>
        /// 批量修改文章审核状态
        /// </summary>
        /// <param name="stwh_atidlist">文章id</param>
        /// <param name="stwh_atissh">状态（0,1）</param>
        /// <returns></returns>
        bool Update(string stwh_atidlist, int stwh_atissh);
    }
}
