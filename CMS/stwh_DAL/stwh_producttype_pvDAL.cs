﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_producttype_pv
    /// </summary>
    public partial class stwh_producttype_pvDAL : BaseDAL, Istwh_producttype_pvDAL
    {
        public stwh_producttype_pvDAL()
        { }
        #region Istwh_producttype_pvDAL接口实现方法
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id, int flag)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_producttype_pv ");
            int rows = 0;
            if (flag == 0)
            {
                strSql.Append(" where stwh_ptpvid=@stwh_ptpvid");
                SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptpvid", SqlDbType.Int,4)
			    };
                parameters[0].Value = id;

                rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            }
            else
            {
                strSql.Append(" where stwh_pid=@stwh_pid");
                SqlParameter[] parameters = {
					new SqlParameter("@stwh_pid", SqlDbType.Int,4)
			    };
                parameters[0].Value = id;

                rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            }
            if (rows > 0) return true;
            else return false;
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_producttype_pv");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_producttype_pv", "stwh_ptpvid", FieldColumn, FieldOrder, "stwh_ptpvid,stwh_pid,stwh_ptpid,stwh_ptpvvalue", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_producttype_pv where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_producttype_pv", "stwh_ptpvid", FieldColumn, FieldOrder, "stwh_ptpvid,stwh_pid,stwh_ptpid,stwh_ptpvvalue", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_ptpvid", "stwh_producttype_pv");
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_ptpvid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_producttype_pv");
            strSql.Append(" where stwh_ptpvid=@stwh_ptpvid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptpvid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptpvid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_producttype_pv jbmodel = model as stwh_producttype_pv;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_producttype_pv(");
            strSql.Append("stwh_pid,stwh_ptpid,stwh_ptpvvalue)");
            strSql.Append(" values (");
            strSql.Append("@stwh_pid,@stwh_ptpid,@stwh_ptpvvalue)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pid", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptpid", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptpvvalue", SqlDbType.NText)};
            parameters[0].Value = jbmodel.stwh_pid;
            parameters[1].Value = jbmodel.stwh_ptpid;
            parameters[2].Value = jbmodel.stwh_ptpvvalue;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_producttype_pv jbmodel = model as stwh_producttype_pv;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_producttype_pv set ");
            strSql.Append("stwh_pid=@stwh_pid,");
            strSql.Append("stwh_ptpid=@stwh_ptpid,");
            strSql.Append("stwh_ptpvvalue=@stwh_ptpvvalue");
            strSql.Append(" where stwh_ptpvid=@stwh_ptpvid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pid", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptpid", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptpvvalue", SqlDbType.NText),
					new SqlParameter("@stwh_ptpvid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_pid;
            parameters[1].Value = jbmodel.stwh_ptpid;
            parameters[2].Value = jbmodel.stwh_ptpvvalue;
            parameters[3].Value = jbmodel.stwh_ptpvid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_ptpvidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_producttype_pv ");
            strSql.Append(" where stwh_ptpvid in (" + stwh_ptpvidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_ptpvid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_ptpvid,stwh_pid,stwh_ptpid,stwh_ptpvvalue from stwh_producttype_pv ");
            strSql.Append(" where stwh_ptpvid=@stwh_ptpvid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptpvid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptpvid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_producttype_pv jbmodel = new stwh_producttype_pv();
            if (row != null)
            {
                if (row["stwh_ptpvid"] != null)
                {
                    jbmodel.stwh_ptpvid = int.Parse(row["stwh_ptpvid"].ToString());
                }
                if (row["stwh_pid"] != null)
                {
                    jbmodel.stwh_pid = int.Parse(row["stwh_pid"].ToString());
                }
                if (row["stwh_ptpid"] != null)
                {
                    jbmodel.stwh_ptpid = int.Parse(row["stwh_ptpid"].ToString());
                }
                if (row["stwh_ptpvvalue"] != null)
                {
                    jbmodel.stwh_ptpvvalue = row["stwh_ptpvvalue"].ToString();
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_ptpvid,stwh_pid,stwh_ptpid,stwh_ptpvvalue ");
            strSql.Append(" FROM stwh_producttype_pv ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_ptpvid,stwh_pid,stwh_ptpid,stwh_ptpvvalue ");
            strSql.Append(" FROM stwh_producttype_pv ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_producttype_pv ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_ptpvid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_producttype_pv T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

