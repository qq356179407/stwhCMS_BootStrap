﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_producttype_p
    /// </summary>
    public partial class stwh_producttype_pDAL : BaseDAL, Istwh_producttype_pDAL
    {
        public stwh_producttype_pDAL()
        { }
        #region Istwh_producttype_pDAL接口实现方法
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public stwh_producttype_p GetModel(int stwh_ptpid, int flag)
        {
            StringBuilder strSql = new StringBuilder();
            if (flag == 0) strSql.Append("select  top 1 stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_pid,stwh_ptpvid,stwh_ptpvvalue from view_productpv ");
            else strSql.Append("select  top 1 stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptorder,stwh_ptparentid from view_productptype ");
            strSql.Append(" where stwh_ptpid=@stwh_ptpid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptpid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptpid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]) as stwh_producttype_p;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere, int flag)
        {
            StringBuilder strSql = new StringBuilder();
            if (flag == 0)
            {
                strSql.Append("select stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_pid,stwh_ptpvid,stwh_ptpvvalue ");
                strSql.Append(" FROM view_productpv ");
            }
            else
            {
                strSql.Append("select stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptorder,stwh_ptparentid ");
                strSql.Append(" FROM view_productptype ");
            }
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_productpv");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_productpv", "stwh_ptpid", FieldColumn, FieldOrder, "stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_pid,stwh_ptpvid,stwh_ptpvvalue", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            if (flag == 0)
            {
                object obj = DbHelperSQL.GetSingle("select count(1) from view_productpv where " + If);
                d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
                return DbHelperSQL.PageData("view_productpv", "stwh_ptpid", FieldColumn, FieldOrder, "stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_pid,stwh_ptpvid,stwh_ptpvvalue", If, pageSize, pageNumber, ref selectCount);
            }
            else
            {
                object obj = DbHelperSQL.GetSingle("select count(1) from view_productptype where " + If);
                d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
                return DbHelperSQL.PageData("view_productptype", "stwh_ptpid", FieldColumn, FieldOrder, "stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptorder,stwh_ptparentid", If, pageSize, pageNumber, ref selectCount);
            }
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_ptpid", "stwh_producttype_p");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_ptpid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_producttype_p");
            strSql.Append(" where stwh_ptpid=@stwh_ptpid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptpid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptpid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        public BaseModel GetModel(int id)
        {
            throw new NotImplementedException();
        }

        public DataSet GetList(string where)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_producttype_p jbmodel = model as stwh_producttype_p;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_producttype_p(");
            strSql.Append("stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_ptid,@stwh_ptptitle,@stwh_ptpname,@stwh_ptptype,@stwh_ptporder)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptptitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptpname", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptptype", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptporder", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ptid;
            parameters[1].Value = jbmodel.stwh_ptptitle;
            parameters[2].Value = jbmodel.stwh_ptpname;
            parameters[3].Value = jbmodel.stwh_ptptype;
            parameters[4].Value = jbmodel.stwh_ptporder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_producttype_p jbmodel = model as stwh_producttype_p;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_producttype_p set ");
            strSql.Append("stwh_ptid=@stwh_ptid,");
            strSql.Append("stwh_ptptitle=@stwh_ptptitle,");
            strSql.Append("stwh_ptpname=@stwh_ptpname,");
            strSql.Append("stwh_ptptype=@stwh_ptptype,");
            strSql.Append("stwh_ptporder=@stwh_ptporder");
            strSql.Append(" where stwh_ptpid=@stwh_ptpid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptptitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptpname", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptptype", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptporder", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptpid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ptid;
            parameters[1].Value = jbmodel.stwh_ptptitle;
            parameters[2].Value = jbmodel.stwh_ptpname;
            parameters[3].Value = jbmodel.stwh_ptptype;
            parameters[4].Value = jbmodel.stwh_ptporder;
            parameters[5].Value = jbmodel.stwh_ptpid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_ptpid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_producttype_p ");
            strSql.Append(" where stwh_ptpid=@stwh_ptpid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptpid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptpid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_ptpidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_producttype_p ");
            strSql.Append(" where stwh_ptpid in (" + stwh_ptpidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_producttype_p jbmodel = new stwh_producttype_p();
            if (row != null)
            {
                if (row["stwh_ptpid"] != null)
                {
                    jbmodel.stwh_ptpid = int.Parse(row["stwh_ptpid"].ToString());
                }
                if (row["stwh_ptid"] != null)
                {
                    jbmodel.stwh_ptid = int.Parse(row["stwh_ptid"].ToString());
                }
                if (row["stwh_ptptitle"] != null)
                {
                    jbmodel.stwh_ptptitle = row["stwh_ptptitle"].ToString();
                }
                if (row["stwh_ptpname"] != null)
                {
                    jbmodel.stwh_ptpname = row["stwh_ptpname"].ToString();
                }
                if (row["stwh_ptptype"] != null)
                {
                    jbmodel.stwh_ptptype = row["stwh_ptptype"].ToString();
                }
                if (row["stwh_ptporder"] != null)
                {
                    jbmodel.stwh_ptporder = int.Parse(row["stwh_ptporder"].ToString());
                }
                if (row.ItemArray.Length > 6)
                {
                    #region pand
                    //if (row.Table.Columns["stwh_pid"] != null)
                    //{
                    //    jbmodel.stwh_pid = int.Parse(row["stwh_pid"].ToString());
                    //}
                    //if (row.Table.Columns["stwh_ptpvid"] != null)
                    //{
                    //    jbmodel.stwh_ptpvid = int.Parse(row["stwh_ptpvid"].ToString());
                    //}
                    //if (row.Table.Columns["stwh_ptpvvalue"] != null)
                    //{
                    //    jbmodel.stwh_ptpvvalue = row["stwh_ptpvvalue"].ToString();
                    //}
                    //if (row.Table.Columns["stwh_ptname"] != null)
                    //{
                    //    jbmodel.stwh_ptname = row["stwh_ptname"].ToString();
                    //}
                    //if (row.Table.Columns["stwh_ptdescription"] != null)
                    //{
                    //    jbmodel.stwh_ptdescription = row["stwh_ptdescription"].ToString();
                    //}
                    //if (row.Table.Columns["stwh_ptdetails"] != null)
                    //{
                    //    jbmodel.stwh_ptdetails = row["stwh_ptdetails"].ToString();
                    //}
                    //if (row.Table.Columns["stwh_ptimg"] != null)
                    //{
                    //    jbmodel.stwh_ptimg = row["stwh_ptimg"].ToString();
                    //}
                    //if (row.Table.Columns["stwh_ptshowmenu"] != null)
                    //{
                    //    jbmodel.stwh_ptshowmenu = int.Parse(row["stwh_ptshowmenu"].ToString());
                    //}
                    //if (row.Table.Columns["stwh_ptparentid"] != null)
                    //{
                    //    jbmodel.stwh_ptparentid = int.Parse(row["stwh_ptparentid"].ToString());
                    //}
                    //if (row.Table.Columns["stwh_ptorder"] != null)
                    //{
                    //    jbmodel.stwh_ptorder = int.Parse(row["stwh_ptorder"].ToString());
                    //}
                    #endregion

                    if (row.ItemArray.Length <= 9)
                    {
                        if (row["stwh_pid"] != null)
                        {
                            jbmodel.stwh_pid = int.Parse(row["stwh_pid"].ToString());
                        }
                        if (row["stwh_ptpvid"] != null)
                        {
                            jbmodel.stwh_ptpvid = int.Parse(row["stwh_ptpvid"].ToString());
                        }
                        if (row["stwh_ptpvvalue"] != null)
                        {
                            jbmodel.stwh_ptpvvalue = row["stwh_ptpvvalue"].ToString();
                        }
                    }
                    else
                    {
                        if (row["stwh_ptname"] != null)
                        {
                            jbmodel.stwh_ptname = row["stwh_ptname"].ToString();
                        }
                        if (row["stwh_ptdescription"] != null)
                        {
                            jbmodel.stwh_ptdescription = row["stwh_ptdescription"].ToString();
                        }
                        if (row["stwh_ptdetails"] != null)
                        {
                            jbmodel.stwh_ptdetails = row["stwh_ptdetails"].ToString();
                        }
                        if (row["stwh_ptimg"] != null)
                        {
                            jbmodel.stwh_ptimg = row["stwh_ptimg"].ToString();
                        }
                        if (row["stwh_ptshowmenu"] != null)
                        {
                            jbmodel.stwh_ptshowmenu = int.Parse(row["stwh_ptshowmenu"].ToString());
                        }
                        if (row["stwh_ptparentid"] != null)
                        {
                            jbmodel.stwh_ptparentid = int.Parse(row["stwh_ptparentid"].ToString());
                        }
                        if (row["stwh_ptorder"] != null)
                        {
                            jbmodel.stwh_ptorder = int.Parse(row["stwh_ptorder"].ToString());
                        }
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_ptpid,stwh_ptid,stwh_ptptitle,stwh_ptpname,stwh_ptptype,stwh_ptporder,stwh_pid,stwh_ptpvid,stwh_ptpvvalue ");
            strSql.Append(" FROM view_productpv ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_productpv ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_ptpid desc");
            }
            strSql.Append(")AS Row, T.*  from view_productpv T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

