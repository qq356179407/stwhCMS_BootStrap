﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_questions
    /// </summary>
    public partial class stwh_questionsDAL : BaseDAL, Istwh_questionsDAL
    {
        public stwh_questionsDAL()
        { }
        #region Istwh_questionsDAL接口实现方法
        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <param name="isview">是否查询视图（默认false，不查询视图，true查询视图）</param>
        /// <returns></returns>
        public DataSet GetList(string strWhere, bool isview)
        {
            if (isview) return GetList(strWhere);
            else
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select stwh_quid,stwh_wxuid,stwh_qutid,stwh_quorder,stwh_qutitle,stwh_qumiaoshu,stwh_qutype,stwh_qufenshu,stwh_qudaan,stwh_quaddtime ");
                strSql.Append(" FROM stwh_questions ");
                if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
                return DbHelperSQL.Query(strSql.ToString());
            }
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_questions");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_questions", "stwh_quid", FieldColumn, FieldOrder, "stwh_qutid,stwh_qutname,stwh_quid,stwh_quorder,stwh_qumiaoshu,stwh_qutype,stwh_qufenshu,stwh_qudaan,stwh_quaddtime,stwh_qutitle", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_questions where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_questions", "stwh_quid", FieldColumn, FieldOrder, "stwh_qutid,stwh_qutname,stwh_quid,stwh_quorder,stwh_qumiaoshu,stwh_qutype,stwh_qufenshu,stwh_qudaan,stwh_quaddtime,stwh_qutitle", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_quid", "stwh_questions");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_quid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_questions");
            strSql.Append(" where stwh_quid=@stwh_quid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_quid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_quid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_questions jbmodel = model as stwh_questions;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_questions(");
            strSql.Append("stwh_qutid,stwh_quorder,stwh_qumiaoshu,stwh_qutype,stwh_qufenshu,stwh_qudaan,stwh_quaddtime,stwh_qutitle)");
            strSql.Append(" values (");
            strSql.Append("@stwh_qutid,@stwh_quorder,@stwh_qumiaoshu,@stwh_qutype,@stwh_qufenshu,@stwh_qudaan,@stwh_quaddtime,@stwh_qutitle)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_qutid", SqlDbType.Int,4),
					new SqlParameter("@stwh_quorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_qumiaoshu", SqlDbType.NText),
					new SqlParameter("@stwh_qutype", SqlDbType.Int,4),
					new SqlParameter("@stwh_qufenshu", SqlDbType.Int,4),
					new SqlParameter("@stwh_qudaan", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_quaddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_qutitle", SqlDbType.NVarChar,500)};
            parameters[0].Value = jbmodel.stwh_qutid;
            parameters[1].Value = jbmodel.stwh_quorder;
            parameters[2].Value = jbmodel.stwh_qumiaoshu;
            parameters[3].Value = jbmodel.stwh_qutype;
            parameters[4].Value = jbmodel.stwh_qufenshu;
            parameters[5].Value = jbmodel.stwh_qudaan;
            parameters[6].Value = jbmodel.stwh_quaddtime;
            parameters[7].Value = jbmodel.stwh_qutitle;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_questions jbmodel = model as stwh_questions;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_questions set ");
            strSql.Append("stwh_qutid=@stwh_qutid,");
            strSql.Append("stwh_quorder=@stwh_quorder,");
            strSql.Append("stwh_qumiaoshu=@stwh_qumiaoshu,");
            strSql.Append("stwh_qutype=@stwh_qutype,");
            strSql.Append("stwh_qufenshu=@stwh_qufenshu,");
            strSql.Append("stwh_qudaan=@stwh_qudaan,");
            strSql.Append("stwh_quaddtime=@stwh_quaddtime,");
            strSql.Append("stwh_qutitle=@stwh_qutitle");
            strSql.Append(" where stwh_quid=@stwh_quid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_qutid", SqlDbType.Int,4),
					new SqlParameter("@stwh_quorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_qumiaoshu", SqlDbType.NText),
					new SqlParameter("@stwh_qutype", SqlDbType.Int,4),
					new SqlParameter("@stwh_qufenshu", SqlDbType.Int,4),
					new SqlParameter("@stwh_qudaan", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_quaddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_qutitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_quid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_qutid;
            parameters[1].Value = jbmodel.stwh_quorder;
            parameters[2].Value = jbmodel.stwh_qumiaoshu;
            parameters[3].Value = jbmodel.stwh_qutype;
            parameters[4].Value = jbmodel.stwh_qufenshu;
            parameters[5].Value = jbmodel.stwh_qudaan;
            parameters[6].Value = jbmodel.stwh_quaddtime;
            parameters[7].Value = jbmodel.stwh_qutitle;
            parameters[8].Value = jbmodel.stwh_quid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_quid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_questions ");
            strSql.Append(" where stwh_quid=@stwh_quid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_quid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_quid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_quidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_questionslist ");
            strSql.Append(" where stwh_quid in (" + stwh_quidlist + ");");
            strSql.Append("delete from stwh_questions ");
            strSql.Append(" where stwh_quid in (" + stwh_quidlist + ");");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_quid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_qutid,stwh_qutname,stwh_quid,stwh_quorder,stwh_qumiaoshu,stwh_qutype,stwh_qufenshu,stwh_qudaan,stwh_quaddtime,stwh_qutitle from view_questions ");
            strSql.Append(" where stwh_quid=@stwh_quid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_quid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_quid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_questions jbmodel = new stwh_questions();
            if (row != null)
            {
                if (row["stwh_quid"] != null)
                {
                    jbmodel.stwh_quid = int.Parse(row["stwh_quid"].ToString());
                }
                if (row["stwh_qutid"] != null)
                {
                    jbmodel.stwh_qutid = int.Parse(row["stwh_qutid"].ToString());
                }
                if (row["stwh_quorder"] != null)
                {
                    jbmodel.stwh_quorder = int.Parse(row["stwh_quorder"].ToString());
                }
                if (row["stwh_qumiaoshu"] != null)
                {
                    jbmodel.stwh_qumiaoshu = row["stwh_qumiaoshu"].ToString();
                }
                if (row["stwh_qutype"] != null)
                {
                    jbmodel.stwh_qutype = int.Parse(row["stwh_qutype"].ToString());
                }
                if (row["stwh_qufenshu"] != null)
                {
                    jbmodel.stwh_qufenshu = int.Parse(row["stwh_qufenshu"].ToString());
                }
                if (row["stwh_qudaan"] != null)
                {
                    jbmodel.stwh_qudaan = row["stwh_qudaan"].ToString();
                }
                if (row["stwh_qutitle"] != null)
                {
                    jbmodel.stwh_qutitle = row["stwh_qutitle"].ToString();
                }
                if (row["stwh_quaddtime"] != null)
                {
                    jbmodel.stwh_quaddtime = DateTime.Parse(row["stwh_quaddtime"].ToString());
                }
                if (row.ItemArray.Length > 9)
                {
                    if (row["stwh_qutid"] != null)
                    {
                        jbmodel.stwh_qutid = int.Parse(row["stwh_qutid"].ToString());
                    }
                    if (row["stwh_qutname"] != null)
                    {
                        jbmodel.stwh_qutname = row["stwh_qutname"].ToString();
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_qutid,stwh_qutname,stwh_quid,stwh_quorder,stwh_qumiaoshu,stwh_qutype,stwh_qufenshu,stwh_qudaan,stwh_quaddtime,stwh_qutitle ");
            strSql.Append(" FROM view_questions ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_qutid,stwh_qutname,stwh_quid,stwh_quorder,stwh_qumiaoshu,stwh_qutype,stwh_qufenshu,stwh_qudaan,stwh_quaddtime,stwh_qutitle ");
            strSql.Append(" FROM view_questions ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_questions ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_quid desc");
            }
            strSql.Append(")AS Row, T.*  from view_questions T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

