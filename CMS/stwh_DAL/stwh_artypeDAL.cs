﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_artype
    /// </summary>
    public partial class stwh_artypeDAL : BaseDAL, Istwh_artypeDAL
    {
        public stwh_artypeDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_artype");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_artype", "stwh_artid", FieldColumn, FieldOrder, "stwh_artid,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_artorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_artype where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_artype", "stwh_artid", FieldColumn, FieldOrder, "stwh_artid,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_artorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_artid", "stwh_artype");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_artid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_artype");
            strSql.Append(" where stwh_artid=@stwh_artid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_artid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_artid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_artype jbmodel = model as stwh_artype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_artype where stwh_artname = '" + jbmodel.stwh_artname + "') = 0 begin ");
            strSql.Append("insert into stwh_artype(");
            strSql.Append("stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_artorder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_artname,@stwh_artdescription,@stwh_artimg,@stwh_artparentid,@stwh_artorder)");
            strSql.Append(";select @@IDENTITY");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_artname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_artdescription", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_artimg", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_artparentid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_artorder", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_artname;
            parameters[1].Value = jbmodel.stwh_artdescription;
            parameters[2].Value = jbmodel.stwh_artimg;
            parameters[3].Value = jbmodel.stwh_artparentid;
            parameters[4].Value = jbmodel.stwh_artorder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_artype jbmodel = model as stwh_artype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_artype where stwh_artname = '" + jbmodel.stwh_artname + "' and stwh_artid <> " + jbmodel.stwh_artid + ") = 0 begin ");
            strSql.Append("update stwh_artype set ");
            strSql.Append("stwh_artname=@stwh_artname,");
            strSql.Append("stwh_artdescription=@stwh_artdescription,");
            strSql.Append("stwh_artimg=@stwh_artimg,");
            strSql.Append("stwh_artparentid=@stwh_artparentid,");
            strSql.Append("stwh_artorder=@stwh_artorder");
            strSql.Append(" where stwh_artid=@stwh_artid");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_artname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_artdescription", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_artimg", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_artparentid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_artorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_artid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_artname;
            parameters[1].Value = jbmodel.stwh_artdescription;
            parameters[2].Value = jbmodel.stwh_artimg;
            parameters[3].Value = jbmodel.stwh_artparentid;
            parameters[4].Value = jbmodel.stwh_artorder;
            parameters[5].Value = jbmodel.stwh_artid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_artid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_artype ");
            strSql.Append(" where stwh_artid=@stwh_artid or stwh_artparentid = " + stwh_artid);
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_artid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_artid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_artidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_article ");
            strSql.Append(" where stwh_artid in (" + stwh_artidlist + ");");
            strSql.Append("delete from stwh_artype ");
            strSql.Append(" where stwh_artid in (" + stwh_artidlist + ") or stwh_artparentid in (" + stwh_artidlist + ");");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_artid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_artid,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_artorder from stwh_artype ");
            strSql.Append(" where stwh_artid=@stwh_artid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_artid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_artid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_artype model = new stwh_artype();
            if (row != null)
            {
                if (row["stwh_artid"] != null)
                {
                    model.stwh_artid = int.Parse(row["stwh_artid"].ToString());
                }
                if (row["stwh_artname"] != null)
                {
                    model.stwh_artname = row["stwh_artname"].ToString();
                }
                if (row["stwh_artdescription"] != null)
                {
                    model.stwh_artdescription = row["stwh_artdescription"].ToString();
                }
                if (row["stwh_artimg"] != null)
                {
                    model.stwh_artimg = row["stwh_artimg"].ToString();
                }
                if (row["stwh_artparentid"] != null)
                {
                    model.stwh_artparentid = int.Parse(row["stwh_artparentid"].ToString());
                }
                if (row["stwh_artorder"] != null)
                {
                    model.stwh_artorder = int.Parse(row["stwh_artorder"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_artid,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_artorder ");
            strSql.Append(" FROM stwh_artype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_artid,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_artorder ");
            strSql.Append(" FROM stwh_artype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_artype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_artid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_artype T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

