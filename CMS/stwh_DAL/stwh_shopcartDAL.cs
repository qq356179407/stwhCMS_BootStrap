﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_shopcart
    /// </summary>
    public partial class stwh_shopcartDAL : BaseDAL, Istwh_shopcartDAL
    {
        public stwh_shopcartDAL()
        { }
        #region Istwh_shopcartDAL接口实现方法
        /// <summary>
        /// 更新购物车
        /// </summary>
        /// <param name="model">数据对象</param>
        /// <param name="controller">操作类型（0添加产品数量，1修改产品数量）</param>
        /// <returns></returns>
        public int Add(stwh_shopcart jbmodel, int controller)
        {
            int result = 0;
            try
            {
                SqlParameter[] parameters = {
					new SqlParameter("@stwh_buid", SqlDbType.Int,4),
					new SqlParameter("@stwh_pid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_sccount", SqlDbType.Int,4),
                    new SqlParameter("@controller", SqlDbType.Int,4),
                    new SqlParameter("@result",SqlDbType.Int,4)};
                parameters[0].Value = jbmodel.stwh_buid;
                parameters[1].Value = jbmodel.stwh_pid;
                parameters[2].Value = jbmodel.stwh_sccount;
                parameters[3].Value = controller;
                parameters[4].Direction = ParameterDirection.Output;
                using (DbHelperSQL.RunProcedure("ProcAddUserCart", parameters))
                {
                    result = int.Parse(parameters[4].Value.ToString());
                }
            }
            catch (Exception)
            {

            }
            return result;
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_shopcart");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_shopcart", "stwh_scid", FieldColumn, FieldOrder, "stwh_scid,stwh_buid,stwh_pid,stwh_ptitle,stwh_pdescription,stwh_pprice,stwh_sccount,stwh_psumcount,stwh_pimagelist,stwh_pimage,stwh_ptitlesimple", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_shopcart where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_shopcart", "stwh_scid", FieldColumn, FieldOrder, "stwh_scid,stwh_buid,stwh_pid,stwh_ptitle,stwh_pdescription,stwh_pprice,stwh_sccount,stwh_psumcount,stwh_pimagelist,stwh_pimage,stwh_ptitlesimple", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_scid", "stwh_shopcart");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_scid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_shopcart");
            strSql.Append(" where stwh_scid=@stwh_scid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_scid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_scid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        public int Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_shopcart jbmodel = model as stwh_shopcart;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_shopcart set ");
            strSql.Append("stwh_buid=@stwh_buid,");
            strSql.Append("stwh_pid=@stwh_pid,");
            strSql.Append("stwh_sccount=@stwh_sccount");
            strSql.Append(" where stwh_scid=@stwh_scid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buid", SqlDbType.Int,4),
					new SqlParameter("@stwh_pid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_sccount", SqlDbType.Int,4),
					new SqlParameter("@stwh_scid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_buid;
            parameters[1].Value = jbmodel.stwh_pid;
            parameters[2].Value = jbmodel.stwh_sccount;
            parameters[3].Value = jbmodel.stwh_scid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_scid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_shopcart ");
            strSql.Append(" where stwh_scid=@stwh_scid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_scid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_scid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_scidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_shopcart ");
            strSql.Append(" where stwh_scid in (" + stwh_scidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_scid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_scid,stwh_buid,stwh_pid,stwh_ptitle,stwh_pdescription,stwh_pprice,stwh_sccount,stwh_psumcount,stwh_pimagelist,stwh_pimage,stwh_ptitlesimple from view_shopcart ");
            strSql.Append(" where stwh_scid=@stwh_scid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_scid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_scid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0) return DataRowToModel(ds.Tables[0].Rows[0]);
            else return null;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_shopcart jbmodel = new stwh_shopcart();
            if (row != null)
            {
                if (row["stwh_scid"] != null)
                {
                    jbmodel.stwh_scid = int.Parse(row["stwh_scid"].ToString());
                }
                if (row["stwh_buid"] != null)
                {
                    jbmodel.stwh_buid = int.Parse(row["stwh_buid"].ToString());
                }
                if (row["stwh_pid"] != null)
                {
                    jbmodel.stwh_pid = int.Parse(row["stwh_pid"].ToString());
                }
                if (row["stwh_sccount"] != null)
                {
                    jbmodel.stwh_sccount = int.Parse(row["stwh_sccount"].ToString());
                }
                if (row.ItemArray.Length > 4)
                {
                    if (row["stwh_pprice"] != null)
                    {
                        jbmodel.stwh_pprice = float.Parse(row["stwh_pprice"].ToString());
                    }
                    if (row["stwh_ptitle"] != null)
                    {
                        jbmodel.stwh_ptitle = row["stwh_ptitle"].ToString();
                    }
                    if (row["stwh_pdescription"] != null)
                    {
                        jbmodel.stwh_pdescription = row["stwh_pdescription"].ToString();
                    }
                    if (row["stwh_ptitlesimple"] != null)
                    {
                        jbmodel.stwh_ptitlesimple = row["stwh_ptitlesimple"].ToString();
                    }
                    if (row["stwh_pimage"] != null)
                    {
                        jbmodel.stwh_pimage = row["stwh_pimage"].ToString();
                    }
                    if (row["stwh_pimagelist"] != null)
                    {
                        jbmodel.stwh_pimagelist = row["stwh_pimagelist"].ToString();
                    }
                    if (row["stwh_psumcount"] != null)
                    {
                        jbmodel.stwh_psumcount = int.Parse(row["stwh_psumcount"].ToString());
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_scid,stwh_buid,stwh_pid,stwh_ptitle,stwh_pdescription,stwh_pprice,stwh_sccount,stwh_psumcount,stwh_pimagelist,stwh_pimage,stwh_ptitlesimple ");
            strSql.Append(" FROM view_shopcart ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0) strSql.Append(" top " + Top.ToString());
            strSql.Append(" stwh_scid,stwh_buid,stwh_pid,stwh_ptitle,stwh_pdescription,stwh_pprice,stwh_sccount,stwh_psumcount,stwh_pimagelist,stwh_pimage,stwh_ptitlesimple ");
            strSql.Append(" FROM view_shopcart ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_shopcart ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null) return 0;
            else return Convert.ToInt32(obj);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_scid desc");
            }
            strSql.Append(")AS Row, T.*  from view_shopcart T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

