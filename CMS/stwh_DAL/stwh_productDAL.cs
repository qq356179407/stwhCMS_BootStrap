﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_product
    /// </summary>
    public partial class stwh_productDAL : BaseDAL, Istwh_productDAL
    {
        public stwh_productDAL()
        { }
        #region Istwh_productDAL接口实现方法
        /// <summary>
        /// 批量修改数据审核状态
        /// </summary>
        /// <param name="stwh_pidlist">id集合</param>
        /// <param name="stwh_pissh">状态（0,1）</param>
        /// <returns></returns>
        public bool Update(string stwh_pidlist, int stwh_pissh)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_product set stwh_pissh = " + stwh_pissh);
            strSql.Append(" where stwh_pid in (" + stwh_pidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0) return true;
            else return false;
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_product");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_product", "stwh_pid", FieldColumn, FieldOrder, "stwh_pid,stwh_pid,stwh_ptid,stwh_pbid,stwh_porder,stwh_ptitlesimple,stwh_ptitle,stwh_pimage,stwh_pimagelist,stwh_pcountry,stwh_pcompany,stwh_pprice,stwh_pcreation,stwh_pdescription,stwh_pcontent,stwh_pbiaoqian,stwh_pissh,stwh_piszhiding,stwh_ptuijian,stwh_ptoutiao,stwh_pgundong,stwh_pseotitle,stwh_psetokeywords,stwh_psetodescription,stwh_paddtime,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder,stwh_ptorder,stwh_psumcount", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_product where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_product", "stwh_pid", FieldColumn, FieldOrder, "stwh_pid,stwh_ptid,stwh_pbid,stwh_porder,stwh_ptitlesimple,stwh_ptitle,stwh_pimage,stwh_pimagelist,stwh_pcountry,stwh_pcompany,stwh_pprice,stwh_pcreation,stwh_pdescription,stwh_pcontent,stwh_pbiaoqian,stwh_pissh,stwh_piszhiding,stwh_ptuijian,stwh_ptoutiao,stwh_pgundong,stwh_pseotitle,stwh_psetokeywords,stwh_psetodescription,stwh_paddtime,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder,stwh_ptorder,stwh_psumcount", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_pid", "stwh_product");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_pid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_product");
            strSql.Append(" where stwh_pid=@stwh_pid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_pid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_product jbmodel = model as stwh_product;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_product(");
            strSql.Append("stwh_ptid,stwh_pbid,stwh_porder,stwh_ptitlesimple,stwh_ptitle,stwh_pimage,stwh_pimagelist,stwh_pcountry,stwh_pcompany,stwh_pprice,stwh_pcreation,stwh_pdescription,stwh_pcontent,stwh_pbiaoqian,stwh_pissh,stwh_piszhiding,stwh_ptuijian,stwh_ptoutiao,stwh_pgundong,stwh_pseotitle,stwh_psetokeywords,stwh_psetodescription,stwh_paddtime,stwh_psumcount)");
            strSql.Append(" values (");
            strSql.Append("@stwh_ptid,@stwh_pbid,@stwh_porder,@stwh_ptitlesimple,@stwh_ptitle,@stwh_pimage,@stwh_pimagelist,@stwh_pcountry,@stwh_pcompany,@stwh_pprice,@stwh_pcreation,@stwh_pdescription,@stwh_pcontent,@stwh_pbiaoqian,@stwh_pissh,@stwh_piszhiding,@stwh_ptuijian,@stwh_ptoutiao,@stwh_pgundong,@stwh_pseotitle,@stwh_psetokeywords,@stwh_psetodescription,@stwh_paddtime,@stwh_psumcount)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4),
					new SqlParameter("@stwh_pbid", SqlDbType.Int,4),
					new SqlParameter("@stwh_porder", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptitlesimple", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pimage", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pimagelist", SqlDbType.NText),
					new SqlParameter("@stwh_pcountry", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pcompany", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pprice", SqlDbType.Float,8),
					new SqlParameter("@stwh_pcreation", SqlDbType.DateTime),
					new SqlParameter("@stwh_pdescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pcontent", SqlDbType.NText),
					new SqlParameter("@stwh_pbiaoqian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pissh", SqlDbType.Int,4),
					new SqlParameter("@stwh_piszhiding", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptuijian", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptoutiao", SqlDbType.Int,4),
					new SqlParameter("@stwh_pgundong", SqlDbType.Int,4),
					new SqlParameter("@stwh_pseotitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_psetokeywords", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_psetodescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_paddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_psumcount",SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ptid;
            parameters[1].Value = jbmodel.stwh_pbid;
            parameters[2].Value = jbmodel.stwh_porder;
            parameters[3].Value = jbmodel.stwh_ptitlesimple;
            parameters[4].Value = jbmodel.stwh_ptitle;
            parameters[5].Value = jbmodel.stwh_pimage;
            parameters[6].Value = jbmodel.stwh_pimagelist;
            parameters[7].Value = jbmodel.stwh_pcountry;
            parameters[8].Value = jbmodel.stwh_pcompany;
            parameters[9].Value = jbmodel.stwh_pprice;
            parameters[10].Value = jbmodel.stwh_pcreation;
            parameters[11].Value = jbmodel.stwh_pdescription;
            parameters[12].Value = jbmodel.stwh_pcontent;
            parameters[13].Value = jbmodel.stwh_pbiaoqian;
            parameters[14].Value = jbmodel.stwh_pissh;
            parameters[15].Value = jbmodel.stwh_piszhiding;
            parameters[16].Value = jbmodel.stwh_ptuijian;
            parameters[17].Value = jbmodel.stwh_ptoutiao;
            parameters[18].Value = jbmodel.stwh_pgundong;
            parameters[19].Value = jbmodel.stwh_pseotitle;
            parameters[20].Value = jbmodel.stwh_psetokeywords;
            parameters[21].Value = jbmodel.stwh_psetodescription;
            parameters[22].Value = jbmodel.stwh_paddtime;
            parameters[23].Value = jbmodel.stwh_psumcount;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_product jbmodel = model as stwh_product;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_product set ");
            strSql.Append("stwh_ptid=@stwh_ptid,");
            strSql.Append("stwh_pbid=@stwh_pbid,");
            strSql.Append("stwh_porder=@stwh_porder,");
            strSql.Append("stwh_ptitlesimple=@stwh_ptitlesimple,");
            strSql.Append("stwh_ptitle=@stwh_ptitle,");
            strSql.Append("stwh_pimage=@stwh_pimage,");
            strSql.Append("stwh_pimagelist=@stwh_pimagelist,");
            strSql.Append("stwh_pcountry=@stwh_pcountry,");
            strSql.Append("stwh_pcompany=@stwh_pcompany,");
            strSql.Append("stwh_pprice=@stwh_pprice,");
            strSql.Append("stwh_pcreation=@stwh_pcreation,");
            strSql.Append("stwh_pdescription=@stwh_pdescription,");
            strSql.Append("stwh_pcontent=@stwh_pcontent,");
            strSql.Append("stwh_pbiaoqian=@stwh_pbiaoqian,");
            strSql.Append("stwh_pissh=@stwh_pissh,");
            strSql.Append("stwh_piszhiding=@stwh_piszhiding,");
            strSql.Append("stwh_ptuijian=@stwh_ptuijian,");
            strSql.Append("stwh_ptoutiao=@stwh_ptoutiao,");
            strSql.Append("stwh_pgundong=@stwh_pgundong,");
            strSql.Append("stwh_pseotitle=@stwh_pseotitle,");
            strSql.Append("stwh_psetokeywords=@stwh_psetokeywords,");
            strSql.Append("stwh_psetodescription=@stwh_psetodescription,");
            strSql.Append("stwh_paddtime=@stwh_paddtime,");
            strSql.Append("stwh_psumcount=@stwh_psumcount");
            strSql.Append(" where stwh_pid=@stwh_pid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4),
					new SqlParameter("@stwh_pbid", SqlDbType.Int,4),
					new SqlParameter("@stwh_porder", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptitlesimple", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pimage", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pimagelist", SqlDbType.NText),
					new SqlParameter("@stwh_pcountry", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pcompany", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pprice", SqlDbType.Float,8),
					new SqlParameter("@stwh_pcreation", SqlDbType.DateTime),
					new SqlParameter("@stwh_pdescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pcontent", SqlDbType.NText),
					new SqlParameter("@stwh_pbiaoqian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pissh", SqlDbType.Int,4),
					new SqlParameter("@stwh_piszhiding", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptuijian", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptoutiao", SqlDbType.Int,4),
					new SqlParameter("@stwh_pgundong", SqlDbType.Int,4),
					new SqlParameter("@stwh_pseotitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_psetokeywords", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_psetodescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_paddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_psumcount", SqlDbType.Int,4),
					new SqlParameter("@stwh_pid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ptid;
            parameters[1].Value = jbmodel.stwh_pbid;
            parameters[2].Value = jbmodel.stwh_porder;
            parameters[3].Value = jbmodel.stwh_ptitlesimple;
            parameters[4].Value = jbmodel.stwh_ptitle;
            parameters[5].Value = jbmodel.stwh_pimage;
            parameters[6].Value = jbmodel.stwh_pimagelist;
            parameters[7].Value = jbmodel.stwh_pcountry;
            parameters[8].Value = jbmodel.stwh_pcompany;
            parameters[9].Value = jbmodel.stwh_pprice;
            parameters[10].Value = jbmodel.stwh_pcreation;
            parameters[11].Value = jbmodel.stwh_pdescription;
            parameters[12].Value = jbmodel.stwh_pcontent;
            parameters[13].Value = jbmodel.stwh_pbiaoqian;
            parameters[14].Value = jbmodel.stwh_pissh;
            parameters[15].Value = jbmodel.stwh_piszhiding;
            parameters[16].Value = jbmodel.stwh_ptuijian;
            parameters[17].Value = jbmodel.stwh_ptoutiao;
            parameters[18].Value = jbmodel.stwh_pgundong;
            parameters[19].Value = jbmodel.stwh_pseotitle;
            parameters[20].Value = jbmodel.stwh_psetokeywords;
            parameters[21].Value = jbmodel.stwh_psetodescription;
            parameters[22].Value = jbmodel.stwh_paddtime;
            parameters[23].Value = jbmodel.stwh_psumcount;
            parameters[24].Value = jbmodel.stwh_pid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_pid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_product ");
            strSql.Append(" where stwh_pid=@stwh_pid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_pid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_pidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_product ");
            strSql.Append(" where stwh_pid in (" + stwh_pidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_pid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_pid,stwh_ptid,stwh_pbid,stwh_porder,stwh_ptitlesimple,stwh_ptitle,stwh_pimage,stwh_pimagelist,stwh_pcountry,stwh_pcompany,stwh_pprice,stwh_pcreation,stwh_pdescription,stwh_pcontent,stwh_pbiaoqian,stwh_pissh,stwh_piszhiding,stwh_ptuijian,stwh_ptoutiao,stwh_pgundong,stwh_pseotitle,stwh_psetokeywords,stwh_psetodescription,stwh_paddtime,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder,stwh_ptorder,stwh_psumcount from view_product ");
            strSql.Append(" where stwh_pid=@stwh_pid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_pid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_product jbmodel = new stwh_product();
            if (row != null)
            {
                if (row["stwh_pid"] != null)
                {
                    jbmodel.stwh_pid = int.Parse(row["stwh_pid"].ToString());
                }
                if (row["stwh_ptid"] != null)
                {
                    jbmodel.stwh_ptid = int.Parse(row["stwh_ptid"].ToString());
                }
                if (row["stwh_pbid"] != null)
                {
                    jbmodel.stwh_pbid = int.Parse(row["stwh_pbid"].ToString());
                }
                if (row["stwh_porder"] != null)
                {
                    jbmodel.stwh_porder = int.Parse(row["stwh_porder"].ToString());
                }
                if (row["stwh_ptitlesimple"] != null)
                {
                    jbmodel.stwh_ptitlesimple = row["stwh_ptitlesimple"].ToString();
                }
                if (row["stwh_ptitle"] != null)
                {
                    jbmodel.stwh_ptitle = row["stwh_ptitle"].ToString();
                }
                if (row["stwh_pimage"] != null)
                {
                    jbmodel.stwh_pimage = row["stwh_pimage"].ToString();
                }
                if (row["stwh_pimagelist"] != null)
                {
                    jbmodel.stwh_pimagelist = row["stwh_pimagelist"].ToString();
                }
                if (row["stwh_pcountry"] != null)
                {
                    jbmodel.stwh_pcountry = row["stwh_pcountry"].ToString();
                }
                if (row["stwh_pcompany"] != null)
                {
                    jbmodel.stwh_pcompany = row["stwh_pcompany"].ToString();
                }
                if (row["stwh_pprice"] != null)
                {
                    jbmodel.stwh_pprice = double.Parse(row["stwh_pprice"].ToString());
                }
                if (row["stwh_pcreation"] != null)
                {
                    jbmodel.stwh_pcreation = DateTime.Parse(row["stwh_pcreation"].ToString());
                }
                if (row["stwh_pdescription"] != null)
                {
                    jbmodel.stwh_pdescription = row["stwh_pdescription"].ToString();
                }
                if (row["stwh_pcontent"] != null)
                {
                    jbmodel.stwh_pcontent = row["stwh_pcontent"].ToString();
                }
                if (row["stwh_pbiaoqian"] != null)
                {
                    jbmodel.stwh_pbiaoqian = row["stwh_pbiaoqian"].ToString();
                }
                if (row["stwh_pissh"] != null)
                {
                    jbmodel.stwh_pissh = int.Parse(row["stwh_pissh"].ToString());
                }
                if (row["stwh_piszhiding"] != null)
                {
                    jbmodel.stwh_piszhiding = int.Parse(row["stwh_piszhiding"].ToString());
                }
                if (row["stwh_ptuijian"] != null)
                {
                    jbmodel.stwh_ptuijian = int.Parse(row["stwh_ptuijian"].ToString());
                }
                if (row["stwh_ptoutiao"] != null)
                {
                    jbmodel.stwh_ptoutiao = int.Parse(row["stwh_ptoutiao"].ToString());
                }
                if (row["stwh_pgundong"] != null)
                {
                    jbmodel.stwh_pgundong = int.Parse(row["stwh_pgundong"].ToString());
                }
                if (row["stwh_pseotitle"] != null)
                {
                    jbmodel.stwh_pseotitle = row["stwh_pseotitle"].ToString();
                }
                if (row["stwh_psetokeywords"] != null)
                {
                    jbmodel.stwh_psetokeywords = row["stwh_psetokeywords"].ToString();
                }
                if (row["stwh_psetodescription"] != null)
                {
                    jbmodel.stwh_psetodescription = row["stwh_psetodescription"].ToString();
                }
                if (row["stwh_psumcount"] != null)
                {
                    jbmodel.stwh_psumcount = int.Parse(row["stwh_psumcount"].ToString());
                }
                if (row["stwh_paddtime"] != null)
                {
                    jbmodel.stwh_paddtime = DateTime.Parse(row["stwh_paddtime"].ToString());
                }
                if (row.ItemArray.Length > 25)
                {
                    if (row["stwh_ptname"] != null)
                    {
                        jbmodel.stwh_ptname = row["stwh_ptname"].ToString();
                    }
                    if (row["stwh_ptdescription"] != null)
                    {
                        jbmodel.stwh_ptdescription = row["stwh_ptdescription"].ToString();
                    }
                    if (row["stwh_ptdetails"] != null)
                    {
                        jbmodel.stwh_ptdetails = row["stwh_ptdetails"].ToString();
                    }
                    if (row["stwh_ptimg"] != null)
                    {
                        jbmodel.stwh_ptimg = row["stwh_ptimg"].ToString();
                    }
                    if (row["stwh_ptshowmenu"] != null)
                    {
                        jbmodel.stwh_ptshowmenu = int.Parse(row["stwh_ptshowmenu"].ToString());
                    }
                    if (row["stwh_ptparentid"] != null)
                    {
                        jbmodel.stwh_ptparentid = int.Parse(row["stwh_ptparentid"].ToString());
                    }
                    if (row["stwh_ptorder"] != null)
                    {
                        jbmodel.stwh_ptorder = int.Parse(row["stwh_ptorder"].ToString());
                    }
                    if (row["stwh_pbname"] != null)
                    {
                        jbmodel.stwh_pbname = row["stwh_pbname"].ToString();
                    }
                    if (row["stwh_pbimage"] != null)
                    {
                        jbmodel.stwh_pbimage = row["stwh_pbimage"].ToString();
                    }
                    if (row["stwh_pburl"] != null)
                    {
                        jbmodel.stwh_pburl = row["stwh_pburl"].ToString();
                    }
                    if (row["stwh_pbdescription"] != null)
                    {
                        jbmodel.stwh_pbdescription = row["stwh_pbdescription"].ToString();
                    }
                    if (row["stwh_pborder"] != null)
                    {
                        jbmodel.stwh_pborder = int.Parse(row["stwh_pborder"].ToString());
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_pid,stwh_ptid,stwh_pbid,stwh_porder,stwh_ptitlesimple,stwh_ptitle,stwh_pimage,stwh_pimagelist,stwh_pcountry,stwh_pcompany,stwh_pprice,stwh_pcreation,stwh_pdescription,stwh_pcontent,stwh_pbiaoqian,stwh_pissh,stwh_piszhiding,stwh_ptuijian,stwh_ptoutiao,stwh_pgundong,stwh_pseotitle,stwh_psetokeywords,stwh_psetodescription,stwh_paddtime,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder,stwh_ptorder,stwh_psumcount ");
            strSql.Append(" FROM view_product ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_pid,stwh_ptid,stwh_pbid,stwh_porder,stwh_ptitlesimple,stwh_ptitle,stwh_pimage,stwh_pimagelist,stwh_pcountry,stwh_pcompany,stwh_pprice,stwh_pcreation,stwh_pdescription,stwh_pcontent,stwh_pbiaoqian,stwh_pissh,stwh_piszhiding,stwh_ptuijian,stwh_ptoutiao,stwh_pgundong,stwh_pseotitle,stwh_psetokeywords,stwh_psetodescription,stwh_paddtime,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder,stwh_ptorder,stwh_psumcount ");
            strSql.Append(" FROM view_product ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_product ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_pid desc");
            }
            strSql.Append(")AS Row, T.*  from view_product T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

