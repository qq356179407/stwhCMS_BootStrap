﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_files
    /// </summary>
    public partial class stwh_filesDAL : BaseDAL, Istwh_filesDAL
    {
        public stwh_filesDAL()
        { }
        #region Istwh_filesDAL接口实现方法
        /// <summary>
        /// 批量修改文件审核状态
        /// </summary>
        /// <param name="stwh_flidlist">文件id</param>
        /// <param name="stwh_flissh">状态（0,1）</param>
        /// <returns></returns>
        public bool Update(string stwh_flidlist, int stwh_flissh)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_files set stwh_flissh = " + stwh_flissh);
            strSql.Append(" where stwh_flid in (" + stwh_flidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_files");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_files", "stwh_flid", FieldColumn, FieldOrder, "stwh_flid,stwh_ftid,stwh_florder,stwh_fltitlesimple,stwh_fltitle,stwh_flimage,stwh_flpath,stwh_flauthor,stwh_flsource,stwh_flfilesize,stwh_fldanwei,stwh_fljianjie,stwh_flcontent,stwh_flbiaoqian,stwh_flissh,stwh_fliszhiding,stwh_fltuijian,stwh_fltoutiao,stwh_flgundong,stwh_flseotitle,stwh_flsetokeywords,stwh_flsetodescription,stwh_fladdtime,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_files where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_files", "stwh_flid", FieldColumn, FieldOrder, "stwh_flid,stwh_ftid,stwh_florder,stwh_fltitlesimple,stwh_fltitle,stwh_flimage,stwh_flpath,stwh_flauthor,stwh_flsource,stwh_flfilesize,stwh_fldanwei,stwh_fljianjie,stwh_flcontent,stwh_flbiaoqian,stwh_flissh,stwh_fliszhiding,stwh_fltuijian,stwh_fltoutiao,stwh_flgundong,stwh_flseotitle,stwh_flsetokeywords,stwh_flsetodescription,stwh_fladdtime,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_flid", "stwh_files");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_flid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_files");
            strSql.Append(" where stwh_flid=@stwh_flid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_flid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_flid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_files jbmodel = model as stwh_files;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_files(");
            strSql.Append("stwh_ftid,stwh_florder,stwh_fltitlesimple,stwh_fltitle,stwh_flimage,stwh_flpath,stwh_flauthor,stwh_flsource,stwh_flfilesize,stwh_fldanwei,stwh_fljianjie,stwh_flcontent,stwh_flbiaoqian,stwh_flissh,stwh_fliszhiding,stwh_fltuijian,stwh_fltoutiao,stwh_flgundong,stwh_flseotitle,stwh_flsetokeywords,stwh_flsetodescription,stwh_fladdtime)");
            strSql.Append(" values (");
            strSql.Append("@stwh_ftid,@stwh_florder,@stwh_fltitlesimple,@stwh_fltitle,@stwh_flimage,@stwh_flpath,@stwh_flauthor,@stwh_flsource,@stwh_flfilesize,@stwh_fldanwei,@stwh_fljianjie,@stwh_flcontent,@stwh_flbiaoqian,@stwh_flissh,@stwh_fliszhiding,@stwh_fltuijian,@stwh_fltoutiao,@stwh_flgundong,@stwh_flseotitle,@stwh_flsetokeywords,@stwh_flsetodescription,@stwh_fladdtime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ftid", SqlDbType.Int,4),
					new SqlParameter("@stwh_florder", SqlDbType.Int,4),
					new SqlParameter("@stwh_fltitlesimple", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_fltitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flimage", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flpath", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flauthor", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flsource", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flfilesize", SqlDbType.Int,4),
					new SqlParameter("@stwh_fldanwei", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_fljianjie", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flcontent", SqlDbType.NText),
					new SqlParameter("@stwh_flbiaoqian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flissh", SqlDbType.Int,4),
					new SqlParameter("@stwh_fliszhiding", SqlDbType.Int,4),
					new SqlParameter("@stwh_fltuijian", SqlDbType.Int,4),
					new SqlParameter("@stwh_fltoutiao", SqlDbType.Int,4),
					new SqlParameter("@stwh_flgundong", SqlDbType.Int,4),
					new SqlParameter("@stwh_flseotitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flsetokeywords", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flsetodescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_fladdtime", SqlDbType.DateTime)};
            parameters[0].Value = jbmodel.stwh_ftid;
            parameters[1].Value = jbmodel.stwh_florder;
            parameters[2].Value = jbmodel.stwh_fltitlesimple;
            parameters[3].Value = jbmodel.stwh_fltitle;
            parameters[4].Value = jbmodel.stwh_flimage;
            parameters[5].Value = jbmodel.stwh_flpath;
            parameters[6].Value = jbmodel.stwh_flauthor;
            parameters[7].Value = jbmodel.stwh_flsource;
            parameters[8].Value = jbmodel.stwh_flfilesize;
            parameters[9].Value = jbmodel.stwh_fldanwei;
            parameters[10].Value = jbmodel.stwh_fljianjie;
            parameters[11].Value = jbmodel.stwh_flcontent;
            parameters[12].Value = jbmodel.stwh_flbiaoqian;
            parameters[13].Value = jbmodel.stwh_flissh;
            parameters[14].Value = jbmodel.stwh_fliszhiding;
            parameters[15].Value = jbmodel.stwh_fltuijian;
            parameters[16].Value = jbmodel.stwh_fltoutiao;
            parameters[17].Value = jbmodel.stwh_flgundong;
            parameters[18].Value = jbmodel.stwh_flseotitle;
            parameters[19].Value = jbmodel.stwh_flsetokeywords;
            parameters[20].Value = jbmodel.stwh_flsetodescription;
            parameters[21].Value = jbmodel.stwh_fladdtime;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_files jbmodel = model as stwh_files;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_files set ");
            strSql.Append("stwh_ftid=@stwh_ftid,");
            strSql.Append("stwh_florder=@stwh_florder,");
            strSql.Append("stwh_fltitlesimple=@stwh_fltitlesimple,");
            strSql.Append("stwh_fltitle=@stwh_fltitle,");
            strSql.Append("stwh_flimage=@stwh_flimage,");
            strSql.Append("stwh_flpath=@stwh_flpath,");
            strSql.Append("stwh_flauthor=@stwh_flauthor,");
            strSql.Append("stwh_flsource=@stwh_flsource,");
            strSql.Append("stwh_flfilesize=@stwh_flfilesize,");
            strSql.Append("stwh_fldanwei=@stwh_fldanwei,");
            strSql.Append("stwh_fljianjie=@stwh_fljianjie,");
            strSql.Append("stwh_flcontent=@stwh_flcontent,");
            strSql.Append("stwh_flbiaoqian=@stwh_flbiaoqian,");
            strSql.Append("stwh_flissh=@stwh_flissh,");
            strSql.Append("stwh_fliszhiding=@stwh_fliszhiding,");
            strSql.Append("stwh_fltuijian=@stwh_fltuijian,");
            strSql.Append("stwh_fltoutiao=@stwh_fltoutiao,");
            strSql.Append("stwh_flgundong=@stwh_flgundong,");
            strSql.Append("stwh_flseotitle=@stwh_flseotitle,");
            strSql.Append("stwh_flsetokeywords=@stwh_flsetokeywords,");
            strSql.Append("stwh_flsetodescription=@stwh_flsetodescription,");
            strSql.Append("stwh_fladdtime=@stwh_fladdtime");
            strSql.Append(" where stwh_flid=@stwh_flid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ftid", SqlDbType.Int,4),
					new SqlParameter("@stwh_florder", SqlDbType.Int,4),
					new SqlParameter("@stwh_fltitlesimple", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_fltitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flimage", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flpath", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flauthor", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flsource", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flfilesize", SqlDbType.Int,4),
					new SqlParameter("@stwh_fldanwei", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_fljianjie", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flcontent", SqlDbType.NText),
					new SqlParameter("@stwh_flbiaoqian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flissh", SqlDbType.Int,4),
					new SqlParameter("@stwh_fliszhiding", SqlDbType.Int,4),
					new SqlParameter("@stwh_fltuijian", SqlDbType.Int,4),
					new SqlParameter("@stwh_fltoutiao", SqlDbType.Int,4),
					new SqlParameter("@stwh_flgundong", SqlDbType.Int,4),
					new SqlParameter("@stwh_flseotitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flsetokeywords", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_flsetodescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_fladdtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_flid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ftid;
            parameters[1].Value = jbmodel.stwh_florder;
            parameters[2].Value = jbmodel.stwh_fltitlesimple;
            parameters[3].Value = jbmodel.stwh_fltitle;
            parameters[4].Value = jbmodel.stwh_flimage;
            parameters[5].Value = jbmodel.stwh_flpath;
            parameters[6].Value = jbmodel.stwh_flauthor;
            parameters[7].Value = jbmodel.stwh_flsource;
            parameters[8].Value = jbmodel.stwh_flfilesize;
            parameters[9].Value = jbmodel.stwh_fldanwei;
            parameters[10].Value = jbmodel.stwh_fljianjie;
            parameters[11].Value = jbmodel.stwh_flcontent;
            parameters[12].Value = jbmodel.stwh_flbiaoqian;
            parameters[13].Value = jbmodel.stwh_flissh;
            parameters[14].Value = jbmodel.stwh_fliszhiding;
            parameters[15].Value = jbmodel.stwh_fltuijian;
            parameters[16].Value = jbmodel.stwh_fltoutiao;
            parameters[17].Value = jbmodel.stwh_flgundong;
            parameters[18].Value = jbmodel.stwh_flseotitle;
            parameters[19].Value = jbmodel.stwh_flsetokeywords;
            parameters[20].Value = jbmodel.stwh_flsetodescription;
            parameters[21].Value = jbmodel.stwh_fladdtime;
            parameters[22].Value = jbmodel.stwh_flid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_flid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_files ");
            strSql.Append(" where stwh_flid=@stwh_flid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_flid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_flid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_flidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_files ");
            strSql.Append(" where stwh_flid in (" + stwh_flidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_flid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_flid,stwh_ftid,stwh_florder,stwh_fltitlesimple,stwh_fltitle,stwh_flimage,stwh_flpath,stwh_flauthor,stwh_flsource,stwh_flfilesize,stwh_fldanwei,stwh_fljianjie,stwh_flcontent,stwh_flbiaoqian,stwh_flissh,stwh_fliszhiding,stwh_fltuijian,stwh_fltoutiao,stwh_flgundong,stwh_flseotitle,stwh_flsetokeywords,stwh_flsetodescription,stwh_fladdtime,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder from view_files ");
            strSql.Append(" where stwh_flid=@stwh_flid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_flid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_flid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_files jbmodel = new stwh_files();
            if (row != null)
            {
                if (row["stwh_flid"] != null)
                {
                    jbmodel.stwh_flid = int.Parse(row["stwh_flid"].ToString());
                }
                if (row["stwh_ftid"] != null)
                {
                    jbmodel.stwh_ftid = int.Parse(row["stwh_ftid"].ToString());
                }
                if (row["stwh_florder"] != null)
                {
                    jbmodel.stwh_florder = int.Parse(row["stwh_florder"].ToString());
                }
                if (row["stwh_fltitlesimple"] != null)
                {
                    jbmodel.stwh_fltitlesimple = row["stwh_fltitlesimple"].ToString();
                }
                if (row["stwh_fltitle"] != null)
                {
                    jbmodel.stwh_fltitle = row["stwh_fltitle"].ToString();
                }
                if (row["stwh_flimage"] != null)
                {
                    jbmodel.stwh_flimage = row["stwh_flimage"].ToString();
                }
                if (row["stwh_flpath"] != null)
                {
                    jbmodel.stwh_flpath = row["stwh_flpath"].ToString();
                }
                if (row["stwh_flauthor"] != null)
                {
                    jbmodel.stwh_flauthor = row["stwh_flauthor"].ToString();
                }
                if (row["stwh_flsource"] != null)
                {
                    jbmodel.stwh_flsource = row["stwh_flsource"].ToString();
                }
                if (row["stwh_flfilesize"] != null)
                {
                    jbmodel.stwh_flfilesize = int.Parse(row["stwh_flfilesize"].ToString());
                }
                if (row["stwh_fldanwei"] != null)
                {
                    jbmodel.stwh_fldanwei = row["stwh_fldanwei"].ToString();
                }
                if (row["stwh_fljianjie"] != null)
                {
                    jbmodel.stwh_fljianjie = row["stwh_fljianjie"].ToString();
                }
                if (row["stwh_flcontent"] != null)
                {
                    jbmodel.stwh_flcontent = row["stwh_flcontent"].ToString();
                }
                if (row["stwh_flbiaoqian"] != null)
                {
                    jbmodel.stwh_flbiaoqian = row["stwh_flbiaoqian"].ToString();
                }
                if (row["stwh_flissh"] != null)
                {
                    jbmodel.stwh_flissh = int.Parse(row["stwh_flissh"].ToString());
                }
                if (row["stwh_fliszhiding"] != null)
                {
                    jbmodel.stwh_fliszhiding = int.Parse(row["stwh_fliszhiding"].ToString());
                }
                if (row["stwh_fltuijian"] != null)
                {
                    jbmodel.stwh_fltuijian = int.Parse(row["stwh_fltuijian"].ToString());
                }
                if (row["stwh_fltoutiao"] != null)
                {
                    jbmodel.stwh_fltoutiao = int.Parse(row["stwh_fltoutiao"].ToString());
                }
                if (row["stwh_flgundong"] != null)
                {
                    jbmodel.stwh_flgundong = int.Parse(row["stwh_flgundong"].ToString());
                }
                if (row["stwh_flseotitle"] != null)
                {
                    jbmodel.stwh_flseotitle = row["stwh_flseotitle"].ToString();
                }
                if (row["stwh_flsetokeywords"] != null)
                {
                    jbmodel.stwh_flsetokeywords = row["stwh_flsetokeywords"].ToString();
                }
                if (row["stwh_flsetodescription"] != null)
                {
                    jbmodel.stwh_flsetodescription = row["stwh_flsetodescription"].ToString();
                }
                if (row["stwh_fladdtime"] != null)
                {
                    jbmodel.stwh_fladdtime = DateTime.Parse(row["stwh_fladdtime"].ToString());
                }
                if (row.ItemArray.Length > 23)
                {
                    if (row["stwh_ftname"] != null)
                    {
                        jbmodel.stwh_ftname = row["stwh_ftname"].ToString();
                    }
                    if (row["stwh_ftdescription"] != null)
                    {
                        jbmodel.stwh_ftdescription = row["stwh_ftdescription"].ToString();
                    }
                    if (row["stwh_ftimg"] != null)
                    {
                        jbmodel.stwh_ftimg = row["stwh_ftimg"].ToString();
                    }
                    if (row["stwh_ftorder"] != null)
                    {
                        jbmodel.stwh_ftorder = int.Parse(row["stwh_ftorder"].ToString());
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_flid,stwh_ftid,stwh_florder,stwh_fltitlesimple,stwh_fltitle,stwh_flimage,stwh_flpath,stwh_flauthor,stwh_flsource,stwh_flfilesize,stwh_fldanwei,stwh_fljianjie,stwh_flcontent,stwh_flbiaoqian,stwh_flissh,stwh_fliszhiding,stwh_fltuijian,stwh_fltoutiao,stwh_flgundong,stwh_flseotitle,stwh_flsetokeywords,stwh_flsetodescription,stwh_fladdtime,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder ");
            strSql.Append(" FROM view_files ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_flid,stwh_ftid,stwh_florder,stwh_fltitlesimple,stwh_fltitle,stwh_flimage,stwh_flpath,stwh_flauthor,stwh_flsource,stwh_flfilesize,stwh_fldanwei,stwh_fljianjie,stwh_flcontent,stwh_flbiaoqian,stwh_flissh,stwh_fliszhiding,stwh_fltuijian,stwh_fltoutiao,stwh_flgundong,stwh_flseotitle,stwh_flsetokeywords,stwh_flsetodescription,stwh_fladdtime,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder ");
            strSql.Append(" FROM view_files ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_files ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_flid desc");
            }
            strSql.Append(")AS Row, T.*  from view_files T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

