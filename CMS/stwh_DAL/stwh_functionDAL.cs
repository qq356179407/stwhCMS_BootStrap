﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_function
    /// </summary>
    public partial class stwh_functionDAL : BaseDAL, Istwh_functionDAL
    {
        public stwh_functionDAL()
        { }
        #region Istwh_functionDAL接口实现方法
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere, int flag)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_fid,stwh_menuid,stwh_fname,stwh_fico,stwh_furl,stwh_fremark,stwh_forder,stwh_rid,stwh_idname,stwh_fpath,stwh_fishow ");
            strSql.Append(" FROM view_role_menu_function ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion
        #region  IBaseDAL接口实现方法
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_fid", "stwh_function");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_fid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_function");
            strSql.Append(" where stwh_fid=@stwh_fid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_fid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_fid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_function jbmodel = model as stwh_function;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_function(");
            strSql.Append("stwh_menuid,stwh_fname,stwh_fico,stwh_furl,stwh_fremark,stwh_forder,stwh_idname,stwh_fpath,stwh_fishow)");
            strSql.Append(" values (");
            strSql.Append("@stwh_menuid,@stwh_fname,@stwh_fico,@stwh_furl,@stwh_fremark,@stwh_forder,@stwh_idname,@stwh_fpath,@stwh_fishow)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4),
					new SqlParameter("@stwh_fname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_fico", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_furl", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_fremark", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_forder", SqlDbType.Int,4),
                    new SqlParameter("@stwh_idname", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_fpath", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_fishow", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_menuid;
            parameters[1].Value = jbmodel.stwh_fname;
            parameters[2].Value = jbmodel.stwh_fico;
            parameters[3].Value = jbmodel.stwh_furl;
            parameters[4].Value = jbmodel.stwh_fremark;
            parameters[5].Value = jbmodel.stwh_forder;
            parameters[6].Value = jbmodel.stwh_idname;
            parameters[7].Value = jbmodel.stwh_fpath;
            parameters[8].Value = jbmodel.stwh_fishow;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_function jbmodel = model as stwh_function;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_function set ");
            strSql.Append("stwh_menuid=@stwh_menuid,");
            strSql.Append("stwh_fname=@stwh_fname,");
            strSql.Append("stwh_fico=@stwh_fico,");
            strSql.Append("stwh_furl=@stwh_furl,");
            strSql.Append("stwh_fremark=@stwh_fremark,");
            strSql.Append("stwh_forder=@stwh_forder,");
            strSql.Append("stwh_idname=@stwh_idname,");
            strSql.Append("stwh_fpath=@stwh_fpath,");
            strSql.Append("stwh_fishow=@stwh_fishow");
            strSql.Append(" where stwh_fid=@stwh_fid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4),
					new SqlParameter("@stwh_fname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_fico", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_furl", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_fremark", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_forder", SqlDbType.Int,4),
                    new SqlParameter("@stwh_idname", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_fpath", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_fishow", SqlDbType.Int,4),
					new SqlParameter("@stwh_fid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_menuid;
            parameters[1].Value = jbmodel.stwh_fname;
            parameters[2].Value = jbmodel.stwh_fico;
            parameters[3].Value = jbmodel.stwh_furl;
            parameters[4].Value = jbmodel.stwh_fremark;
            parameters[5].Value = jbmodel.stwh_forder;
            parameters[6].Value = jbmodel.stwh_idname;
            parameters[7].Value = jbmodel.stwh_fpath;
            parameters[8].Value = jbmodel.stwh_fishow;
            parameters[9].Value = jbmodel.stwh_fid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_fid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_function ");
            strSql.Append(" where stwh_fid=@stwh_fid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_fid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_fid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_fidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_function ");
            strSql.Append(" where stwh_fid in (" + stwh_fidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_fid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_fid,stwh_menuid,stwh_fname,stwh_fico,stwh_furl,stwh_fremark,stwh_forder,stwh_idname,stwh_fpath,stwh_fishow from stwh_function ");
            strSql.Append(" where stwh_fid=@stwh_fid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_fid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_fid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_function jbmodel = new stwh_function();
            if (row != null)
            {
                if (row["stwh_fid"] != null)
                {
                    jbmodel.stwh_fid = int.Parse(row["stwh_fid"].ToString());
                }
                if (row["stwh_fishow"] != null)
                {
                    jbmodel.stwh_fishow = int.Parse(row["stwh_fishow"].ToString());
                }
                if (row["stwh_menuid"] != null)
                {
                    jbmodel.stwh_menuid = int.Parse(row["stwh_menuid"].ToString());
                }
                if (row["stwh_idname"] != null)
                {
                    jbmodel.stwh_idname = row["stwh_idname"].ToString();
                }
                if (row["stwh_fname"] != null)
                {
                    jbmodel.stwh_fname = row["stwh_fname"].ToString();
                }
                if (row["stwh_fico"] != null)
                {
                    jbmodel.stwh_fico = row["stwh_fico"].ToString();
                }
                if (row["stwh_furl"] != null)
                {
                    jbmodel.stwh_furl = row["stwh_furl"].ToString();
                }
                if (row["stwh_forder"] != null)
                {
                    jbmodel.stwh_forder = int.Parse(row["stwh_forder"].ToString());
                }
                if (row["stwh_fremark"] != null)
                {
                    jbmodel.stwh_fremark = row["stwh_fremark"].ToString();
                }
                if (row["stwh_fpath"] != null)
                {
                    jbmodel.stwh_fpath = row["stwh_fpath"].ToString();
                }
                if (row.ItemArray.Length > 10)
                {
                    if (row["stwh_rid"] != null)
                    {
                        jbmodel.stwh_rid = int.Parse(row["stwh_rid"].ToString());
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_fid,stwh_menuid,stwh_fname,stwh_fico,stwh_furl,stwh_fremark,stwh_forder,stwh_idname,stwh_fpath,stwh_fishow ");
            strSql.Append(" FROM stwh_function");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_fid,stwh_menuid,stwh_fname,stwh_fico,stwh_furl,stwh_fremark,stwh_forder,stwh_rid,stwh_idname,stwh_fpath,stwh_fishow ");
            strSql.Append(" FROM view_role_menu_function ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_role_menu_function ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_fid desc");
            }
            strSql.Append(")AS Row, T.*  from view_role_menu_function T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

