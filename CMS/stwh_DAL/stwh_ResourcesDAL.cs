﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_Resources
    /// </summary>
    public partial class stwh_ResourcesDAL : BaseDAL, Istwh_ResourcesDAL
    {
        public stwh_ResourcesDAL()
        { }
        #region Istwh_ResourcesDAL接口实现方法
        /// <summary>
        /// 清空数据
        /// </summary>
        public bool DeleteListALL()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_Resources ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_Resources");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_Resources", "stwh_reid", FieldColumn, FieldOrder, "stwh_reid,stwh_rename,stwh_repath,stwh_retype,stwh_retime,stwh_resize,stwh_reuname,stwh_readdtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_Resources where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_Resources", "stwh_reid", FieldColumn, FieldOrder, "stwh_reid,stwh_rename,stwh_repath,stwh_retype,stwh_retime,stwh_resize,stwh_reuname,stwh_readdtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_reid", "stwh_Resources");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_reid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_Resources");
            strSql.Append(" where stwh_reid=@stwh_reid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_reid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_reid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_Resources jbmodel = model as stwh_Resources;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_Resources(");
            strSql.Append("stwh_rename,stwh_repath,stwh_retype,stwh_retime,stwh_resize,stwh_reuname,stwh_readdtime)");
            strSql.Append(" values (");
            strSql.Append("@stwh_rename,@stwh_repath,@stwh_retype,@stwh_retime,@stwh_resize,@stwh_reuname,@stwh_readdtime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_rename", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_repath", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_retype", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_retime", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_resize", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_reuname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_readdtime", SqlDbType.DateTime)};
            parameters[0].Value = jbmodel.stwh_rename;
            parameters[1].Value = jbmodel.stwh_repath;
            parameters[2].Value = jbmodel.stwh_retype;
            parameters[3].Value = jbmodel.stwh_retime;
            parameters[4].Value = jbmodel.stwh_resize;
            parameters[5].Value = jbmodel.stwh_reuname;
            parameters[6].Value = jbmodel.stwh_readdtime;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_Resources jbmodel = model as stwh_Resources;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_Resources set ");
            strSql.Append("stwh_rename=@stwh_rename,");
            strSql.Append("stwh_repath=@stwh_repath,");
            strSql.Append("stwh_retype=@stwh_retype,");
            strSql.Append("stwh_retime=@stwh_retime,");
            strSql.Append("stwh_resize=@stwh_resize,");
            strSql.Append("stwh_reuname=@stwh_reuname,");
            strSql.Append("stwh_readdtime=@stwh_readdtime");
            strSql.Append(" where stwh_reid=@stwh_reid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_rename", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_repath", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_retype", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_retime", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_resize", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_reuname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_readdtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_reid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_rename;
            parameters[1].Value = jbmodel.stwh_repath;
            parameters[2].Value = jbmodel.stwh_retype;
            parameters[3].Value = jbmodel.stwh_retime;
            parameters[4].Value = jbmodel.stwh_resize;
            parameters[5].Value = jbmodel.stwh_reuname;
            parameters[6].Value = jbmodel.stwh_readdtime;
            parameters[7].Value = jbmodel.stwh_reid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_reid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_Resources ");
            strSql.Append(" where stwh_reid=@stwh_reid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_reid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_reid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_reidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_Resources ");
            strSql.Append(" where stwh_reid in (" + stwh_reidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_reid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_reid,stwh_rename,stwh_repath,stwh_retype,stwh_retime,stwh_resize,stwh_reuname,stwh_readdtime from stwh_Resources ");
            strSql.Append(" where stwh_reid=@stwh_reid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_reid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_reid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_Resources jbmodel = new stwh_Resources();
            if (row != null)
            {
                if (row["stwh_reid"] != null)
                {
                    jbmodel.stwh_reid = int.Parse(row["stwh_reid"].ToString());
                }
                if (row["stwh_rename"] != null)
                {
                    jbmodel.stwh_rename = row["stwh_rename"].ToString();
                }
                if (row["stwh_repath"] != null)
                {
                    jbmodel.stwh_repath = row["stwh_repath"].ToString();
                }
                if (row["stwh_retype"] != null)
                {
                    jbmodel.stwh_retype = row["stwh_retype"].ToString();
                }
                if (row["stwh_retime"] != null)
                {
                    jbmodel.stwh_retime = row["stwh_retime"].ToString();
                }
                if (row["stwh_resize"] != null)
                {
                    jbmodel.stwh_resize = row["stwh_resize"].ToString();
                }
                if (row["stwh_reuname"] != null)
                {
                    jbmodel.stwh_reuname = row["stwh_reuname"].ToString();
                }
                if (row["stwh_readdtime"] != null)
                {
                    jbmodel.stwh_readdtime = DateTime.Parse(row["stwh_readdtime"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_reid,stwh_rename,stwh_repath,stwh_retype,stwh_retime,stwh_resize,stwh_reuname,stwh_readdtime ");
            strSql.Append(" FROM stwh_Resources ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_reid,stwh_rename,stwh_repath,stwh_retype,stwh_retime,stwh_resize,stwh_reuname,stwh_readdtime ");
            strSql.Append(" FROM stwh_Resources ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_Resources ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_reid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_Resources T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

