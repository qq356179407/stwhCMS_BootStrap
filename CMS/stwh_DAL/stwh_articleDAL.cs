﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_article
    /// </summary>
    public partial class stwh_articleDAL : BaseDAL, Istwh_articleDAL
    {
        public stwh_articleDAL()
        { }
        #region Istwh_articleDAL接口实现方法
        /// <summary>
        /// 批量修改文章审核状态
        /// </summary>
        /// <param name="stwh_atidlist">文章id</param>
        /// <param name="stwh_atissh">状态（0,1）</param>
        /// <returns></returns>
        public bool Update(string stwh_atidlist, int stwh_atissh)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_article set stwh_atissh = " + stwh_atissh);
            strSql.Append(" where stwh_atid in (" + stwh_atidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_article");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_article", "stwh_atid", FieldColumn, FieldOrder, "stwh_atid,stwh_artid,stwh_attitlesimple,stwh_attitle,stwh_atimage,stwh_atauthor,stwh_atsource,stwh_atjianjie,stwh_atcontent,stwh_atbiaoqian,stwh_atissh,stwh_atiszhiding,stwh_attuijian,stwh_attoutiao,stwh_atgundong,stwh_atseotitle,stwh_atseokeywords,stwh_atseodescription,stwh_ataddtime,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_atorder,stwh_atimglist,stwh_artorder,stwh_atstyle,stwh_atvideourl,stwh_atsourceurl", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_article where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_article", "stwh_atid", FieldColumn, FieldOrder, "stwh_atid,stwh_artid,stwh_attitlesimple,stwh_attitle,stwh_atimage,stwh_atauthor,stwh_atsource,stwh_atjianjie,stwh_atcontent,stwh_atbiaoqian,stwh_atissh,stwh_atiszhiding,stwh_attuijian,stwh_attoutiao,stwh_atgundong,stwh_atseotitle,stwh_atseokeywords,stwh_atseodescription,stwh_ataddtime,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_atorder,stwh_atimglist,stwh_artorder,stwh_atstyle,stwh_atvideourl,stwh_atsourceurl", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_atid", "stwh_article");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_atid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_article");
            strSql.Append(" where stwh_atid=@stwh_atid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_atid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_atid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_article jbmodel = model as stwh_article;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_article(");
            strSql.Append("stwh_artid,stwh_attitle,stwh_atimage,stwh_atauthor,stwh_atsource,stwh_atjianjie,stwh_atcontent,stwh_atbiaoqian,stwh_atissh,stwh_atiszhiding,stwh_attuijian,stwh_attoutiao,stwh_atgundong,stwh_atseotitle,stwh_atseokeywords,stwh_atseodescription,stwh_ataddtime,stwh_atorder,stwh_attitlesimple,stwh_atimglist,stwh_atstyle,stwh_atvideourl,stwh_atsourceurl)");
            strSql.Append(" values (");
            strSql.Append("@stwh_artid,@stwh_attitle,@stwh_atimage,@stwh_atauthor,@stwh_atsource,@stwh_atjianjie,@stwh_atcontent,@stwh_atbiaoqian,@stwh_atissh,@stwh_atiszhiding,@stwh_attuijian,@stwh_attoutiao,@stwh_atgundong,@stwh_atseotitle,@stwh_atseokeywords,@stwh_atseodescription,@stwh_ataddtime,@stwh_atorder,@stwh_attitlesimple,@stwh_atimglist,@stwh_atstyle,@stwh_atvideourl,@stwh_atsourceurl)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_artid", SqlDbType.Int,4),
					new SqlParameter("@stwh_attitle", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_atimage", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atauthor", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atsource", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atjianjie", SqlDbType.NVarChar,400),
					new SqlParameter("@stwh_atcontent", SqlDbType.NText),
					new SqlParameter("@stwh_atbiaoqian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_atissh", SqlDbType.Int,4),
					new SqlParameter("@stwh_atiszhiding", SqlDbType.Int,4),
					new SqlParameter("@stwh_attuijian", SqlDbType.Int,4),
					new SqlParameter("@stwh_attoutiao", SqlDbType.Int,4),
					new SqlParameter("@stwh_atgundong", SqlDbType.Int,4),
					new SqlParameter("@stwh_atseotitle", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atseokeywords", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_atseodescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ataddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_atorder", SqlDbType.Int,4),
                    new SqlParameter("@stwh_attitlesimple", SqlDbType.NVarChar,200),
                    new SqlParameter("@stwh_atimglist", SqlDbType.NText),
                    new SqlParameter("@stwh_atstyle", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_atvideourl", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_atsourceurl", SqlDbType.NVarChar,500)};
            parameters[0].Value = jbmodel.stwh_artid;
            parameters[1].Value = jbmodel.stwh_attitle;
            parameters[2].Value = jbmodel.stwh_atimage;
            parameters[3].Value = jbmodel.stwh_atauthor;
            parameters[4].Value = jbmodel.stwh_atsource;
            parameters[5].Value = jbmodel.stwh_atjianjie;
            parameters[6].Value = jbmodel.stwh_atcontent;
            parameters[7].Value = jbmodel.stwh_atbiaoqian;
            parameters[8].Value = jbmodel.stwh_atissh;
            parameters[9].Value = jbmodel.stwh_atiszhiding;
            parameters[10].Value = jbmodel.stwh_attuijian;
            parameters[11].Value = jbmodel.stwh_attoutiao;
            parameters[12].Value = jbmodel.stwh_atgundong;
            parameters[13].Value = jbmodel.stwh_atseotitle;
            parameters[14].Value = jbmodel.stwh_atseokeywords;
            parameters[15].Value = jbmodel.stwh_atseodescription;
            parameters[16].Value = jbmodel.stwh_ataddtime;
            parameters[17].Value = jbmodel.stwh_atorder;
            parameters[18].Value = jbmodel.stwh_attitlesimple;
            parameters[19].Value = jbmodel.stwh_atimglist;
            parameters[20].Value = jbmodel.stwh_atstyle;
            parameters[21].Value = jbmodel.stwh_atvideourl;
            parameters[22].Value = jbmodel.stwh_atsourceurl;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_article jbmodel = model as stwh_article;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_article set ");
            strSql.Append("stwh_artid=@stwh_artid,");
            strSql.Append("stwh_attitle=@stwh_attitle,");
            strSql.Append("stwh_atimage=@stwh_atimage,");
            strSql.Append("stwh_atauthor=@stwh_atauthor,");
            strSql.Append("stwh_atsource=@stwh_atsource,");
            strSql.Append("stwh_atjianjie=@stwh_atjianjie,");
            strSql.Append("stwh_atcontent=@stwh_atcontent,");
            strSql.Append("stwh_atbiaoqian=@stwh_atbiaoqian,");
            strSql.Append("stwh_atissh=@stwh_atissh,");
            strSql.Append("stwh_atiszhiding=@stwh_atiszhiding,");
            strSql.Append("stwh_attuijian=@stwh_attuijian,");
            strSql.Append("stwh_attoutiao=@stwh_attoutiao,");
            strSql.Append("stwh_atgundong=@stwh_atgundong,");
            strSql.Append("stwh_atseotitle=@stwh_atseotitle,");
            strSql.Append("stwh_atseokeywords=@stwh_atseokeywords,");
            strSql.Append("stwh_atseodescription=@stwh_atseodescription,");
            strSql.Append("stwh_ataddtime=@stwh_ataddtime,");
            strSql.Append("stwh_atorder=@stwh_atorder,");
            strSql.Append("stwh_attitlesimple=@stwh_attitlesimple,");
            strSql.Append("stwh_atimglist=@stwh_atimglist,");
            strSql.Append("stwh_atstyle=@stwh_atstyle,");
            strSql.Append("stwh_atvideourl=@stwh_atvideourl,");
            strSql.Append("stwh_atsourceurl=@stwh_atsourceurl");
            strSql.Append(" where stwh_atid=@stwh_atid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_artid", SqlDbType.Int,4),
					new SqlParameter("@stwh_attitle", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_atimage", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atauthor", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atsource", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atjianjie", SqlDbType.NVarChar,400),
					new SqlParameter("@stwh_atcontent", SqlDbType.NText),
					new SqlParameter("@stwh_atbiaoqian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_atissh", SqlDbType.Int,4),
					new SqlParameter("@stwh_atiszhiding", SqlDbType.Int,4),
					new SqlParameter("@stwh_attuijian", SqlDbType.Int,4),
					new SqlParameter("@stwh_attoutiao", SqlDbType.Int,4),
					new SqlParameter("@stwh_atgundong", SqlDbType.Int,4),
					new SqlParameter("@stwh_atseotitle", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_atseokeywords", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_atseodescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ataddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_atorder", SqlDbType.Int,4),
                    new SqlParameter("@stwh_attitlesimple", SqlDbType.NVarChar,200),
                    new SqlParameter("@stwh_atimglist", SqlDbType.NText),
                    new SqlParameter("@stwh_atstyle", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_atvideourl", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_atsourceurl", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_atid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_artid;
            parameters[1].Value = jbmodel.stwh_attitle;
            parameters[2].Value = jbmodel.stwh_atimage;
            parameters[3].Value = jbmodel.stwh_atauthor;
            parameters[4].Value = jbmodel.stwh_atsource;
            parameters[5].Value = jbmodel.stwh_atjianjie;
            parameters[6].Value = jbmodel.stwh_atcontent;
            parameters[7].Value = jbmodel.stwh_atbiaoqian;
            parameters[8].Value = jbmodel.stwh_atissh;
            parameters[9].Value = jbmodel.stwh_atiszhiding;
            parameters[10].Value = jbmodel.stwh_attuijian;
            parameters[11].Value = jbmodel.stwh_attoutiao;
            parameters[12].Value = jbmodel.stwh_atgundong;
            parameters[13].Value = jbmodel.stwh_atseotitle;
            parameters[14].Value = jbmodel.stwh_atseokeywords;
            parameters[15].Value = jbmodel.stwh_atseodescription;
            parameters[16].Value = jbmodel.stwh_ataddtime;
            parameters[17].Value = jbmodel.stwh_atorder;
            parameters[18].Value = jbmodel.stwh_attitlesimple;
            parameters[19].Value = jbmodel.stwh_atimglist;
            parameters[20].Value = jbmodel.stwh_atstyle;
            parameters[21].Value = jbmodel.stwh_atvideourl;
            parameters[22].Value = jbmodel.stwh_atsourceurl;
            parameters[23].Value = jbmodel.stwh_atid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_atid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_article ");
            strSql.Append(" where stwh_atid=@stwh_atid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_atid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_atid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_atidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_article ");
            strSql.Append(" where stwh_atid in (" + stwh_atidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_atid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_atid,stwh_artid,stwh_attitle,stwh_atimage,stwh_atauthor,stwh_atsource,stwh_atjianjie,stwh_atcontent,stwh_atbiaoqian,stwh_atissh,stwh_atiszhiding,stwh_attuijian,stwh_attoutiao,stwh_atgundong,stwh_atseotitle,stwh_atseokeywords,stwh_atseodescription,stwh_ataddtime,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_atorder,stwh_attitlesimple,stwh_atimglist,stwh_artorder,stwh_atstyle,stwh_atvideourl,stwh_atsourceurl from view_article ");
            strSql.Append(" where stwh_atid=@stwh_atid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_atid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_atid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_article model = new stwh_article();
            if (row != null)
            {
                if (row["stwh_atid"] != null)
                {
                    model.stwh_atid = int.Parse(row["stwh_atid"].ToString());
                }
                if (row["stwh_artid"] != null)
                {
                    model.stwh_artid = int.Parse(row["stwh_artid"].ToString());
                }
                if (row["stwh_attitlesimple"] != null)
                {
                    model.stwh_attitlesimple = row["stwh_attitlesimple"].ToString();
                }
                if (row["stwh_attitle"] != null)
                {
                    model.stwh_attitle = row["stwh_attitle"].ToString();
                }
                if (row["stwh_atimage"] != null)
                {
                    model.stwh_atimage = row["stwh_atimage"].ToString();
                }
                if (row["stwh_atimglist"] != null)
                {
                    model.stwh_atimglist = row["stwh_atimglist"].ToString();
                }
                if (row["stwh_atauthor"] != null)
                {
                    model.stwh_atauthor = row["stwh_atauthor"].ToString();
                }
                if (row["stwh_atsource"] != null)
                {
                    model.stwh_atsource = row["stwh_atsource"].ToString();
                }
                if (row["stwh_atjianjie"] != null)
                {
                    model.stwh_atjianjie = row["stwh_atjianjie"].ToString();
                }
                if (row["stwh_atcontent"] != null)
                {
                    model.stwh_atcontent = row["stwh_atcontent"].ToString();
                }
                if (row["stwh_atbiaoqian"] != null)
                {
                    model.stwh_atbiaoqian = row["stwh_atbiaoqian"].ToString();
                }
                if (row["stwh_atissh"] != null)
                {
                    model.stwh_atissh = int.Parse(row["stwh_atissh"].ToString());
                }
                if (row["stwh_atiszhiding"] != null)
                {
                    model.stwh_atiszhiding = int.Parse(row["stwh_atiszhiding"].ToString());
                }
                if (row["stwh_attuijian"] != null)
                {
                    model.stwh_attuijian = int.Parse(row["stwh_attuijian"].ToString());
                }
                if (row["stwh_atorder"] != null)
                {
                    model.stwh_atorder = int.Parse(row["stwh_atorder"].ToString());
                }
                if (row["stwh_attoutiao"] != null)
                {
                    model.stwh_attoutiao = int.Parse(row["stwh_attoutiao"].ToString());
                }
                if (row["stwh_atgundong"] != null)
                {
                    model.stwh_atgundong = int.Parse(row["stwh_atgundong"].ToString());
                }
                if (row["stwh_atseotitle"] != null)
                {
                    model.stwh_atseotitle = row["stwh_atseotitle"].ToString();
                }
                if (row["stwh_atseokeywords"] != null)
                {
                    model.stwh_atseokeywords = row["stwh_atseokeywords"].ToString();
                }
                if (row["stwh_atseodescription"] != null)
                {
                    model.stwh_atseodescription = row["stwh_atseodescription"].ToString();
                }
                if (row["stwh_ataddtime"] != null)
                {
                    model.stwh_ataddtime = DateTime.Parse(row["stwh_ataddtime"].ToString());
                }
                if (row["stwh_atstyle"] != null)
                {
                    model.stwh_atstyle = row["stwh_atstyle"].ToString();
                }
                if (row["stwh_atvideourl"] != null)
                {
                    model.stwh_atvideourl = row["stwh_atvideourl"].ToString();
                }
                if (row["stwh_atsourceurl"] != null)
                {
                    model.stwh_atsourceurl = row["stwh_atsourceurl"].ToString();
                }
                if (row.ItemArray.Length > 24)
                {
                    if (row["stwh_artname"] != null)
                    {
                        model.stwh_artname = row["stwh_artname"].ToString();
                    }
                    if (row["stwh_artdescription"] != null)
                    {
                        model.stwh_artdescription = row["stwh_artdescription"].ToString();
                    }
                    if (row["stwh_artimg"] != null)
                    {
                        model.stwh_artimg = row["stwh_artimg"].ToString();
                    }
                    if (row["stwh_artparentid"] != null)
                    {
                        model.stwh_artparentid = int.Parse(row["stwh_artparentid"].ToString());
                    }
                    if (row["stwh_artorder"] != null)
                    {
                        model.stwh_artorder = int.Parse(row["stwh_artorder"].ToString());
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_atid,stwh_artid,stwh_attitle,stwh_atimage,stwh_atauthor,stwh_atsource,stwh_atjianjie,stwh_atcontent,stwh_atbiaoqian,stwh_atissh,stwh_atiszhiding,stwh_attuijian,stwh_attoutiao,stwh_atgundong,stwh_atseotitle,stwh_atseokeywords,stwh_atseodescription,stwh_ataddtime,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_atorder,stwh_attitlesimple,stwh_atimglist,stwh_artorder,stwh_atstyle,stwh_atvideourl,stwh_atsourceurl ");
            strSql.Append(" FROM view_article ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_atid,stwh_artid,stwh_attitle,stwh_atimage,stwh_atauthor,stwh_atsource,stwh_atjianjie,stwh_atcontent,stwh_atbiaoqian,stwh_atissh,stwh_atiszhiding,stwh_attuijian,stwh_attoutiao,stwh_atgundong,stwh_atseotitle,stwh_atseokeywords,stwh_atseodescription,stwh_ataddtime,stwh_artname,stwh_artdescription,stwh_artimg,stwh_artparentid,stwh_atorder,stwh_attitlesimple,stwh_atimglist,stwh_artorder,stwh_atstyle,stwh_atvideourl,stwh_atsourceurl ");
            strSql.Append(" FROM view_article ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_article ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_atid desc");
            }
            strSql.Append(")AS Row, T.*  from view_article T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

