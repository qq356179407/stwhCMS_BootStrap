﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_menuinfo
    /// </summary>
    public partial class stwh_menuinfoDAL : BaseDAL, Istwh_menuinfoDAL
    {
        public stwh_menuinfoDAL()
        { }
        #region Istwh_menuinfoDAL接口实现方法
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(int stwh_rid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder ");
            strSql.Append(" FROM view_role_menu ");
            strSql.Append(" where stwh_rid = " + stwh_rid + " and stwh_menustatus = 0");

            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion
        #region  IBaseDAL接口实现方法
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_menuid", "stwh_menuinfo");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_menuid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_menuinfo");
            strSql.Append(" where stwh_menuid=@stwh_menuid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_menuid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_menuinfo jbmodel = model as stwh_menuinfo;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_menuinfo(");
            strSql.Append("stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_menuname,@stwh_menunameUS,@stwh_menuICO_url,@stwh_menuURL,@stwh_menuparentID,@stwh_menustatus,@stwh_menudescription,@stwh_menuorder)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuname", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_menunameUS", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_menuICO_url", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_menuURL", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_menuparentID", SqlDbType.Int,4),
					new SqlParameter("@stwh_menustatus", SqlDbType.Int,4),
					new SqlParameter("@stwh_menudescription", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_menuorder", SqlDbType.Int,4)
            };
            parameters[0].Value = jbmodel.stwh_menuname;
            parameters[1].Value = jbmodel.stwh_menunameUS;
            parameters[2].Value = jbmodel.stwh_menuICO_url;
            parameters[3].Value = jbmodel.stwh_menuURL;
            parameters[4].Value = jbmodel.stwh_menuparentID;
            parameters[5].Value = jbmodel.stwh_menustatus;
            parameters[6].Value = jbmodel.stwh_menudescription;
            parameters[7].Value = jbmodel.stwh_menuorder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_menuinfo jbmodel = model as stwh_menuinfo;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_menuinfo set ");
            strSql.Append("stwh_menuname=@stwh_menuname,");
            strSql.Append("stwh_menunameUS=@stwh_menunameUS,");
            strSql.Append("stwh_menuICO_url=@stwh_menuICO_url,");
            strSql.Append("stwh_menuURL=@stwh_menuURL,");
            strSql.Append("stwh_menuparentID=@stwh_menuparentID,");
            strSql.Append("stwh_menustatus=@stwh_menustatus,");
            strSql.Append("stwh_menudescription=@stwh_menudescription,");
            strSql.Append("stwh_menuorder=@stwh_menuorder");
            strSql.Append(" where stwh_menuid=@stwh_menuid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuname", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_menunameUS", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_menuICO_url", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_menuURL", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_menuparentID", SqlDbType.Int,4),
					new SqlParameter("@stwh_menustatus", SqlDbType.Int,4),
					new SqlParameter("@stwh_menudescription", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_menuorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_menuname;
            parameters[1].Value = jbmodel.stwh_menunameUS;
            parameters[2].Value = jbmodel.stwh_menuICO_url;
            parameters[3].Value = jbmodel.stwh_menuURL;
            parameters[4].Value = jbmodel.stwh_menuparentID;
            parameters[5].Value = jbmodel.stwh_menustatus;
            parameters[6].Value = jbmodel.stwh_menudescription;
            parameters[7].Value = jbmodel.stwh_menuorder;
            parameters[8].Value = jbmodel.stwh_menuid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_menuid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_menuinfo ");
            strSql.Append(" where stwh_menuid=@stwh_menuid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_menuid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_menuidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_menuinfo ");
            strSql.Append(" where stwh_menuid in (" + stwh_menuidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_menuid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder from stwh_menuinfo ");
            strSql.Append(" where stwh_menuid=@stwh_menuid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_menuid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_menuinfo jbmodel = new stwh_menuinfo();
            if (row != null)
            {
                if (row["stwh_menuid"] != null)
                {
                    jbmodel.stwh_menuid = int.Parse(row["stwh_menuid"].ToString());
                }
                if (row["stwh_menuname"] != null)
                {
                    jbmodel.stwh_menuname = row["stwh_menuname"].ToString();
                }
                if (row["stwh_menunameUS"] != null)
                {
                    jbmodel.stwh_menunameUS = row["stwh_menunameUS"].ToString();
                }
                if (row["stwh_menuICO_url"] != null)
                {
                    jbmodel.stwh_menuICO_url = row["stwh_menuICO_url"].ToString();
                }
                if (row["stwh_menuURL"] != null)
                {
                    jbmodel.stwh_menuURL = row["stwh_menuURL"].ToString();
                }
                if (row["stwh_menuparentID"] != null)
                {
                    jbmodel.stwh_menuparentID = int.Parse(row["stwh_menuparentID"].ToString());
                }
                if (row["stwh_menustatus"] != null)
                {
                    jbmodel.stwh_menustatus = int.Parse(row["stwh_menustatus"].ToString());
                }
                if (row["stwh_menuorder"] != null)
                {
                    jbmodel.stwh_menuorder = int.Parse(row["stwh_menuorder"].ToString());
                }
                if (row["stwh_menudescription"] != null)
                {
                    jbmodel.stwh_menudescription = row["stwh_menudescription"].ToString();
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder ");
            strSql.Append(" FROM stwh_menuinfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder ");
            strSql.Append(" FROM stwh_menuinfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_menuinfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_menuid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_menuinfo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

