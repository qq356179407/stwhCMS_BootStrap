﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_blogroll
    /// </summary>
    public partial class stwh_blogrollDAL : BaseDAL, Istwh_blogrollDAL
    {
        public stwh_blogrollDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_blogroll");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_blogroll", "stwh_blid", FieldColumn, FieldOrder, "stwh_blid,stwh_blname,stwh_blimg,stwh_blurl,stwh_blremark,stwh_blorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_blogroll where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_blogroll", "stwh_blid", FieldColumn, FieldOrder, "stwh_blid,stwh_blname,stwh_blimg,stwh_blurl,stwh_blremark,stwh_blorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_blid", "stwh_blogroll");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_blid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_blogroll");
            strSql.Append(" where stwh_blid=@stwh_blid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_blid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_blid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_blogroll jbmodel = model as stwh_blogroll;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_blogroll(");
            strSql.Append("stwh_blname,stwh_blimg,stwh_blurl,stwh_blremark,stwh_blorder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_blname,@stwh_blimg,@stwh_blurl,@stwh_blremark,@stwh_blorder)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_blname", SqlDbType.NVarChar,100),
                    new SqlParameter("@stwh_blimg", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_blurl", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_blremark", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_blorder", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_blname;
            parameters[1].Value = jbmodel.stwh_blimg;
            parameters[2].Value = jbmodel.stwh_blurl;
            parameters[3].Value = jbmodel.stwh_blremark;
            parameters[4].Value = jbmodel.stwh_blorder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_blogroll jbmodel = model as stwh_blogroll;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_blogroll set ");
            strSql.Append("stwh_blname=@stwh_blname,");
            strSql.Append("stwh_blimg=@stwh_blimg,");
            strSql.Append("stwh_blurl=@stwh_blurl,");
            strSql.Append("stwh_blremark=@stwh_blremark,");
            strSql.Append("stwh_blorder=@stwh_blorder");
            strSql.Append(" where stwh_blid=@stwh_blid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_blname", SqlDbType.NVarChar,100),
                    new SqlParameter("@stwh_blimg", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_blurl", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_blremark", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_blorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_blid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_blname;
            parameters[1].Value = jbmodel.stwh_blimg;
            parameters[2].Value = jbmodel.stwh_blurl;
            parameters[3].Value = jbmodel.stwh_blremark;
            parameters[4].Value = jbmodel.stwh_blorder;
            parameters[5].Value = jbmodel.stwh_blid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_blid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_blogroll ");
            strSql.Append(" where stwh_blid=@stwh_blid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_blid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_blid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_blidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_blogroll ");
            strSql.Append(" where stwh_blid in (" + stwh_blidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_blid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_blid,stwh_blname,stwh_blimg,stwh_blurl,stwh_blremark,stwh_blorder from stwh_blogroll ");
            strSql.Append(" where stwh_blid=@stwh_blid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_blid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_blid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_blogroll model = new stwh_blogroll();
            if (row != null)
            {
                if (row["stwh_blid"] != null)
                {
                    model.stwh_blid = int.Parse(row["stwh_blid"].ToString());
                }
                if (row["stwh_blname"] != null)
                {
                    model.stwh_blname = row["stwh_blname"].ToString();
                }
                if (row["stwh_blimg"] != null)
                {
                    model.stwh_blimg = row["stwh_blimg"].ToString();
                }
                if (row["stwh_blurl"] != null)
                {
                    model.stwh_blurl = row["stwh_blurl"].ToString();
                }
                if (row["stwh_blremark"] != null)
                {
                    model.stwh_blremark = row["stwh_blremark"].ToString();
                }
                if (row["stwh_blorder"] != null)
                {
                    model.stwh_blorder = int.Parse(row["stwh_blorder"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_blid,stwh_blname,stwh_blimg,stwh_blurl,stwh_blremark,stwh_blorder ");
            strSql.Append(" FROM stwh_blogroll ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_blid,stwh_blname,stwh_blimg,stwh_blurl,stwh_blremark,stwh_blorder ");
            strSql.Append(" FROM stwh_blogroll ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_blogroll ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_blid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_blogroll T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

