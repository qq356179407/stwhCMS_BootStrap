﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using stwh_Model;

namespace stwh_Common
{
    /// <summary>
    /// 邮件发送类
    /// </summary>
    public class MailSender
    {
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="server">SMTP 服务器</param>
        /// <param name="sender">发送人</param>
        /// <param name="recipient">收件人</param>
        /// <param name="subject">主题</param>
        /// <param name="body">内容</param>
        /// <param name="isBodyHtml">内容是否是html格式的值</param>
        /// <param name="encoding">邮件编码</param>
        /// <param name="isAuthentication">是否验证发件人身份凭据</param>
        /// <param name="files">附件</param>
        public static void Send(string server, string sender, string recipient, string subject, string body, bool isBodyHtml, Encoding encoding, bool isAuthentication, params string[] files)
        {
            SmtpClient smtpClient = new SmtpClient(server);
            MailMessage message = new MailMessage(sender, recipient);
            message.IsBodyHtml = isBodyHtml;

            message.SubjectEncoding = encoding;
            message.BodyEncoding = encoding;

            message.Subject = subject;
            message.Body = body;

            message.Attachments.Clear();
            if (files != null && files.Length != 0)
            {
                for (int i = 0; i < files.Length; ++i)
                {
                    Attachment attach = new Attachment(files[i]);
                    message.Attachments.Add(attach);
                }
            }

            if (isAuthentication == true)
            {
                smtpClient.Credentials = new NetworkCredential(SmtpConfig.Create().SmtpSetting.User,
                    SmtpConfig.Create().SmtpSetting.Password);
            }
            smtpClient.Send(message);
        }

        /// <summary>
        /// 通过有SSL验证的SMTP发送
        /// </summary>
        /// <param name="server">SMTP服务器或者url</param>
        /// <param name="port">端口号</param>
        /// <param name="smtpUserName">SMTP登陆用户</param>
        /// <param name="smtpUserPwd">SMTP登陆密码</param>
        /// <param name="from">发件人邮箱地址</param>
        /// <param name="fromName">发件人显示名称</param>
        /// <param name="To">收件人</param>
        /// <param name="Cc">抄送人</param>
        /// <param name="Bcc">密抄送</param>
        /// <param name="Subject">主题</param>
        /// <param name="Body">内容</param>
        /// <param name="Encoding">编码</param>
        /// <param name="IsHtml">内容是否是HTML</param>
        /// <param name="Priority">优先级</param>
        /// <param name="AttachFiles">附件列表</param>
        public static void Send(string server,int port,string smtpUserName,string smtpUserPwd ,string from,string fromName,string To, string Cc, string Bcc, string Subject, string Body, string Encoding, bool IsHtml, string Priority, string[] AttachFiles)
        {
            //创建MailMessage
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            //收件人
            if (To != null)
            {
                string[] to = To.Split(',', '，', ';', '；', ' ', '　', '|', '｜');
                foreach (string t in to)
                {
                    msg.To.Add(t);
                }
            }
            //抄送
            if (Cc != null)
            {
                string[] cc = Cc.Split(',', '，', ';', '；', ' ', '　', '|', '｜');
                foreach (string c in cc)
                {
                    msg.To.Add(c);
                }
            }
            //密抄送
            if (Bcc != null)
            {
                string[] bcc = Bcc.Split(',', '，', ';', '；', ' ', '　', '|', '｜');
                foreach (string b in bcc)
                {
                    msg.To.Add(b);
                }
            }
            //主题
            msg.Subject = Subject;
            //内容
            msg.Body = Body;
            //内容编码
            if (Encoding == null)
            {
                msg.BodyEncoding = System.Text.Encoding.UTF8;
            }
            else
            {
                try
                {
                    msg.BodyEncoding = System.Text.Encoding.GetEncoding(Encoding);
                }
                catch
                {
                    msg.BodyEncoding = System.Text.Encoding.UTF8;
                }
            }
            //发件人地址
            msg.From = new System.Net.Mail.MailAddress(from, fromName, msg.BodyEncoding);
            //邮件内容类型
            msg.IsBodyHtml = IsHtml;
            //设置优先级
            if (Priority == null)
            {
                msg.Priority = System.Net.Mail.MailPriority.Normal;
            }
            else
            {
                switch (Priority.ToLower())
                {
                    case "high":
                        msg.Priority = System.Net.Mail.MailPriority.High;
                        break;
                    case "normal":
                        msg.Priority = System.Net.Mail.MailPriority.Normal;
                        break;
                    case "low":
                        msg.Priority = System.Net.Mail.MailPriority.Low;
                        break;
                    default:
                        msg.Priority = System.Net.Mail.MailPriority.Normal;
                        break;
                }
            }

            //添加附件
            if (AttachFiles != null)
            {
                foreach (string attach in AttachFiles)
                {
                    System.Net.Mail.Attachment mailAttach = new System.Net.Mail.Attachment(attach);
                    msg.Attachments.Add(mailAttach);
                }
            }


            //SMTP协议发送邮件
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            //SMTP主机的名称或者IP
            client.Host = server;
            //用于发送邮件邮箱的用户名和密码
            client.Credentials = new System.Net.NetworkCredential(smtpUserName, smtpUserPwd);
            //SSL邮件服务使用的端口
            client.Port = port;
            //经过ssl加密
            client.EnableSsl = true;
            //发送
            client.Send(msg);
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="recipient">收件人</param>
        /// <param name="subject">主题</param>
        /// <param name="body">内容</param>
        public static void Send(string recipient, string subject, string body)
        {
            try
            {
                Send(SmtpConfig.Create().SmtpSetting.Server, SmtpConfig.Create().SmtpSetting.Sender, recipient, subject, body, true, Encoding.UTF8, true, null);
            }
            catch (Exception)
            {
                Send(SmtpConfig.Create().SmtpSetting.Server, SmtpConfig.Create().SmtpSetting.Port, SmtpConfig.Create().SmtpSetting.User, SmtpConfig.Create().SmtpSetting.Password, SmtpConfig.Create().SmtpSetting.Sender, SmtpConfig.Create().SmtpSetting.SenderName, recipient, null, null, subject, body, "utf-8", true, "high", null);
            }
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="Recipient">收件人</param>
        /// <param name="Sender">发件人</param>
        /// <param name="Subject">主题</param>
        /// <param name="Body">内容</param>
        public static void Send(string Recipient, string Sender, string Subject, string Body)
        {
            try
            {
                Send(SmtpConfig.Create().SmtpSetting.Server, Sender, Recipient, Subject, Body, true, Encoding.UTF8, true, null);
            }
            catch (Exception)
            {
                Send(SmtpConfig.Create().SmtpSetting.Server, SmtpConfig.Create().SmtpSetting.Port, SmtpConfig.Create().SmtpSetting.User, SmtpConfig.Create().SmtpSetting.Password, Sender, SmtpConfig.Create().SmtpSetting.SenderName, Recipient, null, null, Subject, Body, "utf-8", true, "high", null);
            }
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="body">正文内容</param>
        /// <param name="to">收件人列表（英文逗号分隔）</param>
        /// <param name="flag">标识</param>
        public void Send(string subject, string body,string to,bool flag)
        {
            string smtpServer = SmtpConfig.Create().SmtpSetting.Server;
            string userName = SmtpConfig.Create().SmtpSetting.User;
            string pwd = SmtpConfig.Create().SmtpSetting.Password;
            int smtpPort = SmtpConfig.Create().SmtpSetting.Port;
            string authorName = SmtpConfig.Create().SmtpSetting.Sender;

            List<string> toList = StringPlus.GetSubStringList(StringPlus.ToDBC(to), ',');
            OpenSmtp.Mail.Smtp smtp = new OpenSmtp.Mail.Smtp(smtpServer, userName, pwd, smtpPort);
            foreach (string s in toList)
            {
                OpenSmtp.Mail.MailMessage msg = new OpenSmtp.Mail.MailMessage();
                msg.From = new OpenSmtp.Mail.EmailAddress(userName, authorName);

                //添加收件人
                msg.AddRecipient(s, OpenSmtp.Mail.AddressType.To);

                //设置邮件正文,并指定格式为 html 格式
                msg.HtmlBody = body;
                //设置邮件标题
                msg.Subject = subject;
                //指定邮件正文的编码
                msg.Charset = "utf-8";
                //发送邮件
                smtp.SendMail(msg);
            }
        }
    }
}
