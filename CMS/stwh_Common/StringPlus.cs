﻿using System;
using System.Collections.Generic;
using System.Text;

namespace stwh_Common
{
    public class StringPlus
    {
        /// <summary>
        /// 微信表情字符串代码
        /// </summary>
        private static List<string> brow = new List<string>() {
            "/:oY","/:skip","/:turn","/:kotow","/:circle","/:<O>","/:shake","/:jump","/:<L>","/:love",
            "/:ok","/:no","/:lvu","/:bad","/:@@","/:jj","/:@)","/:v","/:share","/:weak",
            "/:strong","/:hug","/:gift","/:sun","/:moon","/:shit","/:ladybug","/:footb","/:kn","/:bome",
            "/:li","/:cake","/:break","/:heart","/:showlove","/:fade","/:rose","/:pig","/:eat","/:coffee",
            "/:oo","/:basketb","/:beer","/:<W>","/:pd","/:8*","/:@x","/::*","/:X-)","/::'|",
            "/:P-(","/:>-|","/::-O","/:@>","/:<@","/:B-)","/:&-(","/:handclap","/:dig","/:wipe",
            "/:bye","/:xx","/:!!!","/:,@!","/::8","/:,@@","/:,@x","/:?","/::-S","/:,@f",
            "/::,@","/::>","/::L","/::!","/:|-)","/::g","/:,@o","/::d","/:,@-D","/:,@P",
            "/::T","/::Q","/:--b","/::+","/::(","/::O","/::D","/::P","/::@","/::-|",
            "/::'(","/::Z","/::X","/::$","/::<","/:8-)","/::|","/::B","/::~","/::)"
        };

        private static string[] Day = new string[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        private static string[] Letters = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

        /// <summary>
        /// 将1-26的数字转换为字母
        /// </summary>
        /// <param name="num">1-26的数字</param>
        /// <param name="isLower">是否将字母转换为小写（默认大写false，true小写）</param>
        /// <returns></returns>
        public static string NumToLetter(int num, bool isLower)
        {
            if (num < 0 || num > 26)
            {
                if (isLower) return "a";
                else return "A";
            }
            else
            {
                if (isLower) return Letters[num - 1].ToLower();
                else return Letters[num - 1];
            }
        }

        /// <summary>
        /// 获取表情代码字符串
        /// </summary>
        /// <returns></returns>
        public static List<string> GetBrow()
        {
            return brow;
        }

        /// <summary>
        /// 向末尾添加表情代码字符串,必须与SetImageBrow函数结合使用
        /// </summary>
        /// <param name="brows"></param>
        public static void SetBrow(string brows)
        {
            brow.Add(brows);
        }

        /// <summary>
        /// 微信表情字符串代码对应图片
        /// </summary>
        private static List<string> Imagebrow = new List<string>()
        {
            #region image
            "<img src=\"../../Content/wxbq/e199.gif\"/>",
            "<img src=\"../../Content/wxbq/e198.gif\"/>",
            "<img src=\"../../Content/wxbq/e197.gif\"/>",
            "<img src=\"../../Content/wxbq/e196.gif\"/>",
            "<img src=\"../../Content/wxbq/e195.gif\"/>",
            "<img src=\"../../Content/wxbq/e194.gif\"/>",
            "<img src=\"../../Content/wxbq/e193.gif\"/>",
            "<img src=\"../../Content/wxbq/e192.gif\"/>",
            "<img src=\"../../Content/wxbq/e191.gif\"/>",
            "<img src=\"../../Content/wxbq/e190.gif\"/>",
            "<img src=\"../../Content/wxbq/e189.gif\"/>",
            "<img src=\"../../Content/wxbq/e188.gif\"/>",
            "<img src=\"../../Content/wxbq/e187.gif\"/>",
            "<img src=\"../../Content/wxbq/e186.gif\"/>",
            "<img src=\"../../Content/wxbq/e185.gif\"/>",
            "<img src=\"../../Content/wxbq/e184.gif\"/>",
            "<img src=\"../../Content/wxbq/e183.gif\"/>",
            "<img src=\"../../Content/wxbq/e182.gif\"/>",
            "<img src=\"../../Content/wxbq/e181.gif\"/>",
            "<img src=\"../../Content/wxbq/e180.gif\"/>",
            "<img src=\"../../Content/wxbq/e179.gif\"/>",
            "<img src=\"../../Content/wxbq/e178.gif\"/>",
            "<img src=\"../../Content/wxbq/e177.gif\"/>",
            "<img src=\"../../Content/wxbq/e176.gif\"/>",
            "<img src=\"../../Content/wxbq/e175.gif\"/>",
            "<img src=\"../../Content/wxbq/e174.gif\"/>",
            "<img src=\"../../Content/wxbq/e173.gif\"/>",
            "<img src=\"../../Content/wxbq/e172.gif\"/>",
            "<img src=\"../../Content/wxbq/e171.gif\"/>",
            "<img src=\"../../Content/wxbq/e170.gif\"/>",
            "<img src=\"../../Content/wxbq/e169.gif\"/>",
            "<img src=\"../../Content/wxbq/e168.gif\"/>",
            "<img src=\"../../Content/wxbq/e167.gif\"/>",
            "<img src=\"../../Content/wxbq/e166.gif\"/>",
            "<img src=\"../../Content/wxbq/e165.gif\"/>",
            "<img src=\"../../Content/wxbq/e164.gif\"/>",
            "<img src=\"../../Content/wxbq/e163.gif\"/>",
            "<img src=\"../../Content/wxbq/e162.gif\"/>",
            "<img src=\"../../Content/wxbq/e161.gif\"/>",
            "<img src=\"../../Content/wxbq/e160.gif\"/>",
            "<img src=\"../../Content/wxbq/e159.gif\"/>",
            "<img src=\"../../Content/wxbq/e158.gif\"/>",
            "<img src=\"../../Content/wxbq/e157.gif\"/>",
            "<img src=\"../../Content/wxbq/e156.gif\"/>",
            "<img src=\"../../Content/wxbq/e155.gif\"/>",
            "<img src=\"../../Content/wxbq/e154.gif\"/>",
            "<img src=\"../../Content/wxbq/e153.gif\"/>",
            "<img src=\"../../Content/wxbq/e152.gif\"/>",
            "<img src=\"../../Content/wxbq/e151.gif\"/>",
            "<img src=\"../../Content/wxbq/e150.gif\"/>",
            "<img src=\"../../Content/wxbq/e149.gif\"/>",
            "<img src=\"../../Content/wxbq/e148.gif\"/>",
            "<img src=\"../../Content/wxbq/e147.gif\"/>",
            "<img src=\"../../Content/wxbq/e146.gif\"/>",
            "<img src=\"../../Content/wxbq/e145.gif\"/>",
            "<img src=\"../../Content/wxbq/e144.gif\"/>",
            "<img src=\"../../Content/wxbq/e143.gif\"/>",
            "<img src=\"../../Content/wxbq/e142.gif\"/>",
            "<img src=\"../../Content/wxbq/e141.gif\"/>",
            "<img src=\"../../Content/wxbq/e140.gif\"/>",
            "<img src=\"../../Content/wxbq/e139.gif\"/>",
            "<img src=\"../../Content/wxbq/e138.gif\"/>",
            "<img src=\"../../Content/wxbq/e137.gif\"/>",
            "<img src=\"../../Content/wxbq/e136.gif\"/>",
            "<img src=\"../../Content/wxbq/e135.gif\"/>",
            "<img src=\"../../Content/wxbq/e134.gif\"/>",
            "<img src=\"../../Content/wxbq/e133.gif\"/>",
            "<img src=\"../../Content/wxbq/e132.gif\"/>",
            "<img src=\"../../Content/wxbq/e131.gif\"/>",
            "<img src=\"../../Content/wxbq/e130.gif\"/>",
            "<img src=\"../../Content/wxbq/e129.gif\"/>",
            "<img src=\"../../Content/wxbq/e128.gif\"/>",
            "<img src=\"../../Content/wxbq/e127.gif\"/>",
            "<img src=\"../../Content/wxbq/e126.gif\"/>",
            "<img src=\"../../Content/wxbq/e125.gif\"/>",
            "<img src=\"../../Content/wxbq/e124.gif\"/>",
            "<img src=\"../../Content/wxbq/e123.gif\"/>",
            "<img src=\"../../Content/wxbq/e122.gif\"/>",
            "<img src=\"../../Content/wxbq/e121.gif\"/>",
            "<img src=\"../../Content/wxbq/e120.gif\"/>",
            "<img src=\"../../Content/wxbq/e119.gif\"/>",
            "<img src=\"../../Content/wxbq/e118.gif\"/>",
            "<img src=\"../../Content/wxbq/e117.gif\"/>",
            "<img src=\"../../Content/wxbq/e116.gif\"/>",
            "<img src=\"../../Content/wxbq/e115.gif\"/>",
            "<img src=\"../../Content/wxbq/e114.gif\"/>",
            "<img src=\"../../Content/wxbq/e113.gif\"/>",
            "<img src=\"../../Content/wxbq/e112.gif\"/>",
            "<img src=\"../../Content/wxbq/e111.gif\"/>",
            "<img src=\"../../Content/wxbq/e110.gif\"/>",
            "<img src=\"../../Content/wxbq/e109.gif\"/>",
            "<img src=\"../../Content/wxbq/e108.gif\"/>",
            "<img src=\"../../Content/wxbq/e107.gif\"/>",
            "<img src=\"../../Content/wxbq/e106.gif\"/>",
            "<img src=\"../../Content/wxbq/e105.gif\"/>",
            "<img src=\"../../Content/wxbq/e104.gif\"/>",
            "<img src=\"../../Content/wxbq/e103.gif\"/>",
            "<img src=\"../../Content/wxbq/e102.gif\"/>",
            "<img src=\"../../Content/wxbq/e101.gif\"/>",
            "<img src=\"../../Content/wxbq/e100.gif\"/>"
            #endregion
        };

        /// <summary>
        /// 获取表情代码字符串对应图片
        /// </summary>
        /// <returns></returns>
        public static List<string> GetImageBrow()
        {
            return Imagebrow;
        }

        /// <summary>
        /// 向末尾添加表情代码字符串对应图片,必须与SetBrow函数结合使用
        /// </summary>
        /// <param name="brows"></param>
        public static void SetImageBrow(string brows)
        {
            Imagebrow.Add(brows);
        }

        public static List<string> GetStrArray(string str, char speater, bool toLower)
        {
            List<string> list = new List<string>();
            string[] ss = str.Split(speater);
            foreach (string s in ss)
            {
                if (!string.IsNullOrEmpty(s) && s != speater.ToString())
                {
                    string strVal = s;
                    if (toLower)
                    {
                        strVal = s.ToLower();
                    }
                    list.Add(strVal);
                }
            }
            return list;
        }

        public static string[] GetStrArray(string str)
        {
            return str.Split(new char[',']);
        }

        public static string GetArrayStr(List<string> list, string speater)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                if (i == list.Count - 1)
                {
                    sb.Append(list[i]);
                }
                else
                {
                    sb.Append(list[i]);
                    sb.Append(speater);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// 将内容中出现的微信表情代码字符串替换为图片
        /// </summary>
        /// <param name="original">源内容</param>
        /// <returns></returns>
        public static string ReplaceBrow(string original)
        {
            //获取表情代码字符串
            List<string> strlist = GetBrow();
            List<string> imglist = GetImageBrow();

            if (strlist.Count == 0 || imglist.Count == 0)
            {
                return original;
            }
            string result = original;

            for (int i = 0; i < strlist.Count; i++)
            {
                result = ReplaceEx(result, strlist[i], imglist[i]);
            }

            return result;
        }

        /// <summary>
        /// 将内容中出现的微信表情图片替换为代码
        /// </summary>
        /// <param name="original">源内容</param>
        /// <returns></returns>
        public static string ReplaceText(string original)
        {
            //获取表情代码字符串
            List<string> strlist = GetBrow();
            List<string> imglist = GetImageBrow();

            if (strlist.Count == 0 || imglist.Count == 0)
            {
                return original;
            }
            string result = original;

            for (int i = 0; i < strlist.Count; i++)
            {
                result = ReplaceEx(result, imglist[i], strlist[i]);
            }

            return result;
        }

        public static string ReplaceEx(string original, string pattern, string replacement)
        {
            int count, position0, position1;
            count = position0 = position1 = 0;

            string upperString = original.ToUpper();
            string upperPattern = pattern.ToUpper();

            int inc = (original.Length / pattern.Length) * (replacement.Length - pattern.Length);

            char[] chars = new char[original.Length + Math.Max(0, inc)];

            while ((position1 = upperString.IndexOf(upperPattern, position0)) != -1)
            {
                for (int i = position0; i < position1; ++i)
                {
                    chars[count++] = original[i];
                }
                for (int i = 0; i < replacement.Length; ++i)
                {
                    chars[count++] = replacement[i];
                }
                position0 = position1 + pattern.Length;
            }
            if (position0 == 0)
            {
                return original;
            }
            for (int i = position0; i < original.Length; ++i)
            {
                chars[count++] = original[i]; 
            }
            return new string(chars, 0, count);
        }

        /// <summary>
        /// 转全角的函数(SBC case)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToSBC(string input)
        {
            //半角转全角：
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 32)
                {
                    c[i] = (char)12288;
                    continue;
                }
                if (c[i] < 127)
                    c[i] = (char)(c[i] + 65248);
            }
            return new string(c);
        }

        /// <summary>
        ///  转半角的函数(SBC case)
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns></returns>
        public static string ToDBC(string input)
        {
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }
                if (c[i] > 65280 && c[i] < 65375)
                    c[i] = (char)(c[i] - 65248);
            }
            return new string(c);
        }

        /// <summary>
        /// 将数字转换为大写汉字数字形式
        /// </summary>
        /// <param name="number">数字</param>
        /// <returns>返回转换后的汉字数字形式</returns>
        public static string ToUpperStr(int number)
        {
            string result = "";

            string ss = number + "";
            for (int i = 0; i < ss.Length; i++)
            {
                switch (ss.Substring(i, 1))
                {
                    case "1":
                        result += "一";
                        break;
                    case "2":
                        result += "二";
                        break;
                    case "3":
                        result += "三";
                        break;
                    case "4":
                        result += "四";
                        break;
                    case "5":
                        result += "五";
                        break;
                    case "6":
                        result += "六";
                        break;
                    case "7":
                        result += "七";
                        break;
                    case "8":
                        result += "八";
                        break;
                    case "9":
                        result += "九";
                        break;
                    default:
                        result += "〇";
                        break;
                }
            }

            return result;
        }

        public static List<string> GetSubStringList(string o_str, char sepeater)
        {
            List<string> list = new List<string>();
            string[] ss = o_str.Split(sepeater);
            foreach (string s in ss)
            {
                if (!string.IsNullOrEmpty(s) && s != sepeater.ToString())
                {
                    list.Add(s);
                }
            }
            return list;
        }

        #region 字符16进制转换
        /// <summary>
        /// 字符串转16进制字节数组
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0) hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        /// <summary>
        /// 字节数组转16进制字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                    returnStr += bytes[i].ToString("X2");
            }
            return returnStr;
        }

        /// <summary>
        /// 从汉字转换到16进制
        /// </summary>
        /// <param name="s"></param>
        /// <param name="charset">编码,如"utf-8","gb2312"</param>
        /// <param name="fenge">是否每字符用逗号分隔</param>
        /// <returns></returns>
        public static string ToHex(string s, string charset, bool fenge)
        {
            if ((s.Length % 2) != 0)
            {
                s += " ";//空格
            }
            Encoding chs = Encoding.GetEncoding(charset);
            byte[] bytes = chs.GetBytes(s);
            string str = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                str += string.Format("{0:X}", bytes[i]);
                if (fenge && (i != bytes.Length - 1)) str += string.Format("{0}", ",");
            }
            return str.ToLower();
        }

        ///<summary>
        /// 从16进制转换成汉字
        /// </summary>
        /// <param name="hex"></param>
        /// <param name="charset">编码,如"utf-8","gb2312"</param>
        /// <returns></returns>
        public static string UnHex(string hex, string charset)
        {
            if (hex == null) throw new ArgumentNullException("hex");
            hex = hex.Replace(",", "").Replace("\n", "").Replace("\\", "").Replace(" ", "");
            if (hex.Length % 2 != 0) hex += "20";//空格
            // 需要将 hex 转换成 byte 数组。 
            byte[] bytes = new byte[hex.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                try
                {
                    // 每两个字符是一个 byte。 
                    bytes[i] = byte.Parse(hex.Substring(i * 2, 2),
                    System.Globalization.NumberStyles.HexNumber);
                }
                catch
                {
                    // Rethrow an exception with custom message. 
                    throw new ArgumentException("hex is not a valid hex number!", "hex");
                }
            }
            Encoding chs = Encoding.GetEncoding(charset);
            return chs.GetString(bytes);
        }
        #endregion

        #region 获取时间对应的星期几
        /// <summary>
        /// 获取今日日期对应的星期几
        /// </summary>
        /// <returns>返回星期日、星期一、星期二、星期三、星期四、星期五、星期六，未知</returns>
        public static string GetDayOfWeek()
        {
            string week = "未知";
            try
            {
                week = Day[Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"))].ToString();
            }
            catch (Exception)
            {
            }
            return week;
        }

        /// <summary>
        /// 获取指定日期对应的星期几
        /// </summary>
        /// <param name="time">时间</param>
        /// <returns>返回星期日、星期一、星期二、星期三、星期四、星期五、星期六，未知</returns>
        public static string GetDayOfWeek(DateTime time)
        {
            string week = "未知";
            try
            {
                week = Day[Convert.ToInt32(time.DayOfWeek.ToString("d"))].ToString();
            }
            catch (Exception)
            {
            }
            return week;
        }

        /// <summary>
        /// 获取指定日期对应的星期几
        /// </summary>
        /// <param name="time">时间</param>
        /// <returns>返回星期日、星期一、星期二、星期三、星期四、星期五、星期六，未知</returns>
        public static string GetDayOfWeek(string time)
        {
            string week = "未知";
            try
            {
                week = Day[Convert.ToInt32(DateTime.Parse(time).DayOfWeek.ToString("d"))].ToString();
            }
            catch (Exception)
            {
            }
            return week;
        }
        #endregion

        #region 删除最后一个字符之后的字符

        /// <summary>
        /// 删除最后结尾的一个逗号
        /// </summary>
        public static string DelLastComma(string str)
        {
            return str.Substring(0, str.LastIndexOf(","));
        }

        /// <summary>
        /// 删除最后结尾的指定字符后的字符
        /// </summary>
        public static string DelLastChar(string str, string strchar)
        {
            return str.Substring(0, str.LastIndexOf(strchar));
        }

        #endregion

        #region 将字符串样式转换为纯字符串
        public static string GetCleanStyle(string StrList, string SplitString)
        {
            string RetrunValue = "";
            //如果为空，返回空值
            if (StrList == null)
            {
                RetrunValue = "";
            }
            else
            {
                //返回去掉分隔符
                string NewString = "";
                NewString = StrList.Replace(SplitString, "");
                RetrunValue = NewString;
            }
            return RetrunValue;
        }
        #endregion

        #region 将字符串转换为新样式
        public static string GetNewStyle(string StrList, string NewStyle, string SplitString, out string Error)
        {
            string ReturnValue = "";
            //如果输入空值，返回空，并给出错误提示
            if (StrList == null)
            {
                ReturnValue = "";
                Error = "请输入需要划分格式的字符串";
            }
            else
            {
                //检查传入的字符串长度和样式是否匹配,如果不匹配，则说明使用错误。给出错误信息并返回空值
                int strListLength = StrList.Length;
                int NewStyleLength = GetCleanStyle(NewStyle, SplitString).Length;
                if (strListLength != NewStyleLength)
                {
                    ReturnValue = "";
                    Error = "样式格式的长度与输入的字符长度不符，请重新输入";
                }
                else
                {
                    //检查新样式中分隔符的位置
                    string Lengstr = "";
                    for (int i = 0; i < NewStyle.Length; i++)
                    {
                        if (NewStyle.Substring(i, 1) == SplitString)
                        {
                            Lengstr = Lengstr + "," + i;
                        }
                    }
                    if (Lengstr != "")
                    {
                        Lengstr = Lengstr.Substring(1);
                    }
                    //将分隔符放在新样式中的位置
                    string[] str = Lengstr.Split(',');
                    foreach (string bb in str)
                    {
                        StrList = StrList.Insert(int.Parse(bb), SplitString);
                    }
                    //给出最后的结果
                    ReturnValue = StrList;
                    //因为是正常的输出，没有错误
                    Error = "";
                }
            }
            return ReturnValue;
        }
        #endregion
    }
}
