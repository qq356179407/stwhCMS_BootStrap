﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//导入命名空间
//using System.Speech.Synthesis;
using DotNetSpeech;

namespace stwh_Common
{
    /// <summary>
    /// 语音帮助类
    /// </summary>
    public class SpeechHelper
    {
        #region 使用System.Speech.Synthesis
        //SpeechSynthesizer synth;

        //public SpeechHelper()
        //{
        //    synth = new SpeechSynthesizer();
        //    //注册事件
        //    synth.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(synth_SpeakCompleted);
        //}

        //void synth_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        //{
        //    //Console.WriteLine("UserState:{0}",e.UserState);
        //    //Console.WriteLine("是否取消：{0}",e.Cancelled);
        //    //Console.WriteLine("错误信息：{0}", e.Error);
        //    //Console.WriteLine("Prompt：{0}", e.Prompt!=null?e.Prompt.ToString():null);
        //}

        ///// <summary>
        ///// 阅读指定文本
        ///// </summary>
        ///// <param name="text">文本内容</param>
        //public void Speak(string text)
        //{
        //    try
        //    {
        //        //synth.Speak(text);
        //        synth.SpeakAsync(text);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Console.WriteLine(ex.Message);
        //    }
        //}

        ///// <summary>
        ///// 停止阅读
        ///// </summary>
        //public void Stop()
        //{
        //    try
        //    {
        //        //synth.Speak(string.Empty);
        //        synth.SpeakAsyncCancelAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        //Console.WriteLine(ex.Message);
        //    }
        //}
        #endregion

        #region 使用DotNetSpeech工具
        public delegate void EndSpeakHanlder();
        public event EndSpeakHanlder EndSpeakEvent = null;
        SpVoice voice = new SpVoice();

        public SpeechHelper()
        {
            try
            {
                voice.EndStream += new _ISpeechVoiceEvents_EndStreamEventHandler(voice_EndStream);
            }
            catch (Exception)
            { }
        }

        void voice_EndStream(int StreamNumber, object StreamPosition)
        {
            try
            {
                if (EndSpeakEvent != null)
                {
                    EndSpeakEvent();
                }
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// 停止
        /// </summary>
        public void Stop()
        {
            try
            {
                voice.Speak(string.Empty, SpeechVoiceSpeakFlags.SVSFPurgeBeforeSpeak);
            }
            catch (Exception)
            {
                //ImcpLog.WrtieExceptionLog("AlarmBroadcaster", "Stop", ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// 暂停，使用
        /// </summary>
        public void Pause()
        {
            try
            {
                voice.Pause();//暂停，使用
            }
            catch (Exception)
            { }
        }
        
        /// <summary>
        /// 从暂停中继续刚才的朗读，使用
        /// </summary>
        public void Restart()
        {
            try
            {
                voice.Resume();
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// 朗读文本
        /// </summary>
        /// <param name="text">文本内容</param>
        /// <returns></returns>
        public int SpeakText(string text)
        {
            try
            {
                //sp.Voice = sp.GetVoices("name=Microsoft Simplified Chinese", "").Item(0);
                //Voice中是语音类型(语言、男(女)声),有 Microsoft Simplified Chinese，Microsoft Mary(Sam,Mike)等,
                //也可以这样：voice.Voice = voice.GetVoices(string.Empty, string.Empty).Item(0); //0选择默认的语音,1只能朗读数字和字母
                voice.Volume = 100;
                voice.Rate = 0;//朗读语速
                return voice.Speak(text, SpeechVoiceSpeakFlags.SVSFlagsAsync); //SpeechVoiceSpeakFlags是语音朗读的风格     
            }
            catch (Exception)
            {
                return -1;
            }
        }
        
        /// <summary>
        /// 生成语音文件
        /// </summary>
        /// <param name="text">语音内容</param>
        /// <param name="FileName">文件路径</param>
        public void GenerateVoiceFile(string text, string FileName)
        {
            try
            {
                SpeechVoiceSpeakFlags spFlags = SpeechVoiceSpeakFlags.SVSFlagsAsync;

                SpeechStreamFileMode spFileMode = SpeechStreamFileMode.SSFMCreateForWrite;
                SpFileStream spFileStream = new SpFileStream();
                spFileStream.Open(FileName, spFileMode, false);
                voice.AudioOutputStream = spFileStream;
                voice.Speak(text, spFlags);
                voice.WaitUntilDone(1000);
                //上面两句一定要写上，否则产生的文件没有声音
                //WaitUntilDone的后面的smTimeout是一个int型
                spFileStream.Close();

                //SaveFileDialog dialog = new SaveFileDialog();
                //dialog.Filter = "All files (*.*)|*.*|wav files (*.wav)|*.wav";
                //dialog.Title = "保存WAV文件";
                //dialog.FilterIndex = 2;
                //dialog.RestoreDirectory = true;
                //if (dialog.ShowDialog() == true)
                //{
                //    SpeechStreamFileMode spFileMode = SpeechStreamFileMode.SSFMCreateForWrite;
                //    SpFileStream spFileStream = new SpFileStream();
                //    spFileStream.Open(dialog.FileName, spFileMode, false);
                //    voice.AudioOutputStream = spFileStream;
                //    voice.Speak(text, spFlags);
                //    voice.WaitUntilDone(1000);
                //    //上面两句一定要写上，否则产生的文件没有声音
                //    //WaitUntilDone的后面的smTimeout是一个int型
                //    spFileStream.Close();
                //}
            }
            catch (Exception)
            { }
        }
        #endregion

    }
}
