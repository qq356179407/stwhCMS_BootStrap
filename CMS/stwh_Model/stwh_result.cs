﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_Model
{
    public class stwh_result : BaseModel
    {
        private string _msgcode;
        private object _msg;
        private int _sumcount;

        /// <summary>
        /// 结果数据总数
        /// </summary>
        public int sumcount
        {
            get { return _sumcount; }
            set { _sumcount = value; }
        }

        /// <summary>
        /// 结果信息
        /// </summary>
        public object msg
        {
            get { return _msg; }
            set { _msg = value; }
        }

        /// <summary>
        /// 结果代码
        /// </summary>
        public string msgcode
        {
            get { return _msgcode; }
            set { _msgcode = value; }
        }
    }
}
