﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_Model
{
    [Serializable]
    public class stwh_menu_role_function : BaseModel
    {
        private int _stwh_fid;
        private int _stwh_rid;

        public int stwh_rid
        {
            get { return _stwh_rid; }
            set { _stwh_rid = value; }
        }

        public int stwh_fid
        {
            get { return _stwh_fid; }
            set { _stwh_fid = value; }
        }
    }
}
