﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_notice:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_notice : BaseModel
	{
		public stwh_notice()
		{}
		#region Model
		private int _stwh_noid;
		private int _stwh_notid=1;
		private string _stwh_notitle;
		private string _stwh_nojianjie;
		private string _stwh_nocontent;
		private DateTime _stwh_noaddtime;
        private string _stwh_nopath;
        private int _stwh_noorder;

        public int stwh_noorder
        {
            get { return _stwh_noorder; }
            set { _stwh_noorder = value; }
        }

        public string stwh_nopath
        {
            get { return _stwh_nopath; }
            set { _stwh_nopath = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_noid
		{
			set{ _stwh_noid=value;}
			get{return _stwh_noid;}
		}
		/// <summary>
		/// 
		/// </summary>
        public int stwh_notid
		{
			set{ _stwh_notid=value;}
			get{return _stwh_notid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_notitle
		{
			set{ _stwh_notitle=value;}
			get{return _stwh_notitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_nojianjie
		{
			set{ _stwh_nojianjie=value;}
			get{return _stwh_nojianjie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_nocontent
		{
			set{ _stwh_nocontent=value;}
			get{return _stwh_nocontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_noaddtime
		{
			set{ _stwh_noaddtime=value;}
			get{return _stwh_noaddtime;}
		}
		#endregion Model
        #region Model
        private string _stwh_nottitle;
        private string _stwh_notjianjie;
        private DateTime _stwh_notaddtime;
        /// <summary>
        /// 
        /// </summary>
        public string stwh_nottitle
        {
            set { _stwh_nottitle = value; }
            get { return _stwh_nottitle; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_notjianjie
        {
            set { _stwh_notjianjie = value; }
            get { return _stwh_notjianjie; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime stwh_notaddtime
        {
            set { _stwh_notaddtime = value; }
            get { return _stwh_notaddtime; }
        }
        #endregion Model
	}
}

