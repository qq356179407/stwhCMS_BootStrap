﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_productbrand:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_productbrand : BaseModel
	{
		public stwh_productbrand()
		{}
		#region Model
		private int _stwh_pbid;
		private string _stwh_pbname;
		private string _stwh_pbimage;
		private string _stwh_pburl;
		private string _stwh_pbdescription;
		private int _stwh_pborder;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pbid
		{
			set{ _stwh_pbid=value;}
			get{return _stwh_pbid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pbname
		{
			set{ _stwh_pbname=value;}
			get{return _stwh_pbname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pbimage
		{
			set{ _stwh_pbimage=value;}
			get{return _stwh_pbimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pburl
		{
			set{ _stwh_pburl=value;}
			get{return _stwh_pburl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pbdescription
		{
			set{ _stwh_pbdescription=value;}
			get{return _stwh_pbdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pborder
		{
			set{ _stwh_pborder=value;}
			get{return _stwh_pborder;}
		}
		#endregion Model
	}
}

