﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_producttype_pv:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_producttype_pv : BaseModel
	{
		public stwh_producttype_pv()
		{}
		#region Model
		private int _stwh_ptpvid;
		private int _stwh_pid;
		private int _stwh_ptpid;
		private string _stwh_ptpvvalue;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ptpvid
		{
			set{ _stwh_ptpvid=value;}
			get{return _stwh_ptpvid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pid
		{
			set{ _stwh_pid=value;}
			get{return _stwh_pid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ptpid
		{
			set{ _stwh_ptpid=value;}
			get{return _stwh_ptpid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ptpvvalue
		{
			set{ _stwh_ptpvvalue=value;}
			get{return _stwh_ptpvvalue;}
		}
		#endregion Model

	}
}

