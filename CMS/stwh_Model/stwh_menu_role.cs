﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_menu_role:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_menu_role : BaseModel
	{
		public stwh_menu_role()
		{}

        #region Model
        private int _stwh_menuid;
        private string _stwh_menuname;
        private string _stwh_menunameus;
        private string _stwh_menuico_url;
        private string _stwh_menuurl;
        private int _stwh_menuparentid = 0;
        private int _stwh_menustatus = 0;
        private string _stwh_menudescription = "暂无描述";
        private int _stwh_menuorder;

        public int stwh_menuorder
        {
            get { return _stwh_menuorder; }
            set { _stwh_menuorder = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_menuid
        {
            set { _stwh_menuid = value; }
            get { return _stwh_menuid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_menuname
        {
            set { _stwh_menuname = value; }
            get { return _stwh_menuname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_menunameUS
        {
            set { _stwh_menunameus = value; }
            get { return _stwh_menunameus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_menuICO_url
        {
            set { _stwh_menuico_url = value; }
            get { return _stwh_menuico_url; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_menuURL
        {
            set { _stwh_menuurl = value; }
            get { return _stwh_menuurl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_menuparentID
        {
            set { _stwh_menuparentid = value; }
            get { return _stwh_menuparentid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_menustatus
        {
            set { _stwh_menustatus = value; }
            get { return _stwh_menustatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_menudescription
        {
            set { _stwh_menudescription = value; }
            get { return _stwh_menudescription; }
        }
        #endregion Model

        #region Model
        private int _stwh_rid;
        private string _stwh_rname;
        private string _stwh_rdescription = "暂无描述";
        private DateTime _stwh_rctime = DateTime.Now;
        private int _stwh_rstate = 0;
        private int _stwh_rdelstate = 0;
        /// <summary>
        /// 
        /// </summary>
        public int stwh_rid
        {
            set { _stwh_rid = value; }
            get { return _stwh_rid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_rname
        {
            set { _stwh_rname = value; }
            get { return _stwh_rname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_rdescription
        {
            set { _stwh_rdescription = value; }
            get { return _stwh_rdescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime stwh_rctime
        {
            set { _stwh_rctime = value; }
            get { return _stwh_rctime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_rstate
        {
            set { _stwh_rstate = value; }
            get { return _stwh_rstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_rdelstate
        {
            set { _stwh_rdelstate = value; }
            get { return _stwh_rdelstate; }
        }
        #endregion Model
	}
}

