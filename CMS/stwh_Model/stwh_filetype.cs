﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_filetype:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_filetype : BaseModel
	{
		public stwh_filetype()
		{}
		#region Model
		private int _stwh_ftid;
		private string _stwh_ftname;
		private string _stwh_ftdescription;
		private string _stwh_ftimg;
        private int _stwh_ftorder = 0;

        /// <summary>
        /// 
        /// </summary>
        public int stwh_ftorder
        {
            get { return _stwh_ftorder; }
            set { _stwh_ftorder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ftid
		{
			set{ _stwh_ftid=value;}
			get{return _stwh_ftid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ftname
		{
			set{ _stwh_ftname=value;}
			get{return _stwh_ftname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ftdescription
		{
			set{ _stwh_ftdescription=value;}
			get{return _stwh_ftdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ftimg
		{
			set{ _stwh_ftimg=value;}
			get{return _stwh_ftimg;}
		}
		#endregion Model

	}
}

