﻿using System;
namespace stwh_Model
{
    /// <summary>
    /// stwh_config:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class stwh_config : BaseModel
    {
        public stwh_config()
        { }
        #region Model
        private int _stwh_cfid;
        private int _stwh_cfweihu = 0;
        private int _stwh_cfweihu_m = 1;
        private string _stwh_cfico = "/Upload/image/favicon.ico";
        private string _stwh_cfico_m = "/Upload/image/favicon.ico";
        private string _stwh_cfico_s = "/Upload/image/favicon.ico";
        private string _stwh_cflogo="";
        private string _stwh_cflogo_m = "";
        private string _stwh_cflogo_s = "";
        private string _stwh_cfwebname = "上海舒同文化传播有限公司";
        private string _stwh_cfwebname_m = "上海舒同文化传播有限公司";
        private string _stwh_cfwebname_s = "上海舒同文化传播有限公司";
        private int _stwh_cfuploadsize = 10240;
        private int _stwh_cfthumbnail = 600;
        private string _stwh_cfuploadtype = "png,gif,jpg,bmp,doc,mp3,flv,pdf,ico";
        private string _stwh_cfcopyright = "Copyright 2015 ofall.cn All Rights Reserved.";
        private string _stwh_cfcopyright_m = "Copyright 2015 ofall.cn All Rights Reserved.";
        private string _stwh_cfcopyright_s = "Copyright 2015 ofall.cn All Rights Reserved.";
        private string _stwh_cfkeywords="cms,cms系统,asp,asp系统,系统,网站建设,维护,网站UI设计,手机app";
        private string _stwh_cfkeywords_m = "cms,cms系统,asp,asp系统,系统,网站建设,维护,网站UI设计,手机app";
        private string _stwh_cfkeywords_s = "cms,cms系统,asp,asp系统,系统,网站建设,维护,网站UI设计,手机app";
        private string _stwh_cfdescription = "上海舒同文化传播有限公司是CMS行业最流行的网站建设解决方案提供商之一。专业提供电子商务网站建设、在线考试系统网站建设、企业网站建设、教育及行业门户网站建设解决方案等于一体的高新技术企业。";
        private string _stwh_cfdescription_m = "上海舒同文化传播有限公司是CMS行业最流行的网站建设解决方案提供商之一。专业提供电子商务网站建设、在线考试系统网站建设、企业网站建设、教育及行业门户网站建设解决方案等于一体的高新技术企业。";
        private string _stwh_cfdescription_s = "上海舒同文化传播有限公司是CMS行业最流行的网站建设解决方案提供商之一。专业提供电子商务网站建设、在线考试系统网站建设、企业网站建设、教育及行业门户网站建设解决方案等于一体的高新技术企业。";
        private string _stwh_cfwebtemplate = "default";
        private string _stwh_cfmwebtemplate = "default";
        private string _stwh_cfsytk="";
        private string _stwh_cfystk="";
        private DateTime _stwh_cfaddtime = DateTime.Now;
        private string _stwh_cfsmtp = "smtp.live.com";
        private int _stwh_cfport = 25;
        private string _stwh_cfsendname = "a474197200@hotmail.com";
        private string _stwh_cfname = "系统消息（请勿回复）";
        private string _stwh_cfuser = "a474197200@hotmail.com";
        private string _stwh_cfpwd = "DE364517950990C0E0CF8B44912806F714DEA4972B7F6FD3";
        private string _stwh_cfversion = "v2.6.5";
        private string _stwh_cfshoptemplate = "default";
        private int _stwh_cfweihu_s = 1;
        private string _stwh_cfkey;
        private string _stwh_cfkeytime;

        /// <summary>
        /// 系统key有效期
        /// </summary>
        public string stwh_cfkeytime
        {
            get { return _stwh_cfkeytime; }
            set { _stwh_cfkeytime = value; }
        }

        /// <summary>
        /// 系统key
        /// </summary>
        public string stwh_cfkey
        {
            get { return _stwh_cfkey; }
            set { _stwh_cfkey = value; }
        }

        /// <summary>
        /// 商城网站描述
        /// </summary>
        public string stwh_cfdescription_s
        {
            get { return _stwh_cfdescription_s; }
            set { _stwh_cfdescription_s = value; }
        }

        /// <summary>
        /// 移动网站描述
        /// </summary>
        public string stwh_cfdescription_m
        {
            get { return _stwh_cfdescription_m; }
            set { _stwh_cfdescription_m = value; }
        }

        /// <summary>
        /// 商城网站关键词
        /// </summary>
        public string stwh_cfkeywords_s
        {
            get { return _stwh_cfkeywords_s; }
            set { _stwh_cfkeywords_s = value; }
        }
        /// <summary>
        /// 移动网站关键词
        /// </summary>
        public string stwh_cfkeywords_m
        {
            get { return _stwh_cfkeywords_m; }
            set { _stwh_cfkeywords_m = value; }
        }

        /// <summary>
        /// 移动网站版权
        /// </summary>
        public string stwh_cfcopyright_m
        {
            get { return _stwh_cfcopyright_m; }
            set { _stwh_cfcopyright_m = value; }
        }

        /// <summary>
        /// 商城网站版权
        /// </summary>
        public string stwh_cfcopyright_s
        {
            get { return _stwh_cfcopyright_s; }
            set { _stwh_cfcopyright_s = value; }
        }

        /// <summary>
        /// 商城网站名称
        /// </summary>
        public string stwh_cfwebname_s
        {
            get { return _stwh_cfwebname_s; }
            set { _stwh_cfwebname_s = value; }
        }

        /// <summary>
        /// 移动网站名称
        /// </summary>
        public string stwh_cfwebname_m
        {
            get { return _stwh_cfwebname_m; }
            set { _stwh_cfwebname_m = value; }
        }

        /// <summary>
        /// 移动网站ico
        /// </summary>
        public string stwh_cfico_m
        {
            get { return _stwh_cfico_m; }
            set { _stwh_cfico_m = value; }
        }

        /// <summary>
        /// 删除网站ico
        /// </summary>
        public string stwh_cfico_s
        {
            get { return _stwh_cfico_s; }
            set { _stwh_cfico_s = value; }
        }

        /// <summary>
        /// 移动网站logo
        /// </summary>
        public string stwh_cflogo_m
        {
            get { return _stwh_cflogo_m; }
            set { _stwh_cflogo_m = value; }
        }

        /// <summary>
        /// 删除网站logo
        /// </summary>
        public string stwh_cflogo_s
        {
            get { return _stwh_cflogo_s; }
            set { _stwh_cflogo_s = value; }
        }

        /// <summary>
        /// 是否开启商城网站维护（默认0关闭，1开启）
        /// </summary>
        public int stwh_cfweihu_s
        {
            get { return _stwh_cfweihu_s; }
            set { _stwh_cfweihu_s = value; }
        }

        /// <summary>
        /// 商城网站模板
        /// </summary>
        public string stwh_cfshoptemplate
        {
            get { return _stwh_cfshoptemplate; }
            set { _stwh_cfshoptemplate = value; }
        }

        /// <summary>
        /// 系统版本号
        /// </summary>
        public string stwh_cfversion
        {
            get { return _stwh_cfversion; }
            set { _stwh_cfversion = value; }
        }
        /// <summary>
        /// 邮箱登陆密码
        /// </summary>
        public string stwh_cfpwd
        {
            get { return _stwh_cfpwd; }
            set { _stwh_cfpwd = value; }
        }
        /// <summary>
        /// 邮箱登陆名
        /// </summary>
        public string stwh_cfuser
        {
            get { return _stwh_cfuser; }
            set { _stwh_cfuser = value; }
        }
        /// <summary>
        /// 显示名称
        /// </summary>
        public string stwh_cfname
        {
            get { return _stwh_cfname; }
            set { _stwh_cfname = value; }
        }
        /// <summary>
        /// 发件人
        /// </summary>
        public string stwh_cfsendname
        {
            get { return _stwh_cfsendname; }
            set { _stwh_cfsendname = value; }
        }
        /// <summary>
        /// 邮箱端口
        /// </summary>
        public int stwh_cfport
        {
            get { return _stwh_cfport; }
            set { _stwh_cfport = value; }
        }
        /// <summary>
        /// SMTP服务器地址
        /// </summary>
        public string stwh_cfsmtp
        {
            get { return _stwh_cfsmtp; }
            set { _stwh_cfsmtp = value; }
        }
        /// <summary>
        /// 编号
        /// </summary>
        public int stwh_cfid
        {
            set { _stwh_cfid = value; }
            get { return _stwh_cfid; }
        }
        /// <summary>
        /// 是否开启网站维护（默认0关闭，1开启）
        /// </summary>
        public int stwh_cfweihu
        {
            set { _stwh_cfweihu = value; }
            get { return _stwh_cfweihu; }
        }
        /// <summary>
        /// 是否开启移动端网站维护（默认0关闭，1开启）
        /// </summary>
        public int stwh_cfweihu_m
        {
            get { return _stwh_cfweihu_m; }
            set { _stwh_cfweihu_m = value; }
        }

        /// <summary>
        /// 网站图标ico
        /// </summary>
        public string stwh_cfico
        {
            set { _stwh_cfico = value; }
            get { return _stwh_cfico; }
        }
        /// <summary>
        /// 网站logo
        /// </summary>
        public string stwh_cflogo
        {
            set { _stwh_cflogo = value; }
            get { return _stwh_cflogo; }
        }
        /// <summary>
        /// 网站名称
        /// </summary>
        public string stwh_cfwebname
        {
            set { _stwh_cfwebname = value; }
            get { return _stwh_cfwebname; }
        }
        /// <summary>
        /// 限制上传文件大小，默认10MB，单位KB
        /// </summary>
        public int stwh_cfuploadsize
        {
            set { _stwh_cfuploadsize = value; }
            get { return _stwh_cfuploadsize; }
        }
        /// <summary>
        /// 限制上传缩略图文件大小，默认600KB，单位KB
        /// </summary>
        public int stwh_cfthumbnail
        {
            set { _stwh_cfthumbnail = value; }
            get { return _stwh_cfthumbnail; }
        }
        /// <summary>
        /// 限制上传文件类型，默认png,gif,jpg,bmp,ico
        /// </summary>
        public string stwh_cfuploadtype
        {
            set { _stwh_cfuploadtype = value; }
            get { return _stwh_cfuploadtype; }
        }
        /// <summary>
        /// 网站版权
        /// </summary>
        public string stwh_cfcopyright
        {
            set { _stwh_cfcopyright = value; }
            get { return _stwh_cfcopyright; }
        }
        /// <summary>
        /// 网站关键词
        /// </summary>
        public string stwh_cfkeywords
        {
            set { _stwh_cfkeywords = value; }
            get { return _stwh_cfkeywords; }
        }
        /// <summary>
        /// 网站描述
        /// </summary>
        public string stwh_cfdescription
        {
            set { _stwh_cfdescription = value; }
            get { return _stwh_cfdescription; }
        }
        /// <summary>
        /// 网站模板
        /// </summary>
        public string stwh_cfwebtemplate
        {
            set { _stwh_cfwebtemplate = value; }
            get { return _stwh_cfwebtemplate; }
        }
        /// <summary>
        /// 移动网站模板
        /// </summary>
        public string stwh_cfmwebtemplate
        {
            set { _stwh_cfmwebtemplate = value; }
            get { return _stwh_cfmwebtemplate; }
        }
        /// <summary>
        /// 使用条款
        /// </summary>
        public string stwh_cfsytk
        {
            set { _stwh_cfsytk = value; }
            get { return _stwh_cfsytk; }
        }
        /// <summary>
        /// 隐私条款
        /// </summary>
        public string stwh_cfystk
        {
            set { _stwh_cfystk = value; }
            get { return _stwh_cfystk; }
        }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime stwh_cfaddtime
        {
            set { _stwh_cfaddtime = value; }
            get { return _stwh_cfaddtime; }
        }
        #endregion Model
    }
}

