﻿using System;
using System.Collections.Generic;
namespace stwh_Model
{
    /// <summary>
    /// stwh_order_details:非数据库实体类
    /// </summary>
    [Serializable]
    public partial class stwh_order_details : BaseModel
    {
        public stwh_order_details()
        { }
        private List<stwh_order> _orderList;
        private List<stwh_orderdetails> _orderDetailsList;

        /// <summary>
        /// 订单商品详细
        /// </summary>
        public List<stwh_orderdetails> OrderDetailsList
        {
            get { return _orderDetailsList; }
            set { _orderDetailsList = value; }
        }

        /// <summary>
        /// 订单集合数据
        /// </summary>
        public List<stwh_order> OrderList
        {
            get { return _orderList; }
            set { _orderList = value; }
        }
    }
}

