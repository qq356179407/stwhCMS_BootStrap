﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_blogroll:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_blogroll : BaseModel
	{
		public stwh_blogroll()
		{}
		#region Model
		private int _stwh_blid;
		private string _stwh_blname;
        private string _stwh_blimg;

        public string stwh_blimg
        {
            get { return _stwh_blimg; }
            set { _stwh_blimg = value; }
        }
		private string _stwh_blurl;
		private string _stwh_blremark;
		private int _stwh_blorder;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_blid
		{
			set{ _stwh_blid=value;}
			get{return _stwh_blid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_blname
		{
			set{ _stwh_blname=value;}
			get{return _stwh_blname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_blurl
		{
			set{ _stwh_blurl=value;}
			get{return _stwh_blurl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_blremark
		{
			set{ _stwh_blremark=value;}
			get{return _stwh_blremark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_blorder
		{
			set{ _stwh_blorder=value;}
			get{return _stwh_blorder;}
		}
		#endregion Model
	}
}

