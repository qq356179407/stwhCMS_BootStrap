﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_recruit:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_recruit : BaseModel
	{
		public stwh_recruit()
		{}
		#region Model
		private int _stwh_rtid;
		private string _stwh_rtnamejd;
		private string _stwh_rtname;
		private string _stwh_rtzhize;
		private string _stwh_rtyaoqiu;
		private string _stwh_rtfuli;
		private string _stwh_rtmoney;
		private string _stwh_rtxueli;
		private int _stwh_rtjobtime;
		private int _stwh_rtpeople;
		private string _stwh_rtplace;
		private DateTime _stwh_rtaddtime;
		private string _stwh_rtcname;
		private string _stwh_rtchangye;
		private string _stwh_rtcxingzhi;
		private string _stwh_rtcguimo;
        private int _stwh_rtorder = 1;
        private string _stwh_rtimg;

        public string stwh_rtimg
        {
            get { return _stwh_rtimg; }
            set { _stwh_rtimg = value; }
        }

        public int stwh_rtorder
        {
            get { return _stwh_rtorder; }
            set { _stwh_rtorder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_rtid
		{
			set{ _stwh_rtid=value;}
			get{return _stwh_rtid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtnamejd
		{
			set{ _stwh_rtnamejd=value;}
			get{return _stwh_rtnamejd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtname
		{
			set{ _stwh_rtname=value;}
			get{return _stwh_rtname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtzhize
		{
			set{ _stwh_rtzhize=value;}
			get{return _stwh_rtzhize;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtyaoqiu
		{
			set{ _stwh_rtyaoqiu=value;}
			get{return _stwh_rtyaoqiu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtfuli
		{
			set{ _stwh_rtfuli=value;}
			get{return _stwh_rtfuli;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtmoney
		{
			set{ _stwh_rtmoney=value;}
			get{return _stwh_rtmoney;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtxueli
		{
			set{ _stwh_rtxueli=value;}
			get{return _stwh_rtxueli;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_rtjobtime
		{
			set{ _stwh_rtjobtime=value;}
			get{return _stwh_rtjobtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_rtpeople
		{
			set{ _stwh_rtpeople=value;}
			get{return _stwh_rtpeople;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtplace
		{
			set{ _stwh_rtplace=value;}
			get{return _stwh_rtplace;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_rtaddtime
		{
			set{ _stwh_rtaddtime=value;}
			get{return _stwh_rtaddtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtcname
		{
			set{ _stwh_rtcname=value;}
			get{return _stwh_rtcname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtchangye
		{
			set{ _stwh_rtchangye=value;}
			get{return _stwh_rtchangye;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtcxingzhi
		{
			set{ _stwh_rtcxingzhi=value;}
			get{return _stwh_rtcxingzhi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rtcguimo
		{
			set{ _stwh_rtcguimo=value;}
			get{return _stwh_rtcguimo;}
		}
		#endregion Model

	}
}

