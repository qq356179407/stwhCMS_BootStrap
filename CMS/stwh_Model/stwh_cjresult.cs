﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_cjresult:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_cjresult : BaseModel
	{
		public stwh_cjresult()
		{}
		#region Model
		private int _stwh_cjrid;
		private string _stwh_cjrmobile;
		private DateTime _stwh_cjraddtime;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_cjrid
		{
			set{ _stwh_cjrid=value;}
			get{return _stwh_cjrid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_cjrmobile
		{
			set{ _stwh_cjrmobile=value;}
			get{return _stwh_cjrmobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_cjraddtime
		{
			set{ _stwh_cjraddtime=value;}
			get{return _stwh_cjraddtime;}
		}
		#endregion Model
	}
}

