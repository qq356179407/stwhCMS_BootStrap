﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_noticetype:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_noticetype : BaseModel
	{
		public stwh_noticetype()
		{}
		#region Model
		private int _stwh_notid;
		private string _stwh_nottitle;
		private string _stwh_notjianjie;
		private DateTime _stwh_notaddtime;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_notid
		{
			set{ _stwh_notid=value;}
			get{return _stwh_notid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_nottitle
		{
			set{ _stwh_nottitle=value;}
			get{return _stwh_nottitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_notjianjie
		{
			set{ _stwh_notjianjie=value;}
			get{return _stwh_notjianjie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_notaddtime
		{
			set{ _stwh_notaddtime=value;}
			get{return _stwh_notaddtime;}
		}
		#endregion Model
	}
}

