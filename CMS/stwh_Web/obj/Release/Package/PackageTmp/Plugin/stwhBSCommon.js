﻿/*
封装BootStrap常用插件
*/
(function ($) {
    /*
    提示框
    msg             消息内容
    type             消息类型（success,info,warning,danger）
    url                跳转链接/是否自动隐藏弹出框
    */
    function BS_Alert(msg, type, url) {
        $.bs.speak(msg);
        var msgBox = '<div id="Modal' + type + '" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-body alert alert-' + type + '" style="margin-bottom: 0px;" id="Modal' + type + 'Msg">' + msg + '</div></div></div></div>';
        if ($("#Modal" + type).length == 0) $("body").append(msgBox);
        else $("#Modal" + type + "Msg").html(msg);
        $("#Modal" + type).modal();
        if (typeof url === "string" && url) {
            if (url == "this") {
                setTimeout(function () {
                    var dqurl = window.location.href.split('#')[0];
                    window.location.href = dqurl;
                }, 1000);
            }
            else if (url == "-1") {
                setTimeout(function () {
                    window.history.go(-1);
                }, 1000);
            }
            else if (typeof url === "string") {
                setTimeout(function () {
                    window.location.href = url;
                }, 1000);
            }
        }
        else if (typeof url === "boolean") {
            if (url) {
                setTimeout(function () {
                    $("#Modal" + type).modal('hide');
                }, $.bs.options.hideTime);
            }
        }
        else if (typeof url === "function") {
            $("#Modal" + type).on("hide.bs.modal", function () {
                url();
            });
        }
        else {
            if ($.bs.options.isHide) {
                setTimeout(function () {
                    $("#Modal" + type).modal('hide');
                }, $.bs.options.hideTime);
            }
        }
    }
    $.bs = {
        alert: function (msg, type, url) {
            BS_Alert(msg, type, url);
        },
        //朗读消息内容，该功能需要后台支持
        speak: function (msg) {
            if ($.bs.options.isSpeak) {
                if (msg && typeof msg === "string") {
                    $.post("/handler/speech.ashx", {
                        "type": 0,
                        "text": msg
                    }, function (data) { }, "text");
                }
            }
        },
        //加载
        loading: function (boolean) {
            if (boolean) {
                if ($("#loading").length == 0) {
                    $("body").prepend('<div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>');
                }
            }
            else {
                setTimeout(function () {
                    $("#loading").animate({ opacity: 0 }, 300, function () {
                        $(this).remove();
                    });
                }, 500);
            }
        }
    };
    //默认参数
    $.bs.options = {
        isHide: true, //是否自动隐藏弹出框，false不隐藏，true隐藏，默认true隐藏
        isSpeak: false, //是否朗读消息，false关闭，true开启，默认false。开启后会增加消耗网络流量和延迟时间
        hideTime: 2000 //当isHide为true时，有效，默认1.5秒隐藏弹出框
    };
})(jQuery);