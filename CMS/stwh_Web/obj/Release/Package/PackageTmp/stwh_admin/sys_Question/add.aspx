﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_Question.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">试题管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
        method="post" role="form">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-1 st-text-right">
                    试题分类：</div>
                <div class="col-sm-11 form-group">
                    <input type="hidden" id="stwh_qutid" name="stwh_qutid" value="0" />
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span id="selectShowText">请选择试题分类</span> <span class="caret"></span>
                    </button>
                    <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                        overflow: auto; left: auto; top: auto;">
                        <li><a data-pid="0">父级导航</a></li>
                        <li><a data-pid="0">系统设置</a></li>
                        <li><a data-pid="0">系统管理</a></li>
                        <li class="divider"></li>
                        <li><a data-pid="0">内容管理</a></li>
                    </ul>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    试题类型：</div>
                <div class="col-sm-11 form-group">
                    <input type="hidden" id="stwh_qutype" name="stwh_qutype" value="0" />
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span id="selectShowTextType">请选择试题类型</span> <span class="caret"></span>
                    </button>
                    <ul id="selectShowListType" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                        overflow: auto; left: auto; top: auto;">
                        <li><a data-pid="1">单选题</a></li>
                        <li><a data-pid="2">多选题</a></li>
                        <li><a data-pid="3">判断题</a></li>
                        <li><a data-pid="4">填空题</a></li>
                    </ul>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    试题标题：</div>
                <div class="col-sm-11 form-group">
                    <input type="text" id="stwh_qutitle" name="stwh_qutitle" value="" class="st-input-text-300 form-control pull-left"
                        style="margin-right: 15px;" placeholder="请输入试题标题" />
                    <span class="text-danger">*试题标题长度不能超过100个字符</span>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    序号：</div>
                <div class="col-sm-11 form-group">
                    <input type="text" id="stwh_quorder" name="stwh_quorder" onkeydown="return checkNumber(event);"
                        value="1" style="margin-right: 15px;" class="st-input-text-300 form-control pull-left" />
                    <span class="text-danger">*越大越考前</span>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    试题分数：</div>
                <div class="col-sm-11 form-group">
                    <input type="text" id="stwh_qufenshu" name="stwh_qufenshu" onkeydown="return checkNumber(event);"
                        value="1" style="margin-right: 15px;" class="st-input-text-300 form-control"
                        placeholder="请输入试题分数" />
                    <span class="text-danger"></span>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    试题答案：</div>
                <div class="col-sm-11 form-group">
                    <input type="text" id="stwh_qudaan" name="stwh_qudaan" value="" style="margin-right: 15px;"
                        class="st-input-text-300 form-control" placeholder="请输入试题答案" />
                    <span class="text-danger">类型为单选题，请直接填写答案，例如：A；<br/>
                    类型为多选题，答案格式为：A,B,C，用英文逗号隔开；<br/>
                    类型为判断题，直接填写：对或者错；<br/>
                    类型为填空题，答案格式为：答案,答案,daa，用英文逗号隔开，答案中不能出现英文逗号，否则无法匹配答案。</span>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    试题描述：</div>
                <div class="col-sm-11 form-group">
                    <textarea id="stwh_qumiaoshu" name="stwh_qumiaoshu" style="width: 700px; height: 300px;"></textarea>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <button id="addMenu" class="btn btn-default">
            <i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_qumiaoshu', qjOptions);

        setTimeout(function () {
            qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            $("#stwh_qutitle").focus();
            $(window).scrollTop(0);
        }, 500);
    </script>
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                $("#stwh_qutid").val($(this).attr("data-pid"));
            });
            $("#selectShowListType a").click(function () {
                $("#selectShowTextType").text($(this).text());
                $("#stwh_qutype").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_qutid = $("#stwh_qutid").val();
                if (stwh_qutid == "0") {
                    $.bs.alert("请选择试题分类！", "info");
                    return false;
                }
                var stwh_qutype = $("#stwh_qutype").val();
                if (stwh_qutype == "0") {
                    $.bs.alert("请选择试题类型！", "info");
                    return false;
                }
                var stwh_qutitle = $("#stwh_qutitle").val();
                if (!stwh_qutitle) {
                    $.bs.alert("请输入试题标题！", "info");
                    return false;
                }

                var stwh_qufenshu = $("#stwh_qufenshu").val();
                if (!stwh_qufenshu) {
                    $.bs.alert("请输入试题分数！", "info");
                    return false;
                }
                var stwh_qudaan = $("#stwh_qudaan").val();
                if (!stwh_qudaan) {
                    $.bs.alert("请输入试题答案！", "info");
                    return false;
                }
                var stwh_qumiaoshu = qjUEObject.getContent();
                if (!stwh_qumiaoshu) {
                    $.bs.alert("请输入试题描述内容！", "info");
                    return false;
                }

                $.post("/Handler/stwh_admin/sys_Question/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        qjUEObject.setContent("");
                        //清除本地数据
                        qjUEObject.execCommand("clearlocaldata");
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>