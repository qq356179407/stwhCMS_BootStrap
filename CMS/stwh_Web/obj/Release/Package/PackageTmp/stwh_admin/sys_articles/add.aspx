﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_articles.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <ul id="setTab" class="nav nav-tabs" style="margin-bottom:1px;">
        <li class="active" style="margin-left: 15px;"><a href="#setpanel0" ><span class="fa fa-wrench">
        </span>
            基本信息</a> </li>
        <li><a href="#setpanel1" ><span class="fa fa-link"></span>
            文章属性</a> </li>
        <li><a href="#setpanel2" ><span class="fa fa-link"></span>
            SEO选项</a> </li>
    </ul>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
    <div class="container-fluid">
        <div id="setTabContent" class="tab-content" style="padding-top:15px;">
            <div class="tab-pane active" id="setpanel0">
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        归属栏目：</div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" id="stwh_artid" name="stwh_artid" value="0" />
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span id="selectShowText">请选择归属栏目</span> <span class="caret"></span>
                        </button>
                        <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height:300px; overflow:auto; left:auto; top:auto;">
                            <li><a data-pid="0">父级导航</a></li>
                            <li><a data-pid="0">系统设置</a></li>
                            <li><a data-pid="0">系统管理</a></li>
                            <li class="divider"></li>
                            <li><a data-pid="0">内容管理</a></li>
                        </ul>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        封面图：</div>
                    <div class="col-sm-11 form-group">
                        <input id="stwh_atimage" name="stwh_atimage" type="text" class="st-input-text-700 form-control" placeholder="请输入上传封面图" style="float: left;" />
                        <div style="float: left;width: 100px;">
                            <div id="upwebico">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="progressFile" class="progress progress-striped active" style=" display:none;">
                           <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                           </div>
                        </div>
                    </div>
                    <div class="col-sm-1 st-text-right">
                        相关图片：</div>
                    <div class="col-sm-11 form-group">
                        <input id="stwh_atimglist" name="stwh_atimglist" type="hidden" />
                        <div id="upwebico1"></div>
                        <div class="clearfix"></div>
                        <div id="imglistPanel">
                        </div>
                        <span class="text-danger">*请上传文章相关图片。点击图片即可删除图片</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        简短标题：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_attitlesimple" name="stwh_attitlesimple" value=""  class="st-input-text-700 form-control" placeholder="请输入文章简短标题" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        文章标题：</div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" value="" name="stwh_atstyle" id="stwh_atstyle" />
                        <input type="text" id="stwh_attitle" name="stwh_attitle" value=""  class="st-input-text-700 form-control pull-left" style="margin-right:15px;" placeholder="请输入文章标题" />
                        <div class="btn-group pull-left" data-toggle="buttons">
                            <label class="btn btn-primary btn-sm">
                                <input type="radio" name="bold" id="bold1" value="0" />
                                加粗
                            </label>
                            <label class="btn btn-primary btn-sm active">
                                <input type="radio" name="bold" id="bold2" value="1" checked="checked"/>
                                不加粗
                            </label>
                        </div>
                        <input type="text" id="titlecolor" value=""  class="st-input-text-300 form-control pull-left" style="width:150px; margin-left:15px;" placeholder="请选择标题颜色" />
                        <div class="clearfix"></div>
                        <span class="text-danger">*栏目名称内容长度不能超过100个字符</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        序号：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_atorder" name="stwh_atorder" onkeydown="return checkNumber(event);" value="1" class="st-input-text-300 form-control" />
                        <span class="text-danger">*越大越考前</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        关键词：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_atbiaoqian" name="stwh_atbiaoqian" value=""  class="st-input-text-700 form-control" placeholder="请输入关键词" />
                        <span class="text-danger">*每个关键词以英文逗号隔开，最多支持10个关键词</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        文章作者：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_atauthor" name="stwh_atauthor" value="编辑员"  class="st-input-text-700 form-control" placeholder="请输入文章作者" />
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        文章来源：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_atsource" name="stwh_atsource" value="本站原创"  class="st-input-text-700 form-control" placeholder="请输入文章来源" />
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        来源地址：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_atsourceurl" name="stwh_atsourceurl" value=""  class="st-input-text-700 form-control" placeholder="请输入文章来源地址" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        文章视频：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_atvideourl" name="stwh_atvideourl" value=""  class="st-input-text-700 form-control" placeholder="请输入文章相关视频链接" />
                        <div id="videoImgPanle"><img src="../css/video.jpg" alt="" /></div>
                        <span class="text-danger">* 建议使用第三方视频分享链接（代码），<span id="videoDes">[ 如图 ]</span></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        文章导读：</div>
                    <div class="col-sm-11 form-group">
                        <textarea id="stwh_atjianjie" name="stwh_atjianjie" class="st-input-text-700 form-control" placeholder="请输入文章导读" ></textarea>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        文章内容：</div>
                    <div class="col-sm-11 form-group">
                        <textarea id="stwh_atcontent" name="stwh_atcontent"  style="width:700px; height:300px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="setpanel1">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-sm-1 st-text-right">
                        文章属性：</div>
                    <div class="col-sm-11 form-group">
                        <label><input type="checkbox" name="stwh_atiszhiding" value="1" /> 置顶</label>&nbsp;&nbsp;<label><input type="checkbox" name="stwh_attuijian" value="1" /> 推荐</label>&nbsp;&nbsp;<label><input type="checkbox" name="stwh_attoutiao" value="1" /> 头条</label>&nbsp;&nbsp;<label><input type="checkbox" name="stwh_atgundong" value="1" /> 滚动</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        添加日期：</div>
                    <div class="col-sm-11 form-group">
                        <div class=" input-group st-input-text-300">
                            <input type="text" name="stwh_ataddtime" id="stwh_ataddtime" placeholder="请输入时间"  class="form-control" value="">
                            <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        审核状态：</div>
                    <div class="col-sm-11 form-group">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary btn-sm">
                                <input type="radio" name="stwh_atissh" id="stwh_atissh0" value="0" />
                                待审核
                            </label>
                            <label class="btn btn-primary btn-sm active">
                                <input type="radio" name="stwh_atissh" id="stwh_atissh1" value="1" checked="checked" />
                                已审核
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="setpanel2">
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        seo标题：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-300 form-control" name="stwh_atseotitle" id="stwh_atseotitle" placeholder="请输入seo标题" />
                        <span class="text-danger">*留空则显示文章标题</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        页面关键词：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-700 form-control" name="stwh_atseokeywords" id="stwh_atseokeywords" placeholder="请输入seo关键词" />
                        <span class="text-danger">*留空则显示文章关键词，格式以英文逗号隔开</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        页面描述：</div>
                    <div class="col-sm-11 form-group">
                        <textarea class="st-input-text-700 form-control" name="stwh_atseodescription" id="stwh_atseodescription" placeholder="请输入seo页面描述" ></textarea>
                        <span class="text-danger">*留空则显示文章导读</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script src="/Plugin/jQuery-colorpicker/js/colorpicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_atcontent', qjOptions);

        setTimeout(function () {
            qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            $("#stwh_attitle").focus();
            $(window).scrollTop(0);
        }, 500);
    </script>
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $('#titlecolor').ColorPicker({
                onSubmit: function (hsb, hex, rgb, el) {
                    $(el).val(hex);
                    $(el).ColorPickerHide();
                },
                onBeforeShow: function () {
                    $(this).ColorPickerSetColor(this.value);
                }
            }).bind('keyup', function () {
                $(this).ColorPickerSetColor(this.value);
            });
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                $("#stwh_artid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stylevalue = "";
                if ($("#bold1").is(":checked")) stylevalue = "font-weight:bold;";
                var titlecolor = $("#titlecolor").val();
                if (titlecolor) stylevalue += "color:#" + titlecolor;
                $("#stwh_atstyle").val(stylevalue);
                var stwh_artid = $("#stwh_artid").val();
                if (stwh_artid == "0") {
                    $.bs.alert("请选择归属栏目！", "info");
                    return false;
                }
                var stwh_atimage = $("#stwh_atimage").val();
                var stwh_attitle = $("#stwh_attitle").val();
                if (!stwh_attitle) {
                    $.bs.alert("请输入文章标题！", "info");
                    return false;
                }

                var stwh_atbiaoqian = $("#stwh_atbiaoqian").val();
                if (stwh_atbiaoqian) {
                    var stwh_atbiaoqians = stwh_atbiaoqian.split(',');
                    if (stwh_atbiaoqians.length > 10) {
                        $.bs.alert("关键词数量超过限制！", "info");
                        return false;
                    }
                }
                var stwh_atauthor = $("#stwh_atauthor").val();
                var stwh_atsource = $("#stwh_atsource").val();
                var stwh_atjianjie = $("#stwh_atjianjie").val();
                var stwh_atcontent = qjUEObject.getContent();
                if (!stwh_atcontent) {
                    $.bs.alert("请输入文章内容！", "info");
                    return false;
                }
                var stwh_ataddtime = $("#stwh_ataddtime").val();
                if (!CheckDateTime(stwh_ataddtime)) {
                    $.bs.alert("文章添加时间格式错误！", "info");
                    return false;
                }
                var stwh_atseotitle = $("#stwh_atseotitle").val();
                if (stwh_atseotitle) {
                    if (!IsHanZF(1, 100, stwh_atseotitle)) {
                        $.bs.alert("seo标题长度超出服务器限制100字符！", "info");
                        return false;
                    }
                }
                var stwh_atseokeywords = $("#stwh_atseokeywords").val();
                if (stwh_atseokeywords) {
                    if (!IsHanZF(1, 100, stwh_atseokeywords)) {
                        $.bs.alert("页面关键词长度超出服务器限制100字符！", "info");
                        return false;
                    }
                }
                var stwh_atseodescription = $("#stwh_atseodescription").val();
                if (stwh_atseodescription) {
                    if (!IsHanZF(1, 200, stwh_atseodescription)) {
                        $.bs.alert("页面描述长度超出服务器限制200字符！", "info");
                        return false;
                    }
                }

                $.post("/Handler/stwh_admin/sys_articles/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        qjUEObject.setContent("");
                        //清除本地数据
                        qjUEObject.execCommand("clearlocaldata");
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });

            $("#calendar").click(function () {
                $('#stwh_ataddtime').click();
            });
            $('#stwh_ataddtime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' ' + new Date().Format("hh:mm:ss"));
                }
            }).val(new Date().Format("yyyy-MM-dd hh:mm:ss"));

            $("#imglistPanel").on("click", "img", function () {
                var path = $(this).attr("src");
                var imgliststr = $("#stwh_atimglist").val();
                var imglist = imgliststr.split(',');
                var l1 = imglist.length;
                imglist.removeIndexArray(imglist.IndexOfArray(imgliststr));
                var l2 = imglist.length;
                $("#stwh_atimglist").val(imglist.toStringArray());
                $(this).remove();
            });
            $("#setTab li").click(function () {
                if ($(this).children("a").attr("href") != currentTabId) {
                    $(currentTabId).removeClass("active");
                    $("#setTab li").removeClass("active");
                    $(this).addClass("active");
                    currentTabId = $(this).children("a").attr("href");
                    $(currentTabId).addClass("active");
                    UpdateScroll();
                }
            });
            $("#videoDes").hover(function () {
                $("#videoImgPanle").show();
            }, function () {
                $("#videoImgPanle").hide();
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var flashvarsVoice1 = {
            IsSelect: "true", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "1048576" //1MB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico1", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice1, { btnid: "upwebico1", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            if (btnid=="upwebico") {
                $("#progressFile").show().children().first().css("width", info + "%");
            }
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_atimage").val(msg);
                    }
                    else if (btnid == "upwebico1") {
                        if ($("#stwh_atimglist").val()) {
                            $("#stwh_atimglist").val($("#stwh_atimglist").val() + "," + msg);
                        }
                        else {
                            $("#stwh_atimglist").val(msg);
                        }
                        $("#imglistPanel").append('<img src="' + msg + '" class="img-thumbnail" alt="" style="width:110px; height:110px; margin:0px 10px 10px 0px;" />');
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>