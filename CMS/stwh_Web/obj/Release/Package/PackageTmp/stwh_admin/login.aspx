﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="stwh_Web.stwh_admin.login" %>
<%@ Import Namespace="stwh_Web.stwh_admin.Common" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统</title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
</head>
<body class="login_body">
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <div class="container">
        <div id="loginPanel" class="center-block login" runat="server">
            <div class="pull-left">
                <div class="pull-left">
                    <img src="/stwh_admin/css/default/login.png" width="81" alt="上海舒同文化传播有限公司" />
                </div>
                <div class="pull-left">
                    <div class="t1">
                        舒 同 文 化</div>
                    <div class="t2">
                        <span>企 龙 管 理 系 统</span></div>
                    <div class="t3">
                        <i class="fa fa-qrcode"></i> <%=WebSite.LoadWebSite().Webversion%></div>
                </div>
            </div>
            <div class="pull-left">
                <form id="Form1" role="form" runat="server">
                <div class="pull-left">
                    <div class="form-group clearfix">
                        <div class="pull-left">
                            <asp:TextBox ID="user" runat="server" CssClass="form-control" placeholder="请输入用户名"></asp:TextBox></div>
                        <div class="pull-right">
                            <span class="glyphicon glyphicon-user"></span>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="pull-left">
                            <asp:TextBox ID="pwd" runat="server" CssClass="form-control" TextMode="Password"
                                placeholder="请输入密码"></asp:TextBox></div>
                        <div class="pull-right">
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                    </div>
                    <div id="codediv" class="form-group clearfix" runat="server">
                        <div class="pull-left">
                            <asp:TextBox ID="code" runat="server" CssClass="form-control" MaxLength="4" placeholder="请输入验证码"></asp:TextBox></div>
                        <div class="pull-right">
                            <img id="img_code" class="img-rounded" src="/Handler/securitycode.ashx" alt="点击更换"
                                onclick="ChageImage();" border="0" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="btn-group" data-toggle="buttons">
                            <label id="webcheckla1" class="btn btn-primary btn-sm active">
                                <input type="radio" name="webcheck" id="webcheck0" value="0" checked="checked" />
                                不记住密码
                            </label>
                            <label id="webcheckla2" class="btn btn-primary btn-sm">
                                <input type="radio" name="webcheck" id="webcheck1" value="1" />
                                记住密码
                            </label>
                        </div>
                        <!--默认0加密，1不加密-->
                        <input type="hidden" name="loginck" id="loginCK" value="0" />
                    </div>
                </div>
                <div class="pull-right">
                    <asp:Button ID="ok" runat="server" Text="登录" CssClass="btn btn-default text-center"
                        BorderStyle="None" OnClientClick="return ok_Click();" />
                </div>
                </form>
            </div>
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        //无刷新更换验证码
        function ChageImage() {
            document.getElementById("img_code").src = document.getElementById("img_code").src + "?id=" + Math.random();
        }
        function ok_Click() {
            var user = $("#user").val();
            if (!user) {
                $.bs.alert("请输入用户名！", "warning");
                $("#user").focus();
                return false;
            }
            if (user.length<5 || user.length > 10) {
                $.bs.alert("输入用户名格式错误！", "warning");
                $("#user").focus();
                return false;
            }
            var pwd = $("#pwd").val();
            if (!pwd) {
                $.bs.alert("请输入密码！", "warning");
                $("#pwd").focus();
                return false;
            }
            if (pwd.length < 6 || pwd.length > 12) {
                $.bs.alert("输入密码格式错误！", "warning");
                $("#pwd").focus();
                return false;
            }
            var webcheck = $('input[name="webcheck"]:checked').val();
            var code = 0;
            if ($("#codediv").length > 0) {
                if (!$("#code").val()) {
                    $.bs.alert("请输入验证码！", "warning");
                    $("#code").focus();
                    return false;
                }
                code = $("#code").val();
            }
            $.bs.loading(true);
            $.post(
                "/handler/stwh_admin/login.ashx",
                {
                    user: user,
                    pwd: pwd,
                    code: code,
                    webcheck: webcheck,
                    loginck: $("#loginCK").val()
                },
                function (data) {
                    $.bs.loading(false);
                    if (data.msgcode == "0") {
                        if (data.msg) {
                            qj_storage.removeItem("logindata");
                            qj_storage.setItem("logindata", user + "=" + data.msg);
                        }
                        else {
                            qj_storage.removeItem("logindata");
                        }
                        $.bs.alert("登录系统成功！", "success", "/stwhcms/index");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                },
                "json");
            return false;
        }
        $(function () {
            $("#pwd").keydown(function () {
                $("#loginCK").val("0");
            });
            if (!qj_storage) {
                $.bs.alert("温馨提示：您的浏览器版本过低，无法使用记住密码功能！", "warning",false);
            }
            else {
                //获取本地数据
                var logindata = qj_storage.getItem("logindata");
                if (logindata) {
                    $("#loginCK").val("1");
                    $("#user").val(logindata.split('=')[0]);
                    $("#pwd").val(logindata.split('=')[1]);

                    $("#webcheckla1").removeClass("active");
                    $("#webcheckla2").addClass("active");
                    $("#webcheck0").prop("checked", false);
                    $("#webcheck1").prop("checked", true);
                }
                else {
                    $("#loginCK").val("0");
                }
            }
            $(".login_body").css({
                "background": "url(/stwh_admin/css/loginBG/" + (Math.floor(Math.random() * 6) + 1) + ".jpg) no-repeat",
                "background-size": "1920px 1080px"
            });
            if ($("#codediv").length > 0) {
                $("#loginPanel").addClass("login2");
            }
        });
    </script>
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
