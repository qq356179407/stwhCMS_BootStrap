﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_notices.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                公告类型：</div>
            <div class="col-sm-11 form-group">
                <select id="stwh_notid" name="stwh_notid" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="请选择公告类型" runat="server">
                </select>
                <span class="text-danger">*选择类型</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_noorder" name="stwh_noorder" onkeydown="return checkNumber(event);"
                    value="1" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                公告标题：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_notitle" name="stwh_notitle" value="" class="st-input-text-700 form-control" />
                <span class="text-danger">*标题字数不能超过100字</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                公告简介：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_nojianjie" name="stwh_nojianjie" class="st-input-text-700 form-control"></textarea>
                <span class="text-danger">*简介内容不能超过150字</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                公告内容：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_nocontent" name="stwh_nocontent" style="width: 700px; height: 300px;"></textarea>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                相关文件：</div>
            <div class="col-sm-11 form-group">
                <input id="stwh_nopath" name="stwh_nopath" type="text" runat="server" class="st-input-text-700 form-control"
                    placeholder="请输入上传文件" style="float: left;" />
                <div style="float: left; width: 100px;">
                    <div id="upwebico">
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div id="progressFile" class="progress progress-striped active" style="display: none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $("#addMenu").click(function () {
                var stwh_notid = $("#stwh_notid").val();
                if (!stwh_notid) {
                    $.bs.alert("请选择公告类型！", "info");
                    return false;
                }
                var stwh_notitle = $("#stwh_notitle").val();
                if (!IsHanZF(1, 100, stwh_notitle)) {
                    $.bs.alert("公告标题格式错误，长度控制在100字以内！", "info");
                    return false;
                }
                var stwh_nojianjie = $("#stwh_nojianjie").val();
                if (!IsHanZF(1, 150, stwh_nojianjie)) {
                    $.bs.alert("公告简介格式错误，长度控制在150字以内！", "info");
                    return false;
                }
                if (qjUEObject.getContentLength(true) > 5000) {
                    qjUEObject.focus();
                    $.bs.alert("公告内容超出限制5000字！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_notices/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        qjUEObject.setContent("");
                        //清除本地数据
                        qjUEObject.execCommand("clearlocaldata");
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_nocontent', qjOptions);

        setTimeout(function () {
            qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            $("#stwh_notitle").focus();
        }, 500);
    </script>
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var ttfile = "*." + ("<%=WebSite.Webfiletype %>").replace(/,/ig, ";*.");
        var flashvarsFile = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: ttfile,
            FileType: "image",
            MaxSize: "<%=WebSite.Webfilesize*1024 %>" //500KB 字节为单位
        };
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsFile, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            if (btnid == "upwebico") {
                $("#progressFile").show().children().first().css("width", info + "%");
            }
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_nopath").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
