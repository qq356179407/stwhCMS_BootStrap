﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_logs.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">操作日志管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
            <asp:Literal ID="litBtnList" runat="server"></asp:Literal>
            <div id="searchDiv" class="collapse">
                <div class="form-group">
                    <input type="text" class="form-control" name="stwh_loname" id="stwh_loname" placeholder="请输入操作用户" />
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;<input type="text" class="form-control" name="stwh_loplace" id="stwh_loplace" placeholder="请输入操作地点" />
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;<input type="text" class="form-control" name="stwh_loip" id="stwh_loip" placeholder="请输入IP地址" />
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;
                    <div class=" input-group">
                        <input type="text" name="stwh_lotime" id="stwh_lotime" placeholder="请输入时间"  class="form-control" value="">
                        <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;<a href="#" class="btn btn-default" id="btnsearchOk"><span class="glyphicon glyphicon-search"></span></a>
                </div>
            </div>
        </form>
    </div>
    <div id="scrollPanel">
    <form id="form1" runat="server" class="form-inline">
    <div class="container-fluid">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50px;">
                        <input type="checkbox" id="allParent" />
                    </th>
                    <th style=" width:50px;">
                        编号
                    </th>
                    <th style="width: 150px;">
                        操作用户
                    </th>
                    <th style="width: 170px;">
                        操作时间
                    </th>
                    <th style="width: 200px;">
                        操作地点
                    </th>
                    <th style="width: 150px;">
                        IP地址
                    </th>
                    <th style="width: 130px;">
                        浏览器
                    </th>
                    <th style="width: 130px;">
                        操作系统
                    </th>
                    <th>
                        记录描述
                    </th>
                </tr>
            </thead>
            <tbody id="ChildDatas" runat="server">
            </tbody>
        </table>
    </div>
    <asp:HiddenField ID="hidTotalSum" runat="server" />
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" data-width="55px"
                data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <span id="SumCountSpan"><asp:Literal ID="littotalSum" runat="server"></asp:Literal></span> 条数据
        </div>
        </form>
    </div>
    <div class="modal fade" id="deleteMenuModal" tabindex="-1" role="dialog" aria-labelledby="updatePwdTitle"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="updatePwdTitle">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要清空日志吗（清空后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var pageCount = 15;//每页显示多少条数据
        var ifelse = "";
        //加载数据
        function LoadData(pCount,pIndex,ifstr)
        {
            $.post("/handler/stwh_admin/sys_logs/loaddata.ashx",{
                pageCount:pCount,
                pageIndex:pIndex,
                whereStr:ifstr
            },function(data){
                if (data.msgcode==-1) {
                    $.bs.alert(data.msg, "warning", "");
                }
                else
                {
                    $("#ChildDatas tr").remove();
                    $("#SumCountSpan").text(data.sumcount);
                    if (data.msg.length==0) {
                        $("#ChildDatas").append("<tr><td colspan=\"9\" align=\"center\">暂无数据</td></tr>");
                    }
                    else
                    {
                        $.each(data.msg, function (index, item) {
                            $("#ChildDatas").append("<tr data-id=\"" + item.stwh_loid + "\"><td><td>" + item.stwh_loid + "</td></td><td>" + item.stwh_loname + "</td><td>" + item.stwh_lotime + "</td><td>" + item.stwh_loplace + "</td><td>" + item.stwh_loip + "</td><td>" + item.stwh_lobrowse + "</td><td>" + item.stwh_lodevice + "</td><td>" + item.stwh_loremark + "</td></tr>");
                        });
                        UpdateScroll();
                    }
                }
            },"json");
        }
        //重新设置分页
        function SetPaginator(pageCount,totalsum)
        {
            if (totalsum==0) totalsum = 1;
            $("#pageUl").bootstrapPaginator("setOptions",{
                    currentPage: 1, //当前页面
                    totalPages: totalsum / pageCount + ((totalsum % pageCount) == 0 ? 0 : 1), //总页数
                    numberOfPages: 5, //页码数
                    useBootstrapTooltip: true,
                    onPageChanged: function (event, oldPage, newPage) {
                        if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                    }
                });
        }
        $(function () {
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: <%=totalPages %>, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                }
            });
            $("#btnDeleteAll").click(function () {
                $("#deleteMenuModal").modal('show');
            });
            $("#btnDeleteOk").click(function () {
                $.post(
                "/handler/stwh_admin/sys_logs/deleteALL.ashx",
                {data:null}, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnSearch").click(function(){
                $('#searchDiv').collapse("toggle");
                return false;
            });
            $("#btnsearchOk").click(function(){
                var stwh_loname = $("#stwh_loname").val();
                if (!IsHanZF(1,50,stwh_loname) && stwh_loname != "") {
                    $.bs.alert("操作人员格式错误！", "info");
                    return;
                }
                var stwh_loplace = $("#stwh_loplace").val();
                if(!IsHanZF(1,50,stwh_loplace) && stwh_loplace != "")
                {
                    $.bs.alert("地点格式错误！", "info");
                    return;
                }
                var stwh_loip = $("#stwh_loip").val();
                if(!IsIP(stwh_loip) && stwh_loip != "")
                {
                    $.bs.alert("IP地址格式错误！", "info");
                    return;
                }
                var stwh_lotime = $("#stwh_lotime").val();

                if (stwh_loname=="" && stwh_loplace == "" && stwh_loip == "" && stwh_lotime == "") {
                    $.bs.alert("至少输入一个查询值！", "info");
                    return;
                }
                ifelse = '[{"name":"stwh_loname","value":"'+stwh_loname+'"},{"name":"stwh_loplace","value":"'+stwh_loplace+'"},{"name":"stwh_loip","value":"'+stwh_loip+'"},{"name":"stwh_lotime","value":"'+stwh_lotime+'"}]';
                $.bs.loading(true);
                $.post("/handler/stwh_admin/sys_logs/loaddata.ashx",{
                    pageCount:pageCount,
                    pageIndex:0,
                    whereStr:ifelse
                },function(data){
                    $.bs.loading(false);
                    $('#searchDiv').collapse("toggle");
                    if (data.msgcode==-1) {
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#ChildDatas tr").remove();
                        $("#SumCountSpan").text(data.sumcount);
                        $("#hidTotalSum").val(data.sumcount);
                        SetPaginator(pageCount,parseInt(data.sumcount));
                        if (data.msg.length==0) {
                            $("#ChildDatas").append("<tr><td colspan=\"9\" align=\"center\">暂无数据</td></tr>");
                        }
                        else
                        {
                            $.each(data.msg, function (index, item) {
                                $("#ChildDatas").append("<tr data-id=\"" + item.stwh_loid + "\"><td><td>" + item.stwh_loid + "</td></td><td>" + item.stwh_loname + "</td><td>" + item.stwh_lotime + "</td><td>" + item.stwh_loplace + "</td><td>" + item.stwh_loip + "</td><td>" + item.stwh_lobrowse + "</td><td>" + item.stwh_lodevice + "</td><td>" + item.stwh_loremark + "</td></tr>");
                            });
                            UpdateScroll();
                        }
                    }
                },"json");
                return false;
            });
            $("#calendar").click(function () {
                $('#stwh_lotime').click();
            });
            $('#stwh_lotime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD'));
                }
            });

            $("#basic").change(function(){
                pageCount = $(this).val();
                LoadData(pageCount,0,ifelse);
                var totalsum = parseInt($("#hidTotalSum").val());
                SetPaginator(pageCount,totalsum);
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
