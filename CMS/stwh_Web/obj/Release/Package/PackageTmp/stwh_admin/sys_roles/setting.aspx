﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="setting.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_roles.setting" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <ol class="breadcrumb location scrollTop" style="margin-bottom: 1px;">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">权限分配</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" role="form">
            <div class="container-fluid">
                <asp:HiddenField ID="hidRoleId" runat="server" Value="" />
                <asp:HiddenField ID="hidMenuIds" runat="server" Value="" />
                <asp:HiddenField ID="hidFunctionIds" runat="server" Value="" />
                <asp:HiddenField ID="hidAllData" runat="server" />
                <h4>
                    <strong>角色名称：<asp:Label ID="labRoleName" runat="server" /></strong>&nbsp;&nbsp;<button type="button" class="btn btn-primary btn-xs zhedieall" data-flag="0">反折叠</button></h4>
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <thead>
                        <tr>
                            <th style="width: 250px;">菜单名称
                            </th>
                            <th>功能选项
                            </th>
                        </tr>
                    </thead>
                    <tbody id="ChildDatas" runat="server">
                    </tbody>
                </table>
            </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary btn-sm" id="ckAllCheckbox1">
                    <input type="radio" name="btnradio" value="0" />
                    全选
                </label>
                <label class="btn btn-primary btn-sm" id="ckAllCheckbox2">
                    <input type="radio" name="btnradio" value="1" />
                    反选
                </label>
            </div>
            &nbsp;&nbsp;
        <button id="settingOk" class="btn btn-default">
            <i class="fa fa-save"></i>保存</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllData = JSON.parse($("#hidAllData").val());
        var mid = [<%=mids %>], fid = [<%=fids %>];
        $(function () {
            $(".pmenuid").click(function () {
                //获取菜单id
                var id = $(this).attr("data-id");
                //获取父菜单id
                var pid = $(this).attr("data-pid");
                if ($(this).is(":checked")) {
                    $("#ChildDatas .function:checkbox[data-id='" + id + "']").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas .function:checkbox[data-id='" + id + "']:checked").each(function (i, item) {
                        if (fid.IndexOfArray($(item).val()) == -1) fid.AddArray($(item).val());
                    });
                    $("#hidFunctionIds").val(fid.toStringArray());
                    if (mid.IndexOfArray($(this).val()) == -1) mid.AddArray($(this).val());
                    if (mid.IndexOfArray(pid) == -1) mid.AddArray(pid);
                    $("#hidMenuIds").val(mid.toStringArray());
                }
                else {
                    $("#ChildDatas .function:checkbox[data-id='" + id + "']").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas .function:checkbox[data-id='" + id + "']:not(:checked)").each(function (i, item) {
                        fid.removeIndexArray(fid.IndexOfArray($(item).val()));
                    });
                    $("#hidFunctionIds").val(fid.toStringArray());

                    mid.removeIndexArray(mid.IndexOfArray($(this).val()));
                    var pid_length0 = $("#ChildDatas .pmenuid:checkbox[data-pid='" + pid + "']:not(:checked)").length;
                    var pid_length1 = 0;
                    $.each(AllData, function (index, item) {
                        if (item.stwh_menuparentID == parseInt(pid)) pid_length1 += 1;
                    });
                    if (pid_length0 == pid_length1) {
                        mid.removeIndexArray(mid.IndexOfArray(pid));
                        $(".menuid" + pid).prop("checked", false);
                        var ypid = $(".menuid" + pid).attr("data-pid");
                        var pid_length2 = $("#ChildDatas .pmenuid:checkbox[data-pid='" + ypid + "']:not(:checked)").length;
                        var pid_length3 = 0;
                        $.each(AllData, function (index, item) {
                            if (item.stwh_menuparentID == parseInt(ypid)) pid_length3 += 1;
                        });
                        if (pid_length2 == pid_length3) {
                            mid.removeIndexArray(mid.IndexOfArray(ypid));
                            $(".menuid" + ypid).prop("checked", false);
                        }
                    }
                    $("#hidMenuIds").val(mid.toStringArray());
                }
            }).change(function () {
                if ($(this).is(":checked")) {
                    var pid = $(this).attr("data-pid");
                    $(".menuid" + pid).prop("checked", true);
                    var vv1 = $(".menuid" + pid).val();
                    var ppid = $(".menuid" + pid).attr("data-pid");
                    $(".menuid" + ppid).prop("checked", true);
                    if (mid.IndexOfArray(vv1) == -1) mid.AddArray(vv1);
                    if (mid.IndexOfArray(ppid) == -1) mid.AddArray(ppid);
                    $("#hidMenuIds").val(mid.toStringArray());
                }
            });
            $(".function").click(function () {
                var id = $(this).attr("data-id");
                //获取父菜单id
                var pid = $(this).attr("data-pid");
                if ($(this).is(":checked")) {
                    $(".menuid" + id).prop("checked", true);
                    if (mid.IndexOfArray(id) == -1) mid.AddArray(id);
                    if (mid.IndexOfArray(pid) == -1) mid.AddArray(pid);
                    $("#hidMenuIds").val(mid.toStringArray());
                    if (fid.IndexOfArray($(this).val()) == -1) fid.AddArray($(this).val());
                    $("#hidFunctionIds").val(fid.toStringArray());
                }
                else {
                    if ($("#ChildDatas .function:checkbox[data-id='" + id + "']:not(:checked)").length == $("#ChildDatas .function:checkbox[data-id='" + id + "']").length) {
                        $(".menuid" + id).prop("checked", false);

                        mid.removeIndexArray(mid.IndexOfArray(id));
                        var pid_length0 = $("#ChildDatas .pmenuid:checkbox[data-pid='" + pid + "']:not(:checked)").length;
                        var pid_length1 = 0;
                        $.each(AllData, function (index, item) {
                            if (item.stwh_menuparentID == parseInt(pid)) pid_length1 += 1;
                        });
                        if (pid_length0 == pid_length1) {
                            mid.removeIndexArray(mid.IndexOfArray(pid));
                            $(".menuid" + pid).prop("checked", false);
                            var ypid = $(".menuid" + pid).attr("data-pid");
                            var pid_length2 = $("#ChildDatas .pmenuid:checkbox[data-pid='" + ypid + "']:not(:checked)").length;
                            var pid_length3 = 0;
                            $.each(AllData, function (index, item) {
                                if (item.stwh_menuparentID == parseInt(ypid)) pid_length3 += 1;
                            });
                            if (pid_length2 == pid_length3) {
                                mid.removeIndexArray(mid.IndexOfArray(ypid));
                                $(".menuid" + ypid).prop("checked", false);
                            }
                        }
                        $("#hidMenuIds").val(mid.toStringArray());
                    }
                    fid.removeIndexArray(fid.IndexOfArray($(this).val()));
                    $("#hidFunctionIds").val(fid.toStringArray());
                }
            }).change(function () {
                if ($(this).is(":checked")) {
                    var pid = $(this).attr("data-pid");
                    $(".menuid" + pid).prop("checked", true);
                    var vv1 = $(".menuid" + pid).val();
                    var ppid = $(".menuid" + pid).attr("data-pid");
                    $(".menuid" + ppid).prop("checked", true);
                    if (mid.IndexOfArray(vv1) == -1) mid.AddArray(vv1);
                    if (mid.IndexOfArray(ppid) == -1) mid.AddArray(ppid);
                    $("#hidMenuIds").val(mid.toStringArray());
                }
            });
            $(".zhedie").click(function () {
                var flag = $(this).attr("data-flag");
                var id = $(this).attr("data-id");
                //折叠
                if (flag == 0) {
                    $(this).attr("data-flag", "1").text("展开");
                    $("#ChildDatas tr.menuline" + id).hide();
                    $.each($("#ChildDatas tr.menuline" + id), function (index, item) {
                        var cid = $(item).attr("data-id");
                        $("#ChildDatas tr.menuline" + cid).hide();
                    });
                }
                else {//展开
                    $(this).attr("data-flag", "0").text("折叠");
                    $("#ChildDatas tr.menuline" + id).show();
                    $.each($("#ChildDatas tr.menuline" + id), function (index, item) {
                        var cid = $(item).attr("data-id");
                        $("#ChildDatas tr.menuline" + cid).show();
                    });
                }
                UpdateScroll();
            });
            $(".zhedieall").click(function () {
                var flag = $(this).attr("data-flag");
                if (flag == 0) $(this).attr("data-flag", "1");
                else $(this).attr("data-flag", "0");
                $(".zhedie").click();
            });
            $("#ckAllCheckbox1").click(function () {
                $("#ChildDatas .function:checkbox").each(function (i, item) {
                    if (fid.IndexOfArray($(item).val()) == -1) fid.AddArray($(item).val());
                    $(item).prop("checked", true);
                });
                $("#hidFunctionIds").val(fid.toStringArray());

                $("#ChildDatas .pmenuid:checkbox").each(function (i, item) {
                    if (mid.IndexOfArray($(item).val()) == -1) mid.AddArray($(item).val());
                    if (mid.IndexOfArray($(item).attr("data-pid")) == -1) mid.AddArray($(item).attr("data-pid"));
                    $(item).prop("checked", true);
                });
                $("#hidMenuIds").val(mid.toStringArray());
            });
            $("#ckAllCheckbox2").click(function () {
                $("#ChildDatas .function:checkbox").each(function (i, item) {
                    if ($(item).is(":checked")) {
                        fid.removeIndexArray(fid.IndexOfArray($(item).val()));
                        $(item).prop("checked", false);
                    }
                    else {
                        if (fid.IndexOfArray($(item).val()) == -1) fid.AddArray($(item).val());
                        $(item).prop("checked", true);
                    }
                });
                $("#hidFunctionIds").val(fid.toStringArray());

                $("#ChildDatas .pmenuid:checkbox").each(function (i, item) {
                    if ($(item).is(":checked")) {
                        mid.removeIndexArray(mid.IndexOfArray($(item).val()));
                        mid.removeIndexArray(mid.IndexOfArray($(item).attr("data-pid")));
                        $(item).prop("checked", false);
                    }
                    else {
                        if (mid.IndexOfArray($(item).val()) == -1) mid.AddArray($(item).val());
                        if (mid.IndexOfArray($(item).attr("data-pid")) == -1) mid.AddArray($(item).attr("data-pid"));
                        $(item).prop("checked", true);
                    }
                });
                $("#hidMenuIds").val(mid.toStringArray());
            });
            $("#settingOk").click(function () {
                var mids = $("#hidMenuIds").val();
                var fids = $("#hidFunctionIds").val();
                var rid = $("#hidRoleId").val();
                if (mids == "" || fids == "" || rid == "") {
                    $.bs.alert("请选择菜单和功能！", "warning", "");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_roles/setting.ashx",
                {
                    mids: mids,
                    fids: fids,
                    rid: rid
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        function SetFormHeight() {
            $("#scrollPanel").css("height", (parent.parent.GetMarinRightHeight() - 90) + "px");
        }
        function UpdateScroll() {
            $("#scrollPanel").mCustomScrollbar("update");
        }
        (function ($) {
            $(window).load(function () {
                SetFormHeight();
                $("#scrollPanel").mCustomScrollbar({
                    scrollInertia: 80,
                    autoHideScrollbar: true
                });
            });
        })(jQuery);
    </script>
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
