﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="stwhBT_Meta.ascx.cs"
    Inherits="stwh_Web.UserControls.stwhBT_Meta" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--让部分国产浏览器默认采用高速模式渲染页面-->
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="keywords" content="cms,cms系统,asp,asp系统,系统,网站建设,维护,网站UI设计,手机app" />
<meta name="description" content="上海舒同文化传播有限公司是CMS行业最流行的网站建设解决方案提供商之一。专业提供电子商务网站建设、在线考试系统网站建设、企业网站建设、教育及行业门户网站建设解决方案等于一体的高新技术企业。" />