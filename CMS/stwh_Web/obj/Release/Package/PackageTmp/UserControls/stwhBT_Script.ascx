﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="stwhBT_Script.ascx.cs"
    Inherits="stwh_Web.UserControls.stwhBT_Script" %>
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]>
    <script src="/Plugin/bootstrap-3.3.4/js/ie8-responsive-file-warning.js"  type="text/javascript"></script>
<![endif]-->
<script src="/Plugin/bootstrap-3.3.4/js/ie-emulation-modes-warning.js" type="text/javascript"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"></script>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js" type="text/javascript"></script>
<![endif]-->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/Plugin/bootstrap-3.3.4/js/ie10-viewport-bug-workaround.js" type="text/javascript"></script>
<script src="/Plugin/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="/Plugin/jquery.easing.js" type="text/javascript"></script>
<script src="/Plugin/CustomScrollbar/js/jquery.mCustomScrollbar.min.js" type="text/javascript"></script>
<script src="/Plugin/bootstrap-3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
<!--BootStrap插件-->
<%--<script src="/Plugin/bootstrap-3.3.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/Plugin/bootstrap-3.3.4/js/locale/bootstrap-datepicker-zh_CN.js" type="text/javascript"></script>--%>
<script src="/Plugin/bootstrap-3.3.4/js/moment-with-locales.min.js" type="text/javascript"></script>
<script src="/Plugin/bootstrap-3.3.4/js/daterangepicker.js" type="text/javascript"></script>
<script src="/Plugin/bootstrap-3.3.4/js/bootstrap-paginator.js" type="text/javascript"></script>
<script src="/Plugin/bootstrap-3.3.4/js/locale/bootstrap-paginator-zh_CN.js" type="text/javascript"></script>
<script src="/Plugin/bootstrap-3.3.4/js/bootstrap-select.min.js" type="text/javascript"></script>
<%--<script src="/Plugin/bootstrap-3.3.4/js/bootstrap-switch.js" type="text/javascript"></script>--%>
<script src="/Plugin/bootstrap-3.3.4/js/locale/defaults-zh_CN.js" type="text/javascript"></script>
<!--常用工具包-->
<script src="/Plugin/json2.js" type="text/javascript"></script>
<script src="/Plugin/stwhCommon.js" type="text/javascript"></script>
<!--BootStrap工具包-->
<script src="/Plugin/stwhBSCommon.js" type="text/javascript"></script>
<!--自定义脚本-->
<script src="/Plugin/stwhcms.js" type="text/javascript"></script>
<script type="text/javascript">
    moment.locale("zh_cn");
</script>
