﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="stwhBT_NavbarJS.ascx.cs" Inherits="stwh_Web.UserControls.stwhBT_NavbarJS" %>
<script type="text/javascript">
    var mleft_w = parseFloat($("#mainLeft").width() / $(window).width()) * 100, mright_w = parseFloat($("#mainRight").width() / $(window).width()) * 100;
    var showTime = 500;
    var showType = "easeInOutBack";
    var isUpdateCache = "0",
        timesum = 30, //单位秒
        timecount = timesum;
    var stoptimeid;
    function StartTimeCache() {
        stoptimeid = setInterval(function () {
            if (timecount == 0) {
                timecount = timesum;
                isUpdateCache = "0";
                clearInterval(stoptimeid);
            }
            else {
                isUpdateCache = "1";
                timecount -= 1;
            }
        }, 1000);
    }
    function updatePwdOk_Click() {
        var txtNewPwd = $("#txtNewPwd").val();
        if (!txtNewPwd) {
            $.bs.alert("请输入新密码！", "info");
            return false;
        }
        else if (!IsPwdComplex(txtNewPwd)) {
            $.bs.alert("新密码格式错误，必须包含字母、数字、特称字符(+=_!@#&)，至少8个字符，最多12个字符！", "danger");
            return false;
        }
        var txtNewPwdOk = $("#txtNewPwdOk").val();
        if (!txtNewPwdOk) {
            $.bs.alert("请输入确认密码！", "info");
            return false;
        }
        if (txtNewPwd != txtNewPwdOk) {
            $.bs.alert("两次密码输入不一致！", "info");
            return false;
        }
        $.post(
                "/handler/stwh_admin/updatePwd.ashx",
                {
                    NewPwd: txtNewPwd,
                    NewPwdOk: txtNewPwdOk
                },
                function (data) {
                    if (data.msgcode == "0") {
                        $("#updatePwd").modal('hide');
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
        return false;
    }
    //供iframe页面中调用
    function ShowNotice(title, msg) {
        $("#NoticeTitle span").text(title);
        $("#NoticeContent").html(msg);
        $("#NoticeWindow").modal('show');
    }
    function ShowVoiceNotice(msg) {
        $.bs.speak(msg);
    }
    //供iframe页面中调用
    function GetMarinRightHeight() {
        return $("#mainRight").innerHeight();
    }
    $(function () {
        StartTimeCache();
        $("#accordion").on("click", ".acchead", function () {
            $("#accordion .acchead").removeClass("acchead_hover");
            $(this).addClass("acchead_hover");
            $(this).children("span").last().toggleClass("glyphicon-menu-up");
            setTimeout(function () {
                $("#mainLeft").mCustomScrollbar("update");
            }, 100);
        });
        $("#accordion").on("click",".childrenm", function () {
            $("#accordion .childrenm").removeClass("accmenu_hover");
            $(this).addClass("accmenu_hover");
            $("#mainframe").attr("src", $(this).attr("href"));
            return false;  
        });
        $("#indexLogo").click(function () {
            $("#mainframe").attr("src", "/stwh_admin/main.aspx");
            return false;
        });
        $("#exit").click(function () {
            $.bs.speak("你确定要退出系统吗？");
            $("#myExit").modal('show');
        });
        $("#sysSound").click(function () {
            if (!qj_storage) {
                $.bs.alert("温馨提示：您的浏览器版本过低，无法使用系统提示音！", "warning");
            }
            else {
                if ($(this).attr("data-id") == "0") {
                    $(this).attr("title", "开启系统提示音").html('<span class="fa fa-volume-up"></span>');
                    $(this).attr("data-id", "1");

                    qj_storage.removeItem("sysSound");
                    qj_storage.setItem("sysSound", "1");

                    $.bs.options = {
                        isHide: true,
                        isSpeak: qj_storage.getItem("sysSound") == 0 ? true : false,
                        hideTime: 1500
                    };
                }
                else {
                    $(this).attr("title", "关闭系统提示音").html('<span class="fa fa-volume-off"></span>');
                    $(this).attr("data-id", "0");

                    qj_storage.removeItem("sysSound");
                    qj_storage.setItem("sysSound", "0");

                    $.bs.options = {
                        isHide: true,
                        isSpeak: qj_storage.getItem("sysSound") == 0 ? true : false,
                        hideTime: 1500
                    };
                }
            }
        });
        $("#updateCache").click(function () {
            if (isUpdateCache == "0") {
                $.post(
                    "/handler/stwh_admin/updateCache.ashx",
                    function (data) {
                        if (data.msgcode == "0") {
                            $.bs.alert(data.msg, "success", "this");
                        }
                        else {
                            $.bs.alert(data.msg, "danger");
                        }
                    }, "json");
                StartTimeCache();
            }
            else {
                $.bs.alert(timecount + "秒后，再进行操作！", "info");
            }
            return false;
        });
        $(".mainBtnPanel").css("left", parseFloat($("#mainLeft").width() / $(window).width()) * 100 + "%").click(function () {
            if ($(this).attr("data-id") == "0") {
                $(this).attr("data-id", "1").animate({ left: 0 }, showTime, showType);
                $("#mainLeft").animate({ left: "-" + mleft_w + "%" }, showTime, showType);
                $("#mainRight").animate({ width: "100%" }, showTime, showType);
                $(".mainBtnPanel span").removeClass("glyphicon-menu-left").addClass("glyphicon-menu-right");
            }
            else {
                $(this).attr("data-id", "0").animate({ left: mleft_w + "%" }, showTime, showType);
                $("#mainLeft").animate({ left: "0px" }, showTime, showType);
                $("#mainRight").animate({ width: (100 - mleft_w) + "%" }, showTime, showType);
                $(".mainBtnPanel span").removeClass("glyphicon-menu-right").addClass("glyphicon-menu-left");
            }
        });
        $(window).resize(function () {
            mleft_w = parseFloat($("#mainLeft").width() / $(window).width()) * 100;
            mright_w = parseFloat($("#mainRight").width() / $(window).width()) * 100;
            //调用iframe页面中的函数
            self.frames["mainframe"].SetFormHeight();
        });

        if (qj_storage) {
            if (qj_storage.getItem("sysSound") == "0") {
                $("#sysSound").attr("title", "关闭系统提示音").html('<span class="fa fa-volume-off"></span>');
                $("#sysSound").attr("data-id", "0");

                $.bs.options = {
                    isHide: true,
                    isSpeak: qj_storage.getItem("sysSound") == 0 ? true : false,
                    hideTime: 1500
                };
            }
            else {
                $("#sysSound").attr("title", "开启系统提示音").html('<span class="fa fa-volume-up"></span>');
                $("#sysSound").attr("data-id", "1");

                $.bs.options = {
                    isHide: true,
                    isSpeak: qj_storage.getItem("sysSound") == 0 ? true : false,
                    hideTime: 1500
                };
            }
        }

        $("#btnZD").click(function () {
            $("#accordion .accmenu").removeClass("in").css("height", "auto");
            $("#accordion .acchead span.pull-right").addClass("glyphicon-menu-up");
            $("#mainLeft").mCustomScrollbar("update");
        });
        $("#btnZK").click(function () {
            $("#accordion .accmenu").addClass("in").css("height", "auto");
            $("#accordion .acchead span.pull-right").removeClass("glyphicon-menu-up");
            $("#mainLeft").mCustomScrollbar("update");
        });
    });
    </script>
    <script type="text/javascript">
        (function ($) {
            $(window).load(function () {
                $("#mainLeft").mCustomScrollbar({
                    scrollInertia: 100,
                    autoHideScrollbar: true
                });
            });
        })(jQuery);
    </script>