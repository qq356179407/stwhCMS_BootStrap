﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace stwh_Web.UserControls
{
    public partial class stwhUE_Script : System.Web.UI.UserControl
    {
        public string Language = "zh-cn";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Language = stwh_admin.Common.WebSite.LoadWebSite().Weblanguage == 1 ? "en" : "zh-cn";
            }
            catch (Exception)
            {
                Language = "zh-cn";
            }
        }
    }
}