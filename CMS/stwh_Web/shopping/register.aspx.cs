﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//导入命名空间
using stwh_Web.stwh_admin.Common;

namespace stwh_Web.shopping
{
    public partial class register : WebPageShopBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageBaseVT pbvt = new PageBaseShoppingVT("register");
            pbvt.OutPutHtml();
        }
    }
}