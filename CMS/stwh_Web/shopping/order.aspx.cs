﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;
using stwh_Model;
using stwh_BLL;

namespace stwh_Web.shopping
{
    /// <summary>
    /// 订单确认页面
    /// </summary>
    public partial class order : WebPageShopBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //判断是否登录
            stwh_buyuser model = CheckLogin();
            if (model == null)
            {
                Response.Redirect("/shop/login.html?reUrl=/shop/order.html");
            }
            else
            {
                PageBaseVT pbvt = new PageBaseShoppingVT("order");
                pbvt.OutPutHtml();
            }
        }
    }
}