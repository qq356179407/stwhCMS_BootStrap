﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Model;
using stwh_Web.stwh_admin.Common;

namespace stwh_Web.shopping
{
    public partial class noticy_success : WebPageShopBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //判断是否登录
            stwh_buyuser model = CheckLogin();
            if (model == null)
            {
                Response.Redirect("/shop/login.html?reUrl=/shop/user.html");
            }
            else
            {
                PageBaseVT pbvt = new PageBaseShoppingVT("noticy_success");
                pbvt.OutPutHtml();
            }
        }
    }
}