﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;
using stwh_Model;
using stwh_BLL;
using stwh_Common;

namespace stwh_Web.shopping
{
    /// <summary>
    /// 支付方式选择
    /// </summary>
    public partial class order_pay : WebPageShopBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //判断是否登录
            stwh_buyuser model = CheckLogin();
            string orderid = Request["orderid"];
            if (model == null) Response.Redirect("/shop/login.html?reUrl=/shop/user.html");
            else if (!string.IsNullOrEmpty(orderid))
            {
                if (!PageValidate.IsCustom(@"^\d{16,}$", orderid)) throw new Exception("数据格式非法，请从网站正常提交！！");
            }
            PageBaseVT pbvt = new PageBaseShoppingVT("order_pay");
            pbvt.OutPutHtml();
        }
    }
}