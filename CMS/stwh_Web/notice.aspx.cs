﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;

namespace stwh_Web
{
    public partial class notice : WebPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageBaseVT pbvt = new PageBaseVT("notice");
            pbvt.OutPutHtml();
        }
    }
}