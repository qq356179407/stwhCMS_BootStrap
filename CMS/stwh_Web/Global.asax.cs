﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace stwh_Web
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // 在应用程序启动时运行的代码
        }

        void Application_End(object sender, EventArgs e)
        {
            //  在应用程序关闭时运行的代码
        }

        void Application_Error(object sender, EventArgs e)
        {
            /*
             程序调试是请注释下面代码，发布时请取消注释
             */
            //Exception exception = Server.GetLastError();
            //Response.Clear();
            //HttpException httpException = exception as HttpException;
            //int errorCode = (httpException == null ? 0 : httpException.GetHttpCode());
            //Server.ClearError();
            //switch (errorCode)
            //{
            //    case 403:
            //        Response.Redirect("/sys403.html");
            //        break;
            //    case 404:
            //        Response.Redirect("/sys404.html");
            //        break;
            //    case 500:
            //        stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
            //        Response.Redirect("/sys500.html");
            //        break;
            //    default:
            //        stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
            //        Response.Redirect("/sysother.html");
            //        break;
            //}
        }

        void Session_Start(object sender, EventArgs e)
        {
            // 在新会话启动时运行的代码

        }

        void Session_End(object sender, EventArgs e)
        {
            // 在会话结束时运行的代码。 
            // 注意: 只有在 Web.config 文件中的 sessionstate 模式设置为
            // InProc 时，才会引发 Session_End 事件。如果会话模式设置为 StateServer 
            // 或 SQLServer，则不会引发该事件。

        }

        #region 解决flash上传文件时session丢失问题
        /// <summary>
        /// 请求之前执行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            ////获取请求路径
            //string requestPath = HttpContext.Current.Request.Url.LocalPath.Trim().ToLower();

            //string oldpath = ("/Handler/uploadHandler.ashx").ToLower();
            //IsCompareAdd(requestPath, oldpath);
        }

        ///// <summary>
        ///// 比较路径是否相等，并添加
        ///// </summary>
        ///// <param name="requestPath"></param>
        ///// <param name="oldpath"></param>
        //private void IsCompareAdd(string requestPath, string oldpath)
        //{
        //    if (string.Compare(requestPath, oldpath) == 0)
        //    {
        //        string param_name = "ASPSESSID";
        //        string cookie_name = "ASP.NET_SessionId";
        //        IfCookie(param_name, cookie_name);

        //        param_name = "AUTHID";
        //        cookie_name = FormsAuthentication.FormsCookieName;
        //        IfCookie(param_name, cookie_name);
        //    }
        //}

        ///// <summary>
        ///// 判断cookie是否存在
        ///// </summary>
        ///// <param name="param_name">表单名称</param>
        ///// <param name="cookie_name">cookie名称</param>
        //private void IfCookie(string param_name, string cookie_name)
        //{
        //    string forms = HttpContext.Current.Request.Form[param_name];
        //    string query = HttpContext.Current.Request.QueryString[param_name];
        //    if (!string.IsNullOrEmpty(forms)) UpdateCookie(cookie_name, forms);
        //    else if (!string.IsNullOrEmpty(query)) UpdateCookie(cookie_name, query);
        //}

        ///// <summary>
        ///// 更新当前请求中的cookie信息
        ///// </summary>
        ///// <param name="cookiename">cookie名称</param>
        ///// <param name="cookievalue">cookie值</param>
        //private void UpdateCookie(string cookiename, string cookievalue)
        //{
        //    HttpCookie cookie = HttpContext.Current.Request.Cookies[cookiename];
        //    if (cookie == null) cookie = new HttpCookie(cookiename);
        //    cookie.Value = cookievalue;
        //    HttpContext.Current.Request.Cookies.Set(cookie);
        //}
        #endregion

    }
}
