﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_shoporder.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">订单管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
            <div class="container-fluid">
                <div class="row">
                    <input type="hidden" value="<%=UpdateModel.stwh_orid %>" name="stwh_orid" id="stwh_orid" />
                    <div class="col-sm-1 st-text-right">
                        订单编号：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" value="<%=UpdateModel.stwh_orddid %>" name="stwh_orddid" id="stwh_orddid" /><%=UpdateModel.stwh_orddid %>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        订单状态：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" value="<%=UpdateModel.stwh_orstatus %>" name="stwh_orstatus" id="stwh_orstatus" />
                        <%
                            if (UpdateModel.stwh_orstatus == 0)
                            { %>待支付<%}
                            else if (UpdateModel.stwh_orstatus == 1)
                            { %>已支付<%}
                            else
                            { %>已取消<%}
                        %>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        订单支付方式：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" value="<%=UpdateModel.stwh_orpaystyle %>" name="stwh_orpaystyle" id="stwh_orpaystyle" />
                        <%
                            if (UpdateModel.stwh_orpaystyle == 0)
                            { %>无<%}
                            else if (UpdateModel.stwh_orpaystyle == 1)
                            { %>微信<%}
                            else if (UpdateModel.stwh_orpaystyle == 2)
                            { %>支付宝<%}
                            else if (UpdateModel.stwh_orpaystyle == 3)
                            { %>银行卡<%}
                            else
                            {%>到付<%}
                        %>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        收货详细信息：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" value="<%=UpdateModel.stwh_buid %>" name="stwh_buid" id="stwh_buid" />
                        <input type="hidden" value="<%=UpdateModel.stwh_oruser %>" name="stwh_oruser" id="stwh_oruser" />
                        <input type="hidden" value="<%=UpdateModel.stwh_ortel %>" name="stwh_ortel" id="stwh_ortel" />
                        <input type="hidden" value="<%=UpdateModel.stwh_oryoubian %>" name="stwh_oryoubian" id="stwh_oryoubian" />
                        <input type="hidden" value="<%=UpdateModel.stwh_ortime %>" name="stwh_ortime" id="stwh_ortime" />
                        <input type="hidden" value="<%=UpdateModel.stwh_orcity %>" name="stwh_orcity" id="stwh_orcity" />
                        <input type="hidden" value="<%=UpdateModel.stwh_oraddress %>" name="stwh_oraddress" id="stwh_oraddress" /><%=UpdateModel.stwh_oruser %> <%=UpdateModel.stwh_ortel %> <%=UpdateModel.stwh_oraddress %>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        快递方式：
                    </div>
                    <div class="col-sm-11 form-group">
                        <select class="selectpicker" name="stwh_oryjstyle" id="stwh_oryjstyle" data-live-search="true" data-live-search-style="begins">
                            <option value="AOL澳通速递">AOL澳通速递
                            </option>
                            <option value="A2U速递">A2U速递
                            </option>
                            <option value="AAE快递">AAE快递
                            </option>
                            <option value="安能物流">安能物流
                            </option>
                            <option value="安迅物流">安迅物流
                            </option>
                            <option value="澳邮中国快运">澳邮中国快运
                            </option>
                            <option value="安鲜达">安鲜达
                            </option>
                            <option value="安捷物流">安捷物流
                            </option>
                            <option value="澳多多国际速递">澳多多国际速递
                            </option>
                            <option value="艾瑞斯远">艾瑞斯远
                            </option>
                            <option value="BCWELT">BCWELT
                            </option>
                            <option value="百世快运">百世快运
                            </option>
                            <option value="百世快递">百世快递
                            </option>
                            <option value="包裹/平邮/挂号信">包裹/平邮/挂号信
                            </option>
                            <option value="百福东方物流">百福东方物流
                            </option>
                            <option value="邦送物流">邦送物流
                            </option>
                            <option value="北青小红帽">北青小红帽
                            </option>
                            <option value="宝凯物流">宝凯物流
                            </option>
                            <option value="百千诚物流">百千诚物流
                            </option>
                            <option value="博源恒通">博源恒通
                            </option>
                            <option value="百成大达物流">百成大达物流
                            </option>
                            <option value="百腾物流">百腾物流
                            </option>
                            <option value="巴伦支快递">巴伦支快递
                            </option>
                            <option value="笨鸟海淘">笨鸟海淘
                            </option>
                            <option value="百事亨通">百事亨通
                            </option>
                            <option value="奔腾物流">奔腾物流
                            </option>
                            <option value="布谷鸟速递">布谷鸟速递
                            </option>
                            <option value="邦工快运">邦工快运
                            </option>
                            <option value="堡昕德速递">堡昕德速递
                            </option>
                            <option value="斑马物联网">斑马物联网
                            </option>
                            <option value="北极星快运">北极星快运
                            </option>
                            <option value="CE易欧通国际速递">CE易欧通国际速递
                            </option>
                            <option value="COE（东方快递）">COE（东方快递）
                            </option>
                            <option value="城市100">城市100
                            </option>
                            <option value="城际速递">城际速递
                            </option>
                            <option value="成都立即送">成都立即送
                            </option>
                            <option value="出口易">出口易
                            </option>
                            <option value="晟邦物流">晟邦物流
                            </option>
                            <option value="程光快递">程光快递
                            </option>
                            <option value="传喜物流">传喜物流
                            </option>
                            <option value="钏博物流">钏博物流
                            </option>
                            <option value="城铁物流">城铁物流
                            </option>
                            <option value="CNUP 中联邮">CNUP 中联邮
                            </option>
                            <option value="DHL中国">DHL中国
                            </option>
                            <option value="DHL国际">DHL国际
                            </option>
                            <option value="DHL德国">DHL德国
                            </option>
                            <option value="德邦">德邦
                            </option>
                            <option value="大田物流">大田物流
                            </option>
                            <option value="东方快递">东方快递
                            </option>
                            <option value="递四方">递四方
                            </option>
                            <option value="大洋物流">大洋物流
                            </option>
                            <option value="店通快递">店通快递
                            </option>
                            <option value="德创物流">德创物流
                            </option>
                            <option value="东红物流">东红物流
                            </option>
                            <option value="D速物流">D速物流
                            </option>
                            <option value="东瀚物流">东瀚物流
                            </option>
                            <option value="达方物流">达方物流
                            </option>
                            <option value="东骏快捷物流">东骏快捷物流
                            </option>
                            <option value="叮咚澳洲转运">叮咚澳洲转运
                            </option>
                            <option value="大众佐川急便">大众佐川急便
                            </option>
                            <option value="德方物流">德方物流
                            </option>
                            <option value="德中快递">德中快递
                            </option>
                            <option value="德坤供应链">德坤供应链
                            </option>
                            <option value="达速物流">达速物流
                            </option>
                            <option value="德豪驿">德豪驿
                            </option>
                            <option value="DHL Paket">DHL Paket
                            </option>
                            <option value="德国优拜物流">德国优拜物流
                            </option>
                            <option value="德国雄鹰速递">德国雄鹰速递
                            </option>
                            <option value="德国云快递">德国云快递
                            </option>
                            <option value="EU-EXPRESS">EU-EXPRESS
                            </option>
                            <option value="EMS快递查询">EMS快递查询
                            </option>
                            <option value="EMS国际快递查询">EMS国际快递查询
                            </option>
                            <option value="俄顺达">俄顺达
                            </option>
                            <option value="EWE全球快递">EWE全球快递
                            </option>
                            <option value="FedEx快递查询">FedEx快递查询
                            </option>
                            <option value="FedEx美国">FedEx美国
                            </option>
                            <option value="FedEx英国">FedEx英国
                            </option>
                            <option value="FOX国际速递">FOX国际速递
                            </option>
                            <option value="凡客如风达">凡客如风达
                            </option>
                            <option value="飞康达物流">飞康达物流
                            </option>
                            <option value="飞豹快递">飞豹快递
                            </option>
                            <option value="飞狐快递">飞狐快递
                            </option>
                            <option value="凡宇速递">凡宇速递
                            </option>
                            <option value="颿达国际快递">颿达国际快递
                            </option>
                            <option value="飞远配送">飞远配送
                            </option>
                            <option value="飞鹰物流">飞鹰物流
                            </option>
                            <option value="风行天下">风行天下
                            </option>
                            <option value="飞力士物流">飞力士物流
                            </option>
                            <option value="飞邦快递">飞邦快递
                            </option>
                            <option value="丰程物流">丰程物流
                            </option>
                            <option value="泛远国际物流">泛远国际物流
                            </option>
                            <option value="疯狂快递">疯狂快递
                            </option>
                            <option value="法翔速运">法翔速运
                            </option>
                            <option value="富腾达快递">富腾达快递
                            </option>
                            <option value="GLS">GLS
                            </option>
                            <option value="GSM">GSM
                            </option>
                            <option value="GATI快递">GATI快递
                            </option>
                            <option value="GTS快递">GTS快递
                            </option>
                            <option value="挂号信/国内邮件">挂号信/国内邮件
                            </option>
                            <option value="国通快递">国通快递
                            </option>
                            <option value="国际邮件查询">国际邮件查询
                            </option>
                            <option value="港中能达物流">港中能达物流
                            </option>
                            <option value="港快速递">港快速递
                            </option>
                            <option value="共速达">共速达
                            </option>
                            <option value="广通速递（山东）">广通速递（山东）
                            </option>
                            <option value="高铁速递">高铁速递
                            </option>
                            <option value="冠达快递">冠达快递
                            </option>
                            <option value="哥士传奇速递">哥士传奇速递
                            </option>
                            <option value="冠捷物流">冠捷物流
                            </option>
                            <option value="国晶物流">国晶物流
                            </option>
                            <option value="华宇物流">华宇物流
                            </option>
                            <option value="恒路物流">恒路物流
                            </option>
                            <option value="好来运快递">好来运快递
                            </option>
                            <option value="华夏龙物流">华夏龙物流
                            </option>
                            <option value="海航天天">海航天天
                            </option>
                            <option value="河北建华">河北建华
                            </option>
                            <option value="海盟速递">海盟速递
                            </option>
                            <option value="华企快运">华企快运
                            </option>
                            <option value="昊盛物流">昊盛物流
                            </option>
                            <option value="户通物流">户通物流
                            </option>
                            <option value="华航快递">华航快递
                            </option>
                            <option value="黄马甲快递">黄马甲快递
                            </option>
                            <option value="合众速递（UCS）">合众速递（UCS）
                            </option>
                            <option value="皇家物流">皇家物流
                            </option>
                            <option value="伙伴物流">伙伴物流
                            </option>
                            <option value="红马速递">红马速递
                            </option>
                            <option value="汇文配送">汇文配送
                            </option>
                            <option value="华赫物流">华赫物流
                            </option>
                            <option value="猴急送">猴急送
                            </option>
                            <option value="航宇快递">航宇快递
                            </option>
                            <option value="辉联物流">辉联物流
                            </option>
                            <option value="环球速运">环球速运
                            </option>
                            <option value="华达快运">华达快运
                            </option>
                            <option value="华通务达物流">华通务达物流
                            </option>
                            <option value="海派通">海派通
                            </option>
                            <option value="环球通达">环球通达
                            </option>
                            <option value="航空快递">航空快递
                            </option>
                            <option value="好又快物流">好又快物流
                            </option>
                            <option value="韩润物流">韩润物流
                            </option>
                            <option value="河南次晨达">河南次晨达
                            </option>
                            <option value="和丰同城">和丰同城
                            </option>
                            <option value="荷兰 Sky Post">荷兰 Sky Post
                            </option>
                            <option value="鸿讯物流">鸿讯物流
                            </option>
                            <option value="宏捷国际物流">宏捷国际物流
                            </option>
                            <option value="汇通天下物流">汇通天下物流
                            </option>
                            <option value="恒通快递">恒通快递
                            </option>
                            <option value="黑狗物流">黑狗物流
                            </option>
                            <option value="恒宇运通">恒宇运通
                            </option>
                            <option value="恒瑞物流">恒瑞物流
                            </option>
                            <option value="环创物流">环创物流
                            </option>
                            <option value="河南全速通">河南全速通
                            </option>
                            <option value="佳吉物流">佳吉物流
                            </option>
                            <option value="佳怡物流">佳怡物流
                            </option>
                            <option value="加运美快递">加运美快递
                            </option>
                            <option value="急先达物流">急先达物流
                            </option>
                            <option value="京广速递快件">京广速递快件
                            </option>
                            <option value="晋越快递">晋越快递
                            </option>
                            <option value="京东快递">京东快递
                            </option>
                            <option value="捷特快递">捷特快递
                            </option>
                            <option value="久易快递">久易快递
                            </option>
                            <option value="九曳供应链">九曳供应链
                            </option>
                            <option value="骏丰国际速递">骏丰国际速递
                            </option>
                            <option value="佳家通">佳家通
                            </option>
                            <option value="吉日优派">吉日优派
                            </option>
                            <option value="锦程国际物流">锦程国际物流
                            </option>
                            <option value="景光物流">景光物流
                            </option>
                            <option value="急顺通">急顺通
                            </option>
                            <option value="捷网俄全通">捷网俄全通
                            </option>
                            <option value="嘉里大通">嘉里大通
                            </option>
                            <option value="金马甲">金马甲
                            </option>
                            <option value="佳成快递">佳成快递
                            </option>
                            <option value="骏绅物流">骏绅物流
                            </option>
                            <option value="锦程快递">锦程快递
                            </option>
                            <option value="捷安达国际速递">捷安达国际速递
                            </option>
                            <option value="快捷快递">快捷快递
                            </option>
                            <option value="康力物流">康力物流
                            </option>
                            <option value="跨越速运">跨越速运
                            </option>
                            <option value="快优达速递">快优达速递
                            </option>
                            <option value="快淘快递">快淘快递
                            </option>
                            <option value="开心快递">开心快递
                            </option>
                            <option value="快速递">快速递
                            </option>
                            <option value="联邦快递">联邦快递
                            </option>
                            <option value="联昊通物流">联昊通物流
                            </option>
                            <option value="龙邦速递">龙邦速递
                            </option>
                            <option value="乐捷递">乐捷递
                            </option>
                            <option value="立即送">立即送
                            </option>
                            <option value="蓝弧快递">蓝弧快递
                            </option>
                            <option value="乐天速递">乐天速递
                            </option>
                            <option value="鲁通快运">鲁通快运
                            </option>
                            <option value="乐递供应链">乐递供应链
                            </option>
                            <option value="论道国际物流">论道国际物流
                            </option>
                            <option value="林安物流">林安物流
                            </option>
                            <option value="6LS EXPRESS">6LS EXPRESS
                            </option>
                            <option value="ME物流">ME物流
                            </option>
                            <option value="民航快递">民航快递
                            </option>
                            <option value="美国快递">美国快递
                            </option>
                            <option value="门对门">门对门
                            </option>
                            <option value="明亮物流">明亮物流
                            </option>
                            <option value="民邦速递">民邦速递
                            </option>
                            <option value="闽盛快递">闽盛快递
                            </option>
                            <option value="麦力快递">麦力快递
                            </option>
                            <option value="木春货运">木春货运
                            </option>
                            <option value="美快国际物流">美快国际物流
                            </option>
                            <option value="美通快递">美通快递
                            </option>
                            <option value="马珂博逻">马珂博逻
                            </option>
                            <option value="迈隆递运">迈隆递运
                            </option>
                            <option value="能达速递">能达速递
                            </option>
                            <option value="偌亚奥国际">偌亚奥国际
                            </option>
                            <option value="诺尔国际物流">诺尔国际物流
                            </option>
                            <option value="尼尔快递">尼尔快递
                            </option>
                            <option value="OCS">OCS
                            </option>
                            <option value="欧亚专线">欧亚专线
                            </option>
                            <option value="PCA Express">PCA Express
                            </option>
                            <option value="平安达腾飞">平安达腾飞
                            </option>
                            <option value="陪行物流">陪行物流
                            </option>
                            <option value="品骏快递">品骏快递
                            </option>
                            <option value="鹏远国际速递">鹏远国际速递
                            </option>
                            <option value="PostElbe">PostElbe
                            </option>
                            <option value="全峰快递">全峰快递
                            </option>
                            <option value="全一快递">全一快递
                            </option>
                            <option value="全日通快递">全日通快递
                            </option>
                            <option value="全晨快递">全晨快递
                            </option>
                            <option value="7天连锁物流">7天连锁物流
                            </option>
                            <option value="秦邦快运">秦邦快运
                            </option>
                            <option value="全信通快递">全信通快递
                            </option>
                            <option value="全速通国际快递">全速通国际快递
                            </option>
                            <option value="秦远物流">秦远物流
                            </option>
                            <option value="启辰国际物流">启辰国际物流
                            </option>
                            <option value="全速快运">全速快运
                            </option>
                            <option value="全之鑫物流">全之鑫物流
                            </option>
                            <option value="千顺快递">千顺快递
                            </option>
                            <option value="全时速运">全时速运
                            </option>
                            <option value="如风达快递">如风达快递
                            </option>
                            <option value="日昱物流">日昱物流
                            </option>
                            <option value="瑞丰速递">瑞丰速递
                            </option>
                            <option value="日日顺物流">日日顺物流
                            </option>
                            <option value="日益通速递">日益通速递
                            </option>
                            <option value="瑞达国际速递">瑞达国际速递
                            </option>
                            <option value="日日顺快线">日日顺快线
                            </option>
                            <option value="容智物流速运">容智物流速运
                            </option>
                            <option value="顺丰速运">顺丰速运
                            </option>
                            <option value="申通快递">申通快递
                            </option>
                            <option value="速尔快递">速尔快递
                            </option>
                            <option value="山东海红">山东海红
                            </option>
                            <option value="盛辉物流">盛辉物流
                            </option>
                            <option value="世运快递">世运快递
                            </option>
                            <option value="盛丰物流">盛丰物流
                            </option>
                            <option value="上大物流">上大物流
                            </option>
                            <option value="三态速递">三态速递
                            </option>
                            <option value="赛澳递">赛澳递
                            </option>
                            <option value="申通E物流">申通E物流
                            </option>
                            <option value="圣安物流">圣安物流
                            </option>
                            <option value="山西红马甲">山西红马甲
                            </option>
                            <option value="穗佳物流">穗佳物流
                            </option>
                            <option value="沈阳佳惠尔">沈阳佳惠尔
                            </option>
                            <option value="上海林道货运">上海林道货运
                            </option>
                            <option value="十方通物流">十方通物流
                            </option>
                            <option value="山东广通速递">山东广通速递
                            </option>
                            <option value="顺捷丰达">顺捷丰达
                            </option>
                            <option value="速必达物流">速必达物流
                            </option>
                            <option value="速通成达物流">速通成达物流
                            </option>
                            <option value="速腾物流">速腾物流
                            </option>
                            <option value="速方国际物流">速方国际物流
                            </option>
                            <option value="顺通快递">顺通快递
                            </option>
                            <option value="速递中国">速递中国
                            </option>
                            <option value="苏宁快递">苏宁快递
                            </option>
                            <option value="四海快递">四海快递
                            </option>
                            <option value="首通快运">首通快运
                            </option>
                            <option value="顺时达物流">顺时达物流
                            </option>
                            <option value="Superb Grace">Superb Grace
                            </option>
                            <option value="圣飞捷快递">圣飞捷快递
                            </option>
                            <option value="上海航瑞货运">上海航瑞货运
                            </option>
                            <option value="嗖一下同城快递">嗖一下同城快递
                            </option>
                            <option value="天天快递">天天快递
                            </option>
                            <option value="TNT快递">TNT快递
                            </option>
                            <option value="天地华宇">天地华宇
                            </option>
                            <option value="通和天下">通和天下
                            </option>
                            <option value="天纵物流">天纵物流
                            </option>
                            <option value="同舟行物流">同舟行物流
                            </option>
                            <option value="腾达速递">腾达速递
                            </option>
                            <option value="泰国138">泰国138
                            </option>
                            <option value="通达兴物流">通达兴物流
                            </option>
                            <option value="天联快运">天联快运
                            </option>
                            <option value="途鲜物流">途鲜物流
                            </option>
                            <option value="淘韩国际快递">淘韩国际快递
                            </option>
                            <option value="特急送">特急送
                            </option>
                            <option value="UPS快递查询">UPS快递查询
                            </option>
                            <option value="UC优速快递">UC优速快递
                            </option>
                            <option value="UCS（合众速递）">UCS（合众速递）
                            </option>
                            <option value="USPS美国邮政">USPS美国邮政
                            </option>
                            <option value="UEQ快递">UEQ快递
                            </option>
                            <option value="UEX国际物流">UEX国际物流
                            </option>
                            <option value="万象物流">万象物流
                            </option>
                            <option value="微特派">微特派
                            </option>
                            <option value="万家物流">万家物流
                            </option>
                            <option value="万博快递">万博快递
                            </option>
                            <option value="万家通">万家通
                            </option>
                            <option value="威时沛运">威时沛运
                            </option>
                            <option value="微转运">微转运
                            </option>
                            <option value="万通快递">万通快递
                            </option>
                            <option value="渥途国际速运">渥途国际速运
                            </option>
                            <option value="豌豆物流">豌豆物流
                            </option>
                            <option value="万家康物流">万家康物流
                            </option>
                            <option value="维普恩物流">维普恩物流
                            </option>
                            <option value="希优特快递">希优特快递
                            </option>
                            <option value="新邦物流">新邦物流
                            </option>
                            <option value="信丰物流">信丰物流
                            </option>
                            <option value="新蛋物流">新蛋物流
                            </option>
                            <option value="祥龙运通物流">祥龙运通物流
                            </option>
                            <option value="西安城联速递">西安城联速递
                            </option>
                            <option value="喜来快递">喜来快递
                            </option>
                            <option value="鑫世锐达">鑫世锐达
                            </option>
                            <option value="鑫通宝物流">鑫通宝物流
                            </option>
                            <option value="信天捷快递">信天捷快递
                            </option>
                            <option value="西安胜峰">西安胜峰
                            </option>
                            <option value="新顺丰（NSF）">新顺丰（NSF）
                            </option>
                            <option value="先锋快递">先锋快递
                            </option>
                            <option value="新速航">新速航
                            </option>
                            <option value="西邮寄">西邮寄
                            </option>
                            <option value="信联通">信联通
                            </option>
                            <option value="新杰物流">新杰物流
                            </option>
                            <option value="心怡物流">心怡物流
                            </option>
                            <option value="新时速物流">新时速物流
                            </option>
                            <option value="翔腾物流">翔腾物流
                            </option>
                            <option value="西翼物流">西翼物流
                            </option>
                            <option value="小熊物流">小熊物流
                            </option>
                            <option value="鑫和高铁快运">鑫和高铁快运
                            </option>
                            <option value="圆通速递">圆通速递
                            </option>
                            <option value="韵达快递">韵达快递
                            </option>
                            <option value="韵达美国件">韵达美国件
                            </option>
                            <option value="邮政国内">邮政国内
                            </option>
                            <option value="邮政国际">邮政国际
                            </option>
                            <option value="邮政EMS速递">邮政EMS速递
                            </option>
                            <option value="运通快递">运通快递
                            </option>
                            <option value="宇鑫物流">宇鑫物流
                            </option>
                            <option value="远成物流">远成物流
                            </option>
                            <option value="优速快递">优速快递
                            </option>
                            <option value="越丰物流">越丰物流
                            </option>
                            <option value="源安达快递">源安达快递
                            </option>
                            <option value="原飞航物流">原飞航物流
                            </option>
                            <option value="银捷速递">银捷速递
                            </option>
                            <option value="一统飞鸿">一统飞鸿
                            </option>
                            <option value="亚风速递">亚风速递
                            </option>
                            <option value="易通达">易通达
                            </option>
                            <option value="邮必佳">邮必佳
                            </option>
                            <option value="一柒物流">一柒物流
                            </option>
                            <option value="音素快运">音素快运
                            </option>
                            <option value="亿领速运">亿领速运
                            </option>
                            <option value="煜嘉物流">煜嘉物流
                            </option>
                            <option value="英脉物流">英脉物流
                            </option>
                            <option value="云豹国际货运">云豹国际货运
                            </option>
                            <option value="云南中诚">云南中诚
                            </option>
                            <option value="优配速运">优配速运
                            </option>
                            <option value="永昌物流">永昌物流
                            </option>
                            <option value="御风速运">御风速运
                            </option>
                            <option value="亚马逊物流">亚马逊物流
                            </option>
                            <option value="优速通达">优速通达
                            </option>
                            <option value="亿顺航">亿顺航
                            </option>
                            <option value="永旺达快递">永旺达快递
                            </option>
                            <option value="易客满">易客满
                            </option>
                            <option value="英超物流">英超物流
                            </option>
                            <option value="益递物流">益递物流
                            </option>
                            <option value="远洋国际">远洋国际
                            </option>
                            <option value="一号仓">一号仓
                            </option>
                            <option value="远成快运">远成快运
                            </option>
                            <option value="一号线">一号线
                            </option>
                            <option value="壹品速递">壹品速递
                            </option>
                            <option value="易达通快递">易达通快递
                            </option>
                            <option value="鹰运国际速递">鹰运国际速递
                            </option>
                            <option value="YLTD">YLTD
                            </option>
                            <option value="一运全成物流">一运全成物流
                            </option>
                            <option value="韵丰物流">韵丰物流
                            </option>
                            <option value="驿扬国际速运">驿扬国际速运
                            </option>
                            <option value="一站通快递">一站通快递
                            </option>
                            <option value="易优包裹">易优包裹
                            </option>
                            <option value="云达通">云达通
                            </option>
                            <option value="运东西">运东西
                            </option>
                            <option value="洋包裹">洋包裹
                            </option>
                            <option value="优联吉运">优联吉运
                            </option>
                            <option value="优邦速运">优邦速运
                            </option>
                            <option value="玥玛速运">玥玛速运
                            </option>
                            <option value="远为快递">远为快递
                            </option>
                            <option value="易转运">易转运
                            </option>
                            <option value="一起送">一起送
                            </option>
                            <option value="中通快递">中通快递
                            </option>
                            <option value="宅急送">宅急送
                            </option>
                            <option value="中铁快运">中铁快运
                            </option>
                            <option value="中铁物流">中铁物流
                            </option>
                            <option value="中邮物流">中邮物流
                            </option>
                            <option value="中国东方(COE)">中国东方(COE)
                            </option>
                            <option value="芝麻开门">芝麻开门
                            </option>
                            <option value="中国邮政快递">中国邮政快递
                            </option>
                            <option value="郑州建华">郑州建华
                            </option>
                            <option value="中速快件">中速快件
                            </option>
                            <option value="中天万运">中天万运
                            </option>
                            <option value="中睿速递">中睿速递
                            </option>
                            <option value="中外运速递">中外运速递
                            </option>
                            <option value="增益速递">增益速递
                            </option>
                            <option value="郑州速捷">郑州速捷
                            </option>
                            <option value="智通物流">智通物流
                            </option>
                            <option value="至诚通达快递">至诚通达快递
                            </option>
                            <option value="众辉达物流">众辉达物流
                            </option>
                            <option value="直邮易">直邮易
                            </option>
                            <option value="中运全速">中运全速
                            </option>
                            <option value="中欧快运">中欧快运
                            </option>
                            <option value="准实快运">准实快运
                            </option>
                            <option value="中国翼">中国翼
                            </option>
                            <option value="中宇天地">中宇天地
                            </option>
                            <option value="转运四方">转运四方
                            </option>
                            <option value="卓烨快递">卓烨快递
                            </option>
                        </select>
                        <style type="text/css">
                            .dropdown-menu
                            {
                                position:fixed;
                                left:auto;
                                top:auto;
                                margin-top:34px;
                            }
                            .bootstrap-select.btn-group .dropdown-menu
                            {
                                min-width:unset;
                            }
                        </style>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        快递单号：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_oryjdd" name="stwh_oryjdd" value="<%=UpdateModel.stwh_oryjdd %>" class="st-input-text-300 form-control" placeholder="请输入快递单号" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        备注：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_orremark" name="stwh_orremark" value="<%=UpdateModel.stwh_orremark %>" class="st-input-text-700 form-control" maxlength="100" placeholder="请输入备注信息" />
                        <span class="text-danger">字符最大长度100</span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i>修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $('.selectpicker').selectpicker();
            $('#stwh_oryjstyle').selectpicker('val', '<%=UpdateModel.stwh_oryjstyle%>');
            $("#addMenu").click(function () {
                var stwh_oryjstyle = $("#stwh_oryjstyle").val();
                if (!stwh_oryjstyle) {
                    $.bs.alert("请选择快递！", "info");
                    return false;
                }
                var stwh_oryjdd = $("#stwh_oryjdd").val();
                if (!stwh_oryjdd) {
                    $.bs.alert("请输入快递单号！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_shoporder/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
