﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_resources
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_resources", "资源管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_Resources> ListData = new stwh_ResourcesBLL().GetListByPage<stwh_Resources>("stwh_readdtime", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_Resources item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_reid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_reid + "\" value=\"" + item.stwh_reid + "\" /></td><td>" + item.stwh_reid + "</td><td>" + item.stwh_rename + "</td><td>" + item.stwh_retype + "</td><td>" + item.stwh_retime + "</td><td>" + item.stwh_resize + "</td><td>" + item.stwh_readdtime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td>" + item.stwh_reuname + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"8\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}