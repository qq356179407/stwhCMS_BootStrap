﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_commodityparam.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                商品分类：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_ptpid" name="stwh_ptpid" value="<%=UpdateModel.stwh_ptpid %>" />
                <input type="hidden" id="stwh_ptid" name="stwh_ptid" value="<%=UpdateModel.stwh_ptid %>" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowText" runat="server"></span> <span class="caret"></span>
                </button>
                <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                    <li><a data-pid="0">父级导航</a></li>
                    <li><a data-pid="0">系统设置</a></li>
                    <li><a data-pid="0">系统管理</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">内容管理</a></li>
                </ul>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                参数名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_ptptitle" name="stwh_ptptitle" value="<%=UpdateModel.stwh_ptptitle %>" maxlength="100" class="st-input-text-300 form-control"
                    required="required" />
                <span class="text-danger">*中文名称，100字符内，可由汉字、数字、大小写字母、中英文括号、下划线（_）、减号（-）组成。</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                表单字段名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_ptpname" name="stwh_ptpname" value="<%=UpdateModel.stwh_ptpname %>" maxlength="100" class="st-input-text-300 form-control"
                    required="required" />
                <span class="text-danger">*中文名称，100字符内，可由汉字、数字、大小写字母、中英文括号、下划线（_）、减号（-）组成。不能重复</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                表单字段类型：</div>
            <div class="col-sm-11 form-group">
                <select id="stwh_ptptype" name="stwh_ptptype" class="selectpicker" data-live-search="true"
                    data-live-search-style="begins" title="请选择表单字段类型">
                    <option selected="selected">text</option>
                    <option>textarea</option>
                    <option>select</option>
                    <option>number</option>
                    <option>datetime</option>
                    <option>editor</option>
                    <option>file</option>
                </select>
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_ptporder" name="stwh_ptporder" onkeydown="return checkNumber(event);"
                    value="<%=UpdateModel.stwh_ptporder %>" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span>
            </div>
            <div style="height:250px;"></div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('.selectpicker').selectpicker();
            $('#stwh_ptptype').selectpicker('val', '<%=UpdateModel.stwh_ptptype %>');
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                $("#stwh_ptid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_ptid = $("#stwh_ptid").val();
                if (stwh_ptid == "0") {
                    $.bs.alert("请选择分类！", "info");
                    return false;
                }
                var stwh_ptptitle = $("#stwh_ptptitle").val();
                if (!stwh_ptptitle) {
                    $.bs.alert("请输入参数名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 100, stwh_ptptitle)) {
                    $.bs.alert("参数名称格式错误！", "danger");
                    return false;
                }
                var stwh_ptpname = $("#stwh_ptpname").val();
                if (!stwh_ptpname) {
                    $.bs.alert("请输入表单字段名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 100, stwh_ptpname)) {
                    $.bs.alert("表单字段名称格式错误！", "danger");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_commodityparam/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>