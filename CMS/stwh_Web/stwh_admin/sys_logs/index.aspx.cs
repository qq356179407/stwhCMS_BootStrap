﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_logs
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_logs", "操作日志管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);

                int ss = 0, cc = 0;
                List<stwh_loginlog> ListData = new stwh_loginlogBLL().GetListByPage<stwh_loginlog>("stwh_loid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_loginlog item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_loid + "\"><td><td>" + item.stwh_loid + "</td></td><td>" + item.stwh_loname + "</td><td>" + item.stwh_lotime + "</td><td>" + item.stwh_loplace + "</td><td>" + item.stwh_loip + "</td><td>" + item.stwh_lobrowse + "</td><td>" + item.stwh_lodevice + "</td><td>" + item.stwh_loremark + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"9\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
            }
        }
    }
}