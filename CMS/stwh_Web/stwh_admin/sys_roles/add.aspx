﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_roles.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                角色名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_rname" name="stwh_rname" value="" maxlength="100" class="st-input-text-300 form-control"
                    required="required" />
                
                <span class="text-danger">*中文名称，100字符内，可由汉字、数字、大小写字母、中英文括号、下划线（_）、减号（-）组成。</span></div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                是否隐藏：</div>
            <div class="col-sm-11">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary btn-sm active">
                        <input type="radio" name="stwh_rstate" id="option1" value="0" checked="checked" />
                        启用
                    </label>
                    <label class="btn btn-primary btn-sm">
                        <input type="radio" name="stwh_rstate" id="option2" value="1" />
                        禁用
                    </label>
                </div>
                
                <span class="text-danger">*隐藏后该角色下的用户无法登录后台</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                备注说明：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_rdescription" name="stwh_rdescription" class="st-input-text-300 form-control"></textarea>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $("#addMenu").click(function () {
                var stwh_rname = $("#stwh_rname").val();
                if (!stwh_rname) {
                    $.bs.alert("请输入角色名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 100, stwh_rname)) {
                    $.bs.alert("角色名称格式错误，长度请控制在100字以内！", "danger");
                    return false;
                }
                var stwh_rdescription = $("#stwh_rdescription").val();
                if (stwh_rdescription) {
                    if (!IsHanZF(1, 50, stwh_rdescription)) {
                        $.bs.alert("备注说明格式错误，长度请控制在50字以内！", "danger");
                        return false;
                    }
                }
                $.post("/Handler/stwh_admin/sys_roles/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
