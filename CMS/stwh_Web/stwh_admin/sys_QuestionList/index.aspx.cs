﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_QuestionList
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_QuestionList", "试题选项管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_questionslist> ListData = new stwh_questionslistBLL().GetListByPage<stwh_questionslist>("stwh_qulid", "desc", "1= 1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_questionslist item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_qulid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_qulid + "\" value=\"" + item.stwh_qulid + "\" /></td><td>" + item.stwh_qulid + "</td><td>" + item.stwh_qutitle + "</td><td><div id=\"stpanel" + item.stwh_qulid + "\" style=\"max-width:250px;text-overflow:ellipsis; white-space:nowrap; overflow:hidden; float:left; height:20px;\">" + item.stwh_qulmiaoshu + "</div>&nbsp;&nbsp;&nbsp;&nbsp;[ <a class=\"stbtn\" data-id=\"" + item.stwh_qulid + "\">点击查看</a> ]</td><td>" + item.stwh_quladdtime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_qulorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"6\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);

                List<stwh_questions> AllListData = new stwh_questionsBLL().GetModelList("1 = 1 order by stwh_quorder desc,stwh_quaddtime desc");
                StringBuilder sb = new StringBuilder();
                sb.Append("<li><a data-pid=\"0\">请选择试题标题</a></li>");
                sb.Append("<li class=\"divider\"></li>");
                for (int i = 0; i < AllListData.Count; i++)
                {
                    sb.Append("<li><a data-pid=\"" + AllListData[i].stwh_quid + "\">" + AllListData[i].stwh_qutitle + "</a></li>");
                    if (i != AllListData.Count - 1) sb.Append("<li class=\"divider\"></li>");
                }
                this.selectShowList.InnerHtml = sb.ToString();
            }
        }
    }
}