﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_shopaddress
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_shopaddress", "会员地址管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                stwh_userinfo model = Session["htuser"] as stwh_userinfo;
                string ifstr = "1= 1";
                int ss = 0, cc = 0;
                List<stwh_buyaddress> ListData = new stwh_buyaddressBLL().GetListByPage<stwh_buyaddress>("stwh_baid", "desc", ifstr, 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_buyaddress item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_baid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_baid + "\" value=\"" + item.stwh_baid + "\" /></td><td>" + item.stwh_baid + "</td><td>" + item.stwh_bumobile + "</td><td>" + item.stwh_baname + "</td><td>" + item.stwh_bamobile + "</td><td>" + item.stwh_badiqu + "</td><td>" + item.stwh_baaddress + "</td><td>" + (item.stwh_bayoubian == 0 ? "000000" : item.stwh_bayoubian+"") + "</td><td>" + item.stwh_baaddtime.ToString("yyyy-MM-dd HH:mm:ss") + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"9\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}