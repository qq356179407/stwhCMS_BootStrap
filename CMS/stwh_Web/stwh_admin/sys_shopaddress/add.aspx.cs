﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_shopaddress
{
    public partial class add : Common.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetMenuSpanText(this.menuSpan, "sys_shopaddress", "会员地址管理");
        }
    }
}