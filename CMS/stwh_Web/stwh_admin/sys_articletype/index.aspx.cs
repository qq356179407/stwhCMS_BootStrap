﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_articletype
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_articletype", "文章栏目管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int cc = 0;
                List<stwh_artype> ListData = new stwh_artypeBLL().GetModelList("");
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = ListData.Count + "";
                StringBuilder sb = new StringBuilder();
                ArticleList(ListData, sb, 0, 0);

                if (sb == null || sb.Length == 0) this.ChildDatas.InnerHtml = "<tr><td colspan=\"5\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = sb.ToString();
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }

        /// <summary>
        /// 生成数据
        /// </summary>
        /// <param name="datasource">数据源</param>
        /// <param name="listSave">储存</param>
        /// <param name="pid">父id</param>
        /// <param name="dj">栏目等级</param>
        private void ArticleList(List<stwh_artype> datasource, StringBuilder listSave, int pid, int dj)
        {
            //筛选出一级栏目
            List<stwh_artype> listadd = datasource.Where(aa => aa.stwh_artparentid == pid).ToList<stwh_artype>();
            if (listadd.Count != 0)
            {
                //循环递归判断当前栏目下是否有子栏目
                foreach (stwh_artype item in listadd)
                {
                    string nullstring = "├";
                    for (int i = 0; i < dj; i++) nullstring += "──";
                    listSave.Append("<tr data-id=\"" + item.stwh_artid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_artid + "\" value=\"" + item.stwh_artid + "\" /></td><td>" + item.stwh_artid + "</td><td>" + nullstring + "&nbsp;" + item.stwh_artname + "</td><td>" + item.stwh_artdescription + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_artorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>");
                    ArticleList(datasource, listSave, item.stwh_artid, dj + 1);
                }
            }
            else dj = dj - 1;
        }
    }
}