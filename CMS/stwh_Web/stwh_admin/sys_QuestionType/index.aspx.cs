﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_QuestionType
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_QuestionType", "试题分类管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_qutype> ListData = new stwh_qutypeBLL().GetListByPage<stwh_qutype>("stwh_qutid", "desc", "1= 1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                StringBuilder sb = new StringBuilder();
                ArticleList(ListData, sb, 0, 0);

                if (sb == null || sb.Length == 0) this.ChildDatas.InnerHtml = "<tr><td colspan=\"5\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = sb.ToString();
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }

        /// <summary>
        /// 生成数据
        /// </summary>
        /// <param name="datasource">数据源</param>
        /// <param name="listSave">储存</param>
        /// <param name="pid">父id</param>
        /// <param name="dj">栏目等级</param>
        private void ArticleList(List<stwh_qutype> datasource, StringBuilder listSave, int pid, int dj)
        {
            //筛选出一级栏目
            List<stwh_qutype> listadd = datasource.Where(aa => aa.stwh_qutparentid == pid).ToList<stwh_qutype>();
            if (listadd.Count != 0)
            {
                //循环递归判断当前栏目下是否有子栏目
                foreach (stwh_qutype item in listadd)
                {
                    string nullstring = "├";
                    for (int i = 0; i < dj; i++) nullstring += "──";
                    listSave.Append("<tr data-id=\"" + item.stwh_qutid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_qutid + "\" value=\"" + item.stwh_qutid + "\" /></td><td>" + item.stwh_qutid + "</td><td>" + nullstring + "&nbsp;" + item.stwh_qutname + "</td><td>" + item.stwh_qutdescription + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_qutorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>");
                    ArticleList(datasource, listSave, item.stwh_qutid, dj + 1);
                }
            }
            else dj = dj - 1;
        }
    }
}