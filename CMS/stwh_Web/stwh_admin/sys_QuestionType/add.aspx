﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_QuestionType.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">试题分类管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                归属栏目：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_qutparentid" name="stwh_qutparentid" value="0" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowText">一级栏目</span> <span class="caret"></span>
                </button>
                <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height:300px; overflow:auto; left:auto; top:auto;">
                </ul>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_qutorder" name="stwh_qutorder" onkeydown="return checkNumber(event);" value="1" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                栏目名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_qutname" name="stwh_qutname" value=""  class="st-input-text-700 form-control" placeholder="请输入栏目名称" />
                
                <span class="text-danger">*栏目名称内容长度不能超过100个字符</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                栏目描述：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_qutdescription" name="stwh_qutdescription" class="st-input-text-700 form-control" placeholder="请输入栏目描述" ></textarea>
                
                <span class="text-danger">非必填，内容长度不能超过100字符</span>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                $("#stwh_qutparentid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_qutname = $("#stwh_qutname").val();
                if (!stwh_qutname) {
                    $.bs.alert("请输入栏目名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 100, stwh_qutname)) {
                    $.bs.alert("栏目名称长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_qutdescription = $("#stwh_qutdescription").val();
                if (stwh_qutdescription) {
                    if (!IsHanZF(1, 100, stwh_qutdescription)) {
                        $.bs.alert("描述长度超出服务器限制！", "info");
                        return false;
                    }
                }
                $.post("/Handler/stwh_admin/sys_QuestionType/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>