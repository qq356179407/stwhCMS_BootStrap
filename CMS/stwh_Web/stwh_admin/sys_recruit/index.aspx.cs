﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_recruit
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_recruit", "招聘管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_recruit> ListData = new stwh_recruitBLL().GetListByPage<stwh_recruit>("stwh_rtid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_recruit item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_rtid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_rtid + "\" value=\"" + item.stwh_rtid + "\" /></td><td>" + item.stwh_rtid + "</td><td>" + item.stwh_rtname + "</td><td>" + item.stwh_rtxueli + "</td><td>" + item.stwh_rtjobtime + " / 年</td><td>" + item.stwh_rtpeople + " 人</td><td>" + item.stwh_rtplace + "</td><td>" + item.stwh_rtaddtime.ToString("yyyy-MM-dd hh:mm:ss") + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_rtorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"9\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}