﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_recruit.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <ul id="setTab" class="nav nav-tabs" style="margin-bottom:1px;">
        <li class="active" style="margin-left: 15px;"><a href="#setpanel0" ><span class="fa fa-wrench">
        </span>
            基本信息</a> </li>
        <li><a href="#setpanel1" ><span class="fa fa-link"></span>
            公司信息</a> </li>
    </ul>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
    <div class="container-fluid">
        <div id="setTabContent" class="tab-content" style="padding-top: 15px;">
            <div class="tab-pane active" id="setpanel0">
                <div class="row">
                    <input type="hidden" name="stwh_rtid" value="<%=UpdateModel.stwh_rtid %>" />
                    <input type="hidden" name="stwh_rtaddtime" value="<%=UpdateModel.stwh_rtaddtime %>" />
                    <div class="col-sm-1 st-text-right">
                        序号：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_rtorder" name="stwh_rtorder" onkeydown="return checkNumber(event);"
                            value="<%=UpdateModel.stwh_rtorder %>" class="st-input-text-300 form-control" />
                        <span class="text-danger">*越大越考前</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        缩略图：</div>
                    <div class="col-sm-11 form-group">
                        <input id="stwh_rtimg" name="stwh_rtimg" type="text" value="<%=UpdateModel.stwh_rtimg %>" class="st-input-text-300 form-control"
                            placeholder="请上传图片" style="float: left;" />
                        <div style="float: left; width: 100px;">
                            <div id="upwebico">
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div id="progressFile" class="progress progress-striped active" style="display: none;">
                            <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                            </div>
                        </div>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        简短名称：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_rtnamejd" name="stwh_rtnamejd" value="<%=UpdateModel.stwh_rtnamejd %>"
                            class="st-input-text-700 form-control" placeholder="请输入职位简短标题" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        职位名称：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_rtname" name="stwh_rtname" value="<%=UpdateModel.stwh_rtname %>"
                            class="st-input-text-700 form-control" placeholder="请输入完整职位名称" />
                        <span class="text-danger">*名称内容长度不能超过100个字符</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        薪酬福利：</div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" id="hidstwh_rtfuli" name="stwh_rtfuli" value="<%=UpdateModel.stwh_rtfuli %>" />
                        <select id="stwh_rtfuli" class="selectpicker" multiple="multiple" data-live-search="true"
                            data-live-search-style="begins" data-actions-box="true" title="请选择薪酬福利">
                            <option>五险一金</option>
                            <option>包吃</option>
                            <option>包住</option>
                            <option>年底双薪</option>
                            <option>周末双休</option>
                            <option>交通补助</option>
                            <option>加班补助</option>
                            <option>饭补</option>
                            <option>话补</option>
                            <option>房补</option>
                        </select>
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        薪水范围：</div>
                    <div class="col-sm-11 form-group">
                        <select id="stwh_rtmoney" name="stwh_rtmoney" class="selectpicker" data-live-search="true"
                            data-live-search-style="begins" title="请选择薪水范围">
                            <option>不限</option>
                            <option>1000元以下</option>
                            <option>1000-2000元</option>
                            <option>2000-3000元</option>
                            <option>3000-5000元</option>
                            <option>5000-8000元</option>
                            <option>8000-12000元</option>
                            <option>12000-20000元</option>
                            <option>20000-25000元</option>
                            <option>25000元以上</option>
                        </select>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        文化程度：</div>
                    <div class="col-sm-11 form-group">
                        <select id="stwh_rtxueli" name="stwh_rtxueli" class="selectpicker" data-live-search="true"
                            data-live-search-style="begins" title="请选择学历">
                            <option>不限</option>
                            <option>高中</option>
                            <option>技校</option>
                            <option>中专</option>
                            <option>大专</option>
                            <option>本科</option>
                            <option>硕士</option>
                            <option>博士</option>
                        </select>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        工作年限：</div>
                    <div class="col-sm-11 form-group">
                        <select id="stwh_rtjobtime" name="stwh_rtjobtime" class="selectpicker" data-live-search="true"
                            data-live-search-style="begins" title="请选择工作年限">
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </select>
                        <span class="text-danger">*单位（年）</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        招聘人数：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_rtpeople" name="stwh_rtpeople" value="<%=UpdateModel.stwh_rtpeople %>"
                            class="st-input-text-700 form-control" placeholder="请输入招聘人数" onkeydown="return checkNumber(event);" />
                        <span class="text-danger">*单位（人）</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        工作地点：</div>
                    <div class="col-sm-11 form-group">
                        <select id="stwh_rtplace" name="stwh_rtplace" class="selectpicker" data-live-search="true"
                            data-live-search-style="begins" title="请选择工作地点">
                            <option>北京市</option>
                            <option>天津市</option>
                            <option>上海市</option>
                            <option>重庆市</option>
                            <option>河北省</option>
                            <option>山西省</option>
                            <option>内蒙古</option>
                            <option>辽宁省</option>
                            <option>吉林省</option>
                            <option>黑龙江省</option>
                            <option>江苏省</option>
                            <option>浙江省</option>
                            <option>安徽省</option>
                            <option>福建省</option>
                            <option>江西省</option>
                            <option>山东省</option>
                            <option>河南省</option>
                            <option>湖北省</option>
                            <option>湖南省</option>
                            <option>广东省</option>
                            <option>广西</option>
                            <option>海南省</option>
                            <option>四川省</option>
                            <option>贵州省</option>
                            <option>云南省</option>
                            <option>西藏</option>
                            <option>陕西省</option>
                            <option>甘肃省</option>
                            <option>青海省</option>
                            <option>宁夏</option>
                            <option>新疆</option>
                            <option>香港</option>
                            <option>澳门</option>
                            <option>台湾省</option>
                        </select>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        职位职责：</div>
                    <div class="col-sm-11 form-group">
                        <div id="stwh_rtzhizePanel" style=" display:none;"><%=UpdateModel.stwh_rtzhize%></div>
                        <textarea id="stwh_rtzhize" name="stwh_rtzhize" style="width: 700px; height: 300px;"></textarea>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        职位要求：</div>
                    <div class="col-sm-11 form-group">
                        <div id="stwh_rtyaoqiuPanel" style=" display:none;"><%=UpdateModel.stwh_rtyaoqiu%></div>
                        <textarea id="stwh_rtyaoqiu" name="stwh_rtyaoqiu" style="width: 700px; height: 300px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="setpanel1">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-sm-1 st-text-right">
                        公司名称：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-700 form-control" name="stwh_rtcname" value="<%=UpdateModel.stwh_rtcname %>"
                            id="stwh_rtcname" placeholder="请输入公司名称" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        公司行业：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-700 form-control" name="stwh_rtchangye" value="<%=UpdateModel.stwh_rtchangye %>"
                            id="stwh_rtchangye" placeholder="请输入公司行业" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        公司性质：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-700 form-control" name="stwh_rtcxingzhi"
                            value="<%=UpdateModel.stwh_rtcxingzhi %>" id="stwh_rtcxingzhi" placeholder="请输入公司性质" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        公司规模：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-700 form-control" name="stwh_rtcguimo" value="<%=UpdateModel.stwh_rtcguimo %>"
                            id="stwh_rtcguimo" placeholder="请输入公司规模" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $('.selectpicker').selectpicker();
            var fl = '<%=UpdateModel.stwh_rtfuli %>';
            var fls = fl.split(',');
            $('#stwh_rtfuli').selectpicker('val', fls);
            $("#stwh_rtfuli").change(function () {
                $("#hidstwh_rtfuli").val($(this).val());
            });
            $('#stwh_rtmoney').selectpicker('val', '<%=UpdateModel.stwh_rtmoney %>');
            $('#stwh_rtxueli').selectpicker('val', '<%=UpdateModel.stwh_rtxueli %>');
            $('#stwh_rtjobtime').selectpicker('val', '<%=UpdateModel.stwh_rtjobtime %>');
            $('#stwh_rtplace').selectpicker('val', '<%=UpdateModel.stwh_rtplace %>');

            $("#addMenu").click(function () {
                var stwh_rtnamejd = $("#stwh_rtnamejd").val();
                if (!IsHanZF(1, 100, stwh_rtnamejd)) {
                    $("#stwh_rtnamejd").focus();
                    $.bs.alert("职位简短名称格式错误，长度控制在100字以内！", "info");
                    return false;
                }
                var stwh_rtname = $("#stwh_rtname").val();
                if (!IsHanZF(1, 100, stwh_rtname)) {
                    $("#stwh_rtname").focus();
                    $.bs.alert("职位完整名称格式错误，长度控制在100字以内！", "info");
                    return false;
                }
                var stwh_rtfuli = $("#stwh_rtfuli").val();
                if (stwh_rtfuli) {
                    if (stwh_rtfuli.length > 100) {
                        $("#stwh_rtfuli").focus();
                        $.bs.alert("薪酬福利格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtmoney = $("#stwh_rtmoney").val();
                if (stwh_rtmoney) {
                    if (stwh_rtmoney.length > 100) {
                        $("#stwh_rtmoney").focus();
                        $.bs.alert("薪水范围格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtxueli = $("#stwh_rtxueli").val();
                if (stwh_rtxueli) {
                    if (stwh_rtxueli.length > 100) {
                        $("#stwh_rtxueli").focus();
                        $.bs.alert("学历格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtplace = $("#stwh_rtplace").val();
                if (stwh_rtplace) {
                    if (stwh_rtplace.length > 100) {
                        $("#stwh_rtplace").focus();
                        $.bs.alert("工作地点格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtcname = $("#stwh_rtcname").val();
                if (stwh_rtcname) {
                    if (stwh_rtcname.length > 100) {
                        $("#stwh_rtcname").focus();
                        $.bs.alert("公司名称格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtchangye = $("#stwh_rtchangye").val();
                if (stwh_rtchangye) {
                    if (stwh_rtchangye.length > 100) {
                        $("#stwh_rtchangye").focus();
                        $.bs.alert("公司行业格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtcxingzhi = $("#stwh_rtcxingzhi").val();
                if (stwh_rtcxingzhi) {
                    if (stwh_rtcxingzhi.length > 100) {
                        $("#stwh_rtcxingzhi").focus();
                        $.bs.alert("公司性质格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtcguimo = $("#stwh_rtcguimo").val();
                if (stwh_rtcguimo) {
                    if (stwh_rtcguimo.length > 100) {
                        $("#stwh_rtcguimo").focus();
                        $.bs.alert("公司规模格式错误，长度控制在100字以内！", "info");
                        return false;
                    }
                }
                var stwh_rtzhize = qjUEObject1.getContent();
                if (!stwh_rtzhize) {
                    qjUEObject1.focus();
                    $.bs.alert("请输入职位职责内容", "info");
                    return false;
                }
                if (qjUEObject1.getContentLength(true) > 5000) {
                    qjUEObject1.focus();
                    $.bs.alert("职位职责内容超出限制5000字！", "info");
                    return false;
                }
                var stwh_rtyaoqiu = qjUEObject2.getContent();
                if (!stwh_rtyaoqiu) {
                    qjUEObject2.focus();
                    $.bs.alert("请输入职位要求内容", "info");
                    return false;
                }
                if (qjUEObject2.getContentLength(true) > 5000) {
                    qjUEObject2.focus();
                    $.bs.alert("职位要求内容超出限制5000字！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_recruit/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        qjUEObject1.setContent("");
                        qjUEObject2.setContent("");
                        //清除本地数据
                        qjUEObject1.execCommand("clearlocaldata");
                        qjUEObject2.execCommand("clearlocaldata");
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
            $("#setTab li").click(function () {
                if ($(this).children("a").attr("href") != currentTabId) {
                    $(currentTabId).removeClass("active");
                    $("#setTab li").removeClass("active");
                    $(this).addClass("active");
                    currentTabId = $(this).children("a").attr("href");
                    $(currentTabId).addClass("active");
                    UpdateScroll();
                }
            });
        });
    </script>
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify']
                    ]//工具栏
        };
        var qjUEObject1 = UE.getEditor('stwh_rtzhize', qjOptions);
        var qjUEObject2 = UE.getEditor('stwh_rtyaoqiu', qjOptions);

        setTimeout(function () {
            qjUEObject1.setContent($("#stwh_rtzhizePanel").html(), false);
            qjUEObject2.setContent($("#stwh_rtyaoqiuPanel").html(), false);
            $("#stwh_rtnamejd").focus();
        }, 500);
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            $("#progressFile").show().children().first().css("width", info + "%");
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_rtimg").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
