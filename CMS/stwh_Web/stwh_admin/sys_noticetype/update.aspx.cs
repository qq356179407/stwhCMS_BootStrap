﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_noticetype
{
    public partial class update : Common.PageBase
    {
        public stwh_noticetype UpdateModel = new stwh_noticetype();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
				SetMenuSpanText(this.menuSpan, "sys_noticetype", "文章管理");
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid)) UpdateModel = new stwh_noticetypeBLL().GetModel(int.Parse(mid));
            }
            catch (Exception)
            {

            }
        }
    }
}