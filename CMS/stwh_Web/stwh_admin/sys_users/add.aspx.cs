﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_users
{
    public partial class add : Common.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
				SetMenuSpanText(this.menuSpan, "sys_users", "文章管理");
                List<stwh_roleinfo> AllListData = new stwh_roleinfoBLL().GetModelList("stwh_rdelstate = 0");
                StringBuilder sb = new StringBuilder();
                sb.Append("<li><a data-pid=\"0\"><span class=\"glyphicon glyphicon-th-list\"> 请选择角色</a></li>");
                sb.Append("<li class=\"divider\"></li>");
                for (int i = 0; i < AllListData.Count; i++)
                {
                    sb.Append("<li><a data-pid=\"" + AllListData[i].stwh_rid + "\">" + AllListData[i].stwh_rname + "</a></li>");
                    if (i != AllListData.Count - 1) sb.Append("<li class=\"divider\"></li>");
                }
                this.selectShowList.InnerHtml = sb.ToString();
            }
            catch (Exception)
            {

            }
        }
    }
}