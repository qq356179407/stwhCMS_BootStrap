﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_notices
{
    public partial class update : Common.PageBase
    {
        public stwh_notice UpdateModel = new stwh_notice();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid))
                {
					SetMenuSpanText(this.menuSpan, "sys_notices", "文章管理");
                    UpdateModel = new stwh_noticeBLL().GetModel(int.Parse(mid));
                    List<stwh_noticetype> AllListData = new stwh_noticetypeBLL().GetModelList("");
                    StringBuilder sb = new StringBuilder();
                    foreach (stwh_noticetype item in AllListData)
                    {
                        ListItem li = new ListItem(item.stwh_nottitle, item.stwh_notid.ToString());
                        if (item.stwh_notid == UpdateModel.stwh_notid) li.Selected = true;
                        this.stwh_notid.Items.Add(li);
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}