﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_notices
{
    public partial class add : Common.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
				SetMenuSpanText(this.menuSpan, "sys_notices", "文章管理");
                List<stwh_noticetype> AllListData = new stwh_noticetypeBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                foreach (stwh_noticetype item in AllListData)
                {
                    this.stwh_notid.Items.Add(new ListItem(item.stwh_nottitle, item.stwh_notid.ToString()));
                }
            }
            catch (Exception)
            {

            }
        }
    }
}