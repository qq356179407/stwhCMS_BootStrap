﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Text;

namespace stwh_Web.stwh_admin.sys_notices
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_notices", "公告内容管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);

                int ss = 0, cc = 0;
                List<stwh_notice> ListData = new stwh_noticeBLL().GetListByPage<stwh_notice>("stwh_noid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_notice item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_noid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_noid + "\" value=\"" + item.stwh_noid + "\" /></td><td>" + item.stwh_noid + "</td><td>" + item.stwh_nottitle + "</td><td>" + item.stwh_notitle + "</td><td>" + item.stwh_nojianjie + "</td><td>" + item.stwh_noaddtime.ToString("yyyy-MM-dd hh:mm:ss") + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_noorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"7\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);

                List<stwh_noticetype> AllListData = new stwh_noticetypeBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                this.stwh_notid.Items.Add(new ListItem("请选择公告类型", "0"));
                foreach (stwh_noticetype item in AllListData)
                {
                    this.stwh_notid.Items.Add(new ListItem(item.stwh_nottitle, item.stwh_notid.ToString()));
                }
            }
        }
    }
}