﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_Question
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0,strandomcount = 30;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_Question", "试题管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                stwh_userinfo model = Session["htuser"] as stwh_userinfo;
                int ss = 0, cc = 0;
                List<stwh_questions> ListData = new stwh_questionsBLL().GetListByPage<stwh_questions>("stwh_quid", "desc", "1= 1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_questions item in ListData)
                {
                    string type = "单选题";
                    switch (item.stwh_qutype)
                    {
                        case 1:
                            type = "单选题";
                            break;
                        case 2:
                            type = "多选题";
                            break;
                        case 3:
                            type = "判断题";
                            break;
                        default:
                            type = "填空题";
                            break;
                    }
                    strDatas += "<tr data-id=\"" + item.stwh_quid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_quid + "\" value=\"" + item.stwh_quid + "\" /></td><td>" + item.stwh_quid + "</td><td>" + item.stwh_qutname + "</td><td>" + item.stwh_qutitle + "</td><td><div id=\"stpanel" + item.stwh_quid + "\" style=\"max-width:250px;text-overflow:ellipsis; white-space:nowrap; overflow:hidden; float:left; height:20px;\">" + item.stwh_qumiaoshu + "</div>&nbsp;&nbsp;&nbsp;&nbsp;[ <a class=\"stbtn\" data-id=\"" + item.stwh_quid + "\">点击查看</a> ]</td><td>" + type + "</td><td>" + item.stwh_qufenshu + "</td><td>" + item.stwh_qudaan + "</td><td>" + item.stwh_quaddtime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_quorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"10\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);

                List<stwh_qutype> AllListData = new stwh_qutypeBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                sb.Append("<li><a data-pid=\"0\">请选择试题分类</a></li>");
                sb.Append("<li class=\"divider\"></li>");
                for (int i = 0; i < AllListData.Count; i++)
                {
                    sb.Append("<li><a data-pid=\"" + AllListData[i].stwh_qutid + "\">" + AllListData[i].stwh_qutname + "</a></li>");
                    if (i != AllListData.Count - 1) sb.Append("<li class=\"divider\"></li>");
                }
                this.selectShowList.InnerHtml = sb.ToString();

                try
                {
                    INIFile inifile = new INIFile(System.Web.HttpRuntime.AppDomainAppPath + "config/questioncog.ini");
                    string count = inifile.IniReadValue("section", "key");
                    if (!string.IsNullOrEmpty(count) && PageValidate.IsNumber(count)) strandomcount = int.Parse(count);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}