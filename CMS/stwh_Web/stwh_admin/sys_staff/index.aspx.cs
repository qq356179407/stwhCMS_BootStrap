﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_staff
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_staff", "员工管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_staff> ListData = new stwh_staffBLL().GetListByPage<stwh_staff>("stwh_sid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_staff item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_sid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_sid + "\" value=\"" + item.stwh_sid + "\" /></td><td>" + item.stwh_sid + "</td><td>" + item.stwh_snumber + "</td><td>" + item.stwh_sname + "</td><td>" + (item.stwh_ssex == 0 ? "男" : "女") + "</td><td>" + item.stwh_dtname + "</td><td>" + item.stwh_szw + "</td><td>" + item.stwh_stel + "</td><td>" + (item.stwh_siszz == 0 ? "<span class=\"text-danger\">[未转正]</span>  " : "<span class=\"text-success\">[已转正]</span> ") + (item.stwh_sisht == 0 ? "<span class=\"text-danger\">[未签订]</span> " : "<span class=\"text-success\">[已签订]</span> ") + (item.stwh_sislz == 0 ? "<span class=\"text-success\">[在职]</span> " : "<span class=\"text-danger\">[离职]</span>") + "</td><td><button class=\"btn btn-default btnxg\" data-id=\"" + item.stwh_sid + "\"><i class=\"fa fa-edit\"></i> 修改</button> | <button class=\"btn btn-default btnLook\" data-id=\"" + item.stwh_sid + "\"><i class=\"fa fa-eye\"></i> 查看</button></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"11\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);

                List<stwh_department> AllListData = new stwh_departmentBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                sb.Append("<li><a data-pid=\"0\">请选择部门</a></li>");
                sb.Append("<li class=\"divider\"></li>");
                for (int i = 0; i < AllListData.Count; i++)
                {
                    sb.Append("<li><a data-pid=\"" + AllListData[i].stwh_dtid + "\">" + AllListData[i].stwh_dtname + "</a></li>");
                    if (i != AllListData.Count - 1) sb.Append("<li class=\"divider\"></li>");
                }
                this.selectShowList.InnerHtml = sb.ToString();
            }
        }
    }
}