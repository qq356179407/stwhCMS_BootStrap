﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.IO;

namespace stwh_Web.stwh_admin.sys_staff
{
    public partial class excel : Common.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetMenuSpanText(this.menuSpan, "sys_staff", "员工管理");
                #region 获取Excel文件信息
                string templatename = "stwhUp";
                string path = Server.MapPath("/" + templatename + "/image/");
                DirectoryInfo dir = new DirectoryInfo(path);
                List<FileInfo> files = dir.GetFiles("*.xls").OrderByDescending<FileInfo,object>(a=>a.CreationTime).ToList<FileInfo>();
                string strDatas = "";
                int count = 0;
                foreach (FileInfo item in files)
                {
                    if (count > 10) break;
                    strDatas += "<tr><td><a href=\"/stwhup/image/" + item.Name + "\" target=\"_blank\">" + item.Name + "</a></td><td>" + item.CreationTime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td><button class=\"btn btn-default btnundo\" data-id=\"/stwhup/image/" + item.Name + "\"><i class=\"fa fa-undo\"></i>  恢复</button> | <button class=\"btn btn-default btndelete\" data-id=\"/stwhup/image/" + item.Name + "\"><i class=\"fa fa-trash\"></i> 删除</button></td></tr>";
                    count++;
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"3\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                #endregion
            }
            catch (Exception)
            {
                
            }
        }
    }
}