﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_bidding
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_bidding", "招标信息");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_bidding> ListData = new stwh_biddingBLL().GetListByPage<stwh_bidding>("stwh_bdid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_bidding item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_bdid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_bdid + "\" value=\"" + item.stwh_bdid + "\" /></td><td>" + item.stwh_bdid + "</td><td>" + item.stwh_bdtitle + "</td><td>" + item.stwh_bdcompany + "</td><td><a href=\"" + item.stwh_bdpath + "\" title=\"点击下载附件\"><i class=\"fa fa-file-pdf-o\" style=\"font-size:16px;\"></i></td><td>" + item.stwh_bdaddtime.ToString("yyyy-MM-dd hh:mm:ss") + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_bdorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"7\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}