﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_filetype.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">文件分类管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
            <asp:Literal ID="litBtnList" runat="server"></asp:Literal>
        </form>
    </div>
    <div id="scrollPanel">
    <form id="form1" runat="server" class="form-inline">
    <div class="container-fluid">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50px;">
                        <input type="checkbox" id="allParent" />
                    </th>
                    <th style=" width:50px;">
                        编号
                    </th>
                    <th style=" width:300px;">
                        栏目名称
                    </th>
                    <th>
                        栏目描述
                    </th>
                    <th style=" width:80px;">
                        序号
                    </th>
                </tr>
            </thead>
            <tbody id="ChildDatas" runat="server">
            </tbody>
        </table>
    </div>
    <asp:HiddenField ID="hidListId" runat="server" />
    <asp:HiddenField ID="hidAllData" runat="server" />
    <asp:HiddenField ID="hidTotalSum" runat="server" />
    </form>
    </div>
    <div class="container-fluid st-page" style="display:none;">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" data-width="55px"
                data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <span id="SumCountSpan"><asp:Literal ID="littotalSum" runat="server"></asp:Literal></span> 条数据
        </div>
        </form>
    </div>
    <div class="modal fade" id="deleteMessageModal" tabindex="-1" role="dialog" aria-labelledby="h2"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="h2">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要删除吗（删除后不可恢复，并且会删除当前栏目下的所有数据）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllData = JSON.parse($("#hidAllData").val());
        var listid = [];
        var pageCount = 15;//每页显示多少条数据
        var ifelse = "";
        //加载数据
        function LoadData(pCount,pIndex,ifstr)
        {
            $.post("/handler/stwh_admin/sys_filetype/loaddata.ashx",{
                pageCount:pCount,
                pageIndex:pIndex,
                whereStr:ifstr
            },function(data){
                if (data.msgcode==-1) {
                    $.bs.alert(data.msg, "warning", "");
                }
                else
                {
                    $("#hidListId").val("");
                    AllData = data.msg;
                    $("#ChildDatas tr").remove();
                    $("#SumCountSpan").text(data.sumcount);
                    if (data.msg.length==0) {
                        $("#ChildDatas").append("<tr><td colspan=\"5\" align=\"center\">暂无数据</td></tr>");
                    }
                    else
                    {
                        $.each(data.msg, function (index, item) {
                            $("#ChildDatas").append("<tr data-id=\"" + item.stwh_ftid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_ftid + "\" value=\"" + item.stwh_ftid + "\" /></td><td>" + item.stwh_ftid + "</td><td>" + item.stwh_ftname + "</td><td>" + item.stwh_ftdescription + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_ftorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>");
                        });
                        UpdateScroll();
                    }
                }
            },"json");
        }
        //重新设置分页
        function SetPaginator(pageCount,totalsum)
        {
            if (totalsum==0) totalsum = 1;
            $("#pageUl").bootstrapPaginator("setOptions",{
                    currentPage: 1, //当前页面
                    totalPages: totalsum / pageCount + ((totalsum % pageCount) == 0 ? 0 : 1), //总页数
                    numberOfPages: 5, //页码数
                    useBootstrapTooltip: true,
                    onPageChanged: function (event, oldPage, newPage) {
                        if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                    }
                });
        }
        $(function () {
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: <%=totalPages %>, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                }
            });
            $("#allParent").click(function () {
                if ($(this).is(":checked")) {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:checked").each(function (i, item) {
                        if (listid.IndexOfArray($(item).val()) == -1) listid.AddArray($(item).val());
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:not(:checked)").each(function (i, item) {
                        listid.removeIndexArray(listid.IndexOfArray($(item).val()));
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#ChildDatas").on("keyup","input[type='text']",function(){
                var jb_order = $(this).val();
                var jb_mid = $(this).parent().parent().attr("data-id");

                $.each(AllData, function (index, item) {
                    if (item.stwh_ftid == parseInt(jb_mid)) {
                        item.stwh_ftorder = parseInt(jb_order);
                        return false;
                    }
                });
            }).on("change","input[type='text']",function(){
                var jb_order = $(this).val();
                var jb_mid = $(this).parent().parent().attr("data-id");

                $.each(AllData, function (index, item) {
                    if (item.stwh_ftid == parseInt(jb_mid)) {
                        item.stwh_ftorder = parseInt(jb_order);
                        return false;
                    }
                });
            });
            $("#btnSave").click(function () {
                $.post(
                "/handler/stwh_admin/sys_filetype/save.ashx",
                { data: JSON.stringify(AllData) },
                function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#ChildDatas").on("click",":checkbox",function(){
              if ($(this).is(":checked")) {
                    if ($("#ChildDatas :checkbox:checked").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", true);
                    if (listid.IndexOfArray($(this).val()) == -1) listid.AddArray($(this).val());
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    if ($("#ChildDatas :checkbox:not(:checked)").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", false);
                    listid.removeIndexArray(listid.IndexOfArray($(this).val()));
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#btnDelete").click(function () {
                $("#deleteMessageModal").modal('show');
            });
            $("#btnDeleteOk").click(function () {
                var ids = $("#hidListId").val();
                if (!ids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(ids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_filetype/delete.ashx",
                {
                    ids: ids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnUpdate").click(function () {
                if ($("#ChildDatas :checkbox:checked").length == 0) {
                    $.bs.alert("请选择数据！", "warning", "");
                    return false;
                }
                $("#ChildDatas :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "update.aspx?id=" + $(item).val();
                });
                return false;
            });

            $("#basic").change(function(){
                pageCount = $(this).val();
                LoadData(pageCount,0,ifelse);
                var totalsum = parseInt($("#hidTotalSum").val());
                SetPaginator(pageCount,totalsum);
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>