﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.HttpProc;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.Collections;
using System.Data;

namespace stwh_Web.Handler.stwh_admin.sys_cjuser
{
    /// <summary>
    /// download 的摘要说明
    /// </summary>
    public class download : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string whereStr = context.Request["whereStr"];
                    #region 分析查询条件
                    if (string.IsNullOrEmpty(whereStr)) whereStr = "1 = 1 and ";
                    else
                    {
                        List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(whereStr);
                        if (dataList.Count != 0)
                        {
                            whereStr = "1 = 1 and ";
                            foreach (stwh_FormModel item in dataList)
                            {
                                if (item.Name.ToLower().Trim() == "stwh_cjname")
                                {
                                    if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_cjname like '%" + item.Value + "%' and ";
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_cjaddtime")
                                {
                                    if (!string.IsNullOrEmpty(item.Value)) whereStr += "CONVERT(varchar(300),stwh_cjaddtime,120) like '" + item.Value + "%' and ";
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_cjmobile")
                                {
                                    if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_cjmobile like '%" + item.Value + "%' and ";
                                }
                            }
                        }
                    }
                    whereStr += "1 = 1";
                    #endregion
                    string savepath = "/stwhup/" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls";

                    //生成列的中文对应表
                    Dictionary<string, string> nameList = new Dictionary<string, string>();
                    nameList.Add("stwh_cjid", "编号{split}10");
                    nameList.Add("stwh_cjname", "姓名{split}15");
                    nameList.Add("stwh_cjmobile", "手机号码{split}15");
                    nameList.Add("stwh_cjaddtime", "添加时间{split}30");

                    DataTable dt = new stwh_cjtelBLL().GetList(whereStr).Tables[0];
                    AsposeHelper.OutFileToDisk(dt, "已成功导出" + dt.Rows.Count + "条数据", "抽奖人员数据", WebClient.GetRootPath() + savepath, nameList);

                    stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 导出抽奖注册人员数据到excel成功！");
                    BaseHandler.SendResponseMsgs(context, "0", savepath, 0);
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}