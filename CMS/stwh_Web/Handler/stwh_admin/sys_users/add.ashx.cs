﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.DEncrypt;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_users
{
    /// <summary>
    /// add 的摘要说明
    /// </summary>
    public class add : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string data = context.Request["data"];
                    if (string.IsNullOrEmpty(data))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请上传数据！", 0);
                        return;
                    }
                    List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(data);
                    if (dataList.Count == 0) BaseHandler.SendResponseMsgs(context, "-1", "没有任何数据！", 0);
                    else
                    {
                        stwh_userinfo model = FormModel.ToModel<stwh_userinfo>(dataList);
                        if (!PageValidate.IsPwdComplex(model.stwh_uipwd))
                        {
                            BaseHandler.SendResponseMsgs(context, "-1", "密码格式错误，必须包含字母、数字、特称字符(+=_!@#&)，至少8个字符，最多12个字符！", 0);
                            return;
                        }
                        model.stwh_uictime = DateTime.Now;
                        model.stwh_uipwd = DESEncrypt.Encrypt(model.stwh_uipwd);
                        if (string.IsNullOrEmpty(model.stwh_uiportrait)) model.stwh_uiportrait = "/stwh_admin/css/default/logo.png";
                        if (new stwh_userinfoBLL().Add(model) > 0)
                        {
                            stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 添加用户成功！");
                            BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                        }
                        else
                        {
                            stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 添加用户失败！");
                            BaseHandler.SendResponseMsgs(context, "-1", "操作失败，请仔细检查数据！", 0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); 
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}