﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Web.stwh_admin.Common;
using stwh_Common.DEncrypt;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin
{
    /// <summary>
    /// updatePwd 的摘要说明
    /// </summary>
    public class updatePwd : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string newpwd = context.Request["NewPwd"];
                    string newpwdok = context.Request["NewPwdOk"]; ;

                    if (!PageValidate.IsPwdComplex(newpwd))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "新密码格式错误，必须包含字母、数字、特称字符(+=_!@#&)，至少8个字符，最多12个字符！", 0);
                        return;
                    }
                    if (newpwd != newpwdok)
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "两次密码输入不一致", 0);
                        return;
                    }
                    loginModel.stwh_uipwd = DESEncrypt.Encrypt(newpwd);

                    if (new stwh_userinfoBLL().Update(loginModel))
                    {
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 修改自己的登陆密码成功！");
                        BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                        
                    }
                    else
                    {
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 修改自己的登陆密码失败！");
                        BaseHandler.SendResponseMsgs(context, "-1", "修改密码失败！", 0);
                    }
                }
                else
                { 
                    BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
                    
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); 
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}