﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_shopaddress
{
    /// <summary>
    /// add 的摘要说明
    /// </summary>
    public class add : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string data = context.Request["data"];
                    if (string.IsNullOrEmpty(data))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请上传数据！", 0);
                        return;
                    }
                    List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(data);
                    if (dataList.Count == 0) BaseHandler.SendResponseMsgs(context, "-1", "没有任何数据！", 0);
                    else
                    {
                        string ss = "";
                        int citycode = 0;
                        foreach (stwh_FormModel item in dataList)
                        {
                            if (item.Name.ToLower() == "city")
                            {
                                ss += item.Value + " ";
                                #region 城市代码转换
                                switch (item.Value.Trim())
                                {
                                    case "北京":
                                        citycode = 0;
                                        break;
                                    case "天津":
                                        citycode = 1;
                                        break;
                                    case "上海":
                                        citycode = 2;
                                        break;
                                    case "重庆":
                                        citycode = 3;
                                        break;
                                    case "广西":
                                        citycode = 4;
                                        break;
                                    case "内蒙古":
                                        citycode = 5;
                                        break;
                                    case "西藏":
                                        citycode = 6;
                                        break;
                                    case "宁夏":
                                        citycode = 7;
                                        break;
                                    case "新疆":
                                        citycode = 8;
                                        break;
                                    case "香港":
                                        citycode = 9;
                                        break;
                                    case "澳门":
                                        citycode = 10;
                                        break;
                                    case "辽宁":
                                        citycode = 11;
                                        break;
                                    case "吉林":
                                        citycode = 12;
                                        break;
                                    case "黑龙江":
                                        citycode = 13;
                                        break;
                                    case "河北":
                                        citycode = 14;
                                        break;
                                    case "山西":
                                        citycode = 15;
                                        break;
                                    case "江苏":
                                        citycode = 16;
                                        break;
                                    case "浙江":
                                        citycode = 17;
                                        break;
                                    case "安徽":
                                        citycode = 18;
                                        break;
                                    case "福建":
                                        citycode = 19;
                                        break;
                                    case "江西":
                                        citycode = 20;
                                        break;
                                    case "山东":
                                        citycode = 21;
                                        break;
                                    case "河南":
                                        citycode =22;
                                        break;
                                    case "湖北":
                                        citycode = 23;
                                        break;
                                    case "湖南":
                                        citycode = 24;
                                        break;
                                    case "广东":
                                        citycode = 25;
                                        break;
                                    case "海南":
                                        citycode = 26;
                                        break;
                                    case "四川":
                                        citycode = 27;
                                        break;
                                    case "贵州":
                                        citycode = 28;
                                        break;
                                    case "云南":
                                        citycode = 29;
                                        break;
                                    case "陕西":
                                        citycode = 30;
                                        break;
                                    case "甘肃":
                                        citycode = 31;
                                        break;
                                    case "青海":
                                        citycode = 32;
                                        break;
                                    default: //台湾
                                        citycode = 33;
                                        break;
                                }
                                #endregion
                            }
                            if (item.Name.ToLower() == "xiang") ss += item.Value + " ";
                            if (item.Name.ToLower() == "cun") ss += item.Value + " ";
                        }
                        stwh_buyaddress model = FormModel.ToModel<stwh_buyaddress>(dataList);
                        model.stwh_baaddtime = DateTime.Now;
                        model.stwh_baismr = 0;
                        model.stwh_badiqu = ss;
                        model.stwh_bacity = citycode;
                        if (new stwh_buyaddressBLL().Add(model) > 0)
                        {
                            stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 添加会员地址成功！");
                            BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                        }
                        else
                        {
                            stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 添加会员地址失败！");
                            BaseHandler.SendResponseMsgs(context, "-1", "操作失败，请仔细检查数据！", 0);
                        }
                    }
                }
                else
                {
                    BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}