﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Text;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_commodity
{
    /// <summary>
    /// parameter 的摘要说明
    /// </summary>
    public class parameter : IHttpHandler, IRequiresSessionState
    {
        private string files = "";
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string ptid = context.Request["ptid"];
                    if (string.IsNullOrEmpty(ptid) || !PageValidate.IsCustom(@"[\d,]*", ptid))
                    {
                        context.Response.Write("");
                        context.Response.Flush();
                        return;
                    }
                    List<stwh_producttype_p> typeparam = new stwh_producttype_pBLL().GetModelList("stwh_ptid = "+ptid, 1);
                    StringBuilder sb = new StringBuilder();
                    foreach (stwh_producttype_p model in typeparam)
                    {
                        #region 生成表单字段
                        switch (model.stwh_ptptype.Trim().ToLower())
                        {
                            case "text":
                                sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input type=\"text\" id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" value=\"\" class=\"st-input-text-700 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" /></div><div class=\"line10\"></div>");
                                break;
                            case "textarea":
                                sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><textarea id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" class=\"st-input-text-700 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" ></textarea></div><div class=\"line10\"></div>");
                                break;
                            case "select":
                                break;
                            case "number":
                                sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input type=\"text\" id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" onkeydown=\"return checkNumber(event);\" value=\"\" class=\"st-input-text-700 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" /></div><div class=\"line10\"></div>");
                                break;
                            case "datetime":
                                sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input type=\"text\" id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" class=\"st-input-text-300 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" /></div><div class=\"line10\"></div><script>$(function(){$('#" + model.stwh_ptpname + "').datepicker({format: 'yyyy-mm-dd'}).on(\"changeDate\", function (ev) {var dqdate = new Date();$(this).val($(this).val() + \" \" + dqdate.getHours() + \":\" + dqdate.getMinutes() + \":\" + dqdate.getSeconds());}).val(new Date().Format(\"yyyy-MM-dd hh:mm:ss\"));});</script>");
                                break;
                            case "editor":
                                sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><textarea id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" style=\"width: 700px; height: 300px;\"></textarea></div><div class=\"line10\"></div><script>var qjUEObject_" + model.stwh_ptpname + " = UE.getEditor('" + model.stwh_ptpname + "', qjOptions);</script>");
                                break;
                            case "file":
                                sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" type=\"text\" class=\"st-input-text-700 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" style=\"float: left;\" /><div style=\"float: left;width: 100px;\"><div id=\"up" + model.stwh_ptpname + "\"></div></div><div class=\"clearfix\"></div></div><div class=\"line10\"></div><script>swfobject.embedSWF(\"/Plugin/upload.swf\", \"up" + model.stwh_ptpname + "\", \"95\", \"34\", \"11.0.0\", \"/Plugin/expressInstall.swf\", $.extend({}, flashvarsFile, { btnid: \"up" + model.stwh_ptpname + "\", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);</script>");
                                if (string.IsNullOrEmpty(files)) files = "else if(btnid==\"up" + model.stwh_ptpname + "\"){$(\"#" + model.stwh_ptpname + "\").val(msg);}";
                                else files += "else if(btnid==\"up" + model.stwh_ptpname + "\"){$(\"#" + model.stwh_ptpname + "\").val(msg);}";
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
                    sb.Append("<script>function chkFileBtnId(btnid,msg){if(btnid==\"upwebico\"){$(\"#stwh_pimage\").val(msg)}else if(btnid==\"upwebico1\"){if($(\"#stwh_pimagelist\").val()){$(\"#stwh_pimagelist\").val($(\"#stwh_pimagelist\").val()+\",\"+msg)}else{$(\"#stwh_pimagelist\").val(msg)}$(\"#imglistPanel\").append('<img src=\"'+msg+'\" class=\"img-thumbnail\" alt=\"\" style=\"width:110px; height:110px; margin:0px 10px 10px 0px;\" />')}"+files+"}</script>");
                    context.Response.Write(sb.ToString());
                    context.Response.Flush();
                }
                else
                {
                    context.Response.Write("");
                    context.Response.Flush();
                }
            }
            catch (Exception)
            {
                context.Response.Write("");
                context.Response.Flush();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}