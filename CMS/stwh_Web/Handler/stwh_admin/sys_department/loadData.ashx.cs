﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_department
{
    /// <summary>
    /// loadData 的摘要说明
    /// </summary>
    public class loadData : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string pageCount = context.Request["pageCount"];
                    string pageIndex = context.Request["pageIndex"];
                    string whereStr = context.Request["whereStr"];
                    if (PageValidate.IsNumber(pageCount) && PageValidate.IsNumber(pageIndex))
                    {
                        #region 分析查询条件
                        if (string.IsNullOrEmpty(whereStr)) whereStr = "1 = 1";
                        else
                        {
                            List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(whereStr);
                            if (dataList.Count != 0)
                            {
                                whereStr = "1 = 1 and ";
                                foreach (stwh_FormModel item in dataList)
                                {
                                    if (item.Name.ToLower().Trim() == "stwh_notitle")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_notitle like '%" + item.Value + "%' and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_noaddtime")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value)) whereStr += "CONVERT(varchar(300),stwh_noaddtime,120) like '" + item.Value + "%' and ";
                                    }
                                }
                                whereStr += "1 = 1";
                            }
                            else whereStr = "1 = 1";
                        }
                        #endregion
                        int ss = 0, cc = 0;
                        List<stwh_department> ListData = new stwh_departmentBLL().GetListByPage<stwh_department>("stwh_dtid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                        BaseHandler.SendResponseMsgs(context, "0", ListData, cc);
                    }
                    else BaseHandler.SendResponseMsgs(context, "-1", "数据格式错误！", 0);
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}