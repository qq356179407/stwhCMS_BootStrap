﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_resources
{
    /// <summary>
    /// delete 的摘要说明
    /// </summary>
    public class delete : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string mids = context.Request["ids"];
                    if (string.IsNullOrEmpty(mids))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请选择数据！", 0);
                        
                        return;
                    }
                    if (!PageValidate.IsCustom(@"[\d,]*", mids))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "参数格式错误，请刷新页面重试！", 0);
                        
                        return;
                    }
                    if (new stwh_ResourcesBLL().DeleteList(mids))
                    {
                        List<stwh_Resources> listdata = new stwh_ResourcesBLL().GetModelList(string.Format("stwh_reid in ({0})", mids));
                        if (listdata.Count != 0)
                        {
                            string pp = "";
                            foreach (stwh_Resources item in listdata)
                            {
                                pp = context.Server.MapPath(item.stwh_repath);
                                if (System.IO.File.Exists(pp)) System.IO.File.Delete(pp);
                            }
                        }
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 删除资源文件成功！");
                        BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                        
                    }
                    else
                    {
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 删除资源文件失败！");
                        BaseHandler.SendResponseMsgs(context, "-1", "删除失败！", 0);
                        
                    }
                }
                else
                {
                    BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
                    
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
                
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}