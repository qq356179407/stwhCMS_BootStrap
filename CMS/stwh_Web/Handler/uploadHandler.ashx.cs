﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
//导入命名空间
using stwh_Model;
using stwh_Web.stwh_admin.Common;

namespace stwh_Web.Handler
{
    /// <summary>
    /// uploadHandler 的摘要说明
    /// </summary>
    public class uploadHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                //判断是否登录
                object userobj = context.Session["htuser"];
                if (userobj == null) context.Response.Write("login");
                else
                {
                    //获取网站配置信息
                    stwh_website webmodel = WebSite.LoadWebSite();
                    //允许上传文件类型
                    string strfiletype = webmodel.Webfiletype;
                    if (context.Request.Files.Count == 0) context.Response.Write("NoFile");
                    string pathlist = "";
                    string filetype = context.Request["FileType"];

                    for (int i = 0; i < context.Request.Files.Count; i++)
                    {
                        HttpPostedFile file = context.Request.Files[i];
                        //获取上传文件后缀名
                        string filetypes = file.FileName.Split('.')[file.FileName.Split('.').Length - 1];

                        #region 判断文件是否是允许上传类型（通过文件后缀名进行判断，不严谨）
                        string[] ss = strfiletype.Split(',');
                        int strIndex = -1;
                        for (int j = 0; j < ss.Length; j++)
                        {
                            strIndex = ss[j].ToLower().IndexOf(filetypes.ToLower());
                            if (strIndex >= 0) break;
                        }
                        if (strIndex == -1)
                        {
                            context.Response.Write("NoType");
                            return;
                        }
                        #endregion
                        #region 判断文件大小是否超出限制
                        int upSize = file.ContentLength/1024;//获取上传文件大小
                        int sumSize = webmodel.Webfilesize;
                        if (upSize>sumSize)
                        {
                            context.Response.Write("NoSize");
                            return;
                        }
                        #endregion
                        string filename = DateTime.Now.ToString("yyyyMMddHHmmss") + Guid.NewGuid().ToString() + "." + filetypes;
                        //获取上传目录
                        string dire = webmodel.WebUploadDire + filetype + "/";
                        //获取物理路径
                        string dirpath = context.Server.MapPath(dire);
                        //判断指定目录是否存在，不存在创建
                        if (!System.IO.Directory.Exists(dirpath)) System.IO.Directory.CreateDirectory(dirpath);
                        pathlist = dire + filename;
                        file.SaveAs(context.Server.MapPath(pathlist));
                        stwh_admin.BaseHandler.AddResource(context, pathlist);
                    }
                    context.Response.Write(pathlist);
                }
            }
            catch (Exception)
            {
                context.Response.Write("Exception");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}