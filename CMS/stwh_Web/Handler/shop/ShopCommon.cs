﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using stwh_Model;

namespace stwh_Web.Handler.wechat.shop
{
    /// <summary>
    /// 商城Handler的工具类
    /// </summary>
    public class ShopCommon
    {
        /// <summary>
        /// 移除商城所有session信息
        /// </summary>
        /// <param name="context"></param>
        public static void ClearAllSession(HttpContext context)
        {
            context.Session.Clear();
        }

        /// <summary>
        /// 检查商城会员是否登录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="model">已登录的会员对象</param>
        /// <returns></returns>
        public static bool ChkShopLoginUser(HttpContext context, ref stwh_buyuser model)
        {
            bool result = false;
            try
            {
                object dlobj = context.Session["shopqtmodel"];
                if (dlobj != null)
                {
                    model = dlobj as stwh_buyuser;
                    result = true;
                }
            }
            catch (Exception) { }
            return result;
        }
    }
}