﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//引入命名空间
using System.Configuration;
using stwh_Common.DEncrypt;

namespace stwh_DBUtility
{
    public class PubConstant
    {
        /// <summary>
        /// 获取连接字符串
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                string _connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                string ConStringEncrypt = ConfigurationManager.AppSettings["ConStringEncrypt"];
                if (ConStringEncrypt == "true") _connectionString = DESEncrypt.Decrypt(_connectionString);
                return _connectionString;
            }
        }

        /// <summary>
        /// 得到web.config里AppSettings配置项的数据库连接字符串。
        /// </summary>
        /// <param name="configName"></param>
        /// <returns></returns>
        public static string GetConnectionString(string configName)
        {
            string connectionString = ConfigurationManager.AppSettings[configName];
            string ConStringEncrypt = ConfigurationManager.AppSettings["ConStringEncrypt"];
            if (ConStringEncrypt == "true") connectionString = DESEncrypt.Decrypt(connectionString);
            return connectionString;
        }

        /// <summary>
        /// 得到web.config里配置项的数据库连接字符串。
        /// </summary>
        /// <param name="configName"></param>
        /// <param name="isconfig">是否获取connectionStrings节点值，true获取，false获取appsettings节点值</param>
        /// <returns></returns>
        public static string GetConnectionString(string configName, bool isconfig)
        {
            if (isconfig) return ConfigurationManager.ConnectionStrings[configName].ConnectionString;
            return GetConnectionString(configName);
        }
    }
}
