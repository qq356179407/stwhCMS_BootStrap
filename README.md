##  **提示：** 
##  **由于工作、生活等多方面原因，stwhCMS_BootStrap不再进行维护，谢谢大家一直的支持！
** 
# stwhCMS_BootStrap

***

[stwhCMS_BootStrap](http://118.178.187.187/index.html)是基于asp.net开发的内容管理系统，数据库采用关系型数据库（MS Sql Server），使用了UEditor编辑器作为用户编辑内容的控件、自己开发的flash upload上传插件作为后台上传控件、BootStrap3.0作为系统后台框架。
以VTemplate模板引擎作为和前端工程师沟通的桥梁， **让程序猿和前端把更多的精力放在各自的领域。** 


## VTemplate模板引擎简介

VTemplate模板引擎也简称为VT，是基于.NET的模板引擎，它允许任何人使用简单的类似HTML语法的模板语言来引用.NET里定义的对象。当VTemplate应用于web开发时，界面设计人员可以和程序开发人员同步开发一个遵循MVC架构的web站点，也就是说，页面设计人员可以只关注页面的显示效果，而由程序开发人员关注业务逻辑编码。  
VTemplate将.NET程序代码从web页面中分离出来，这样为web站点的长期维护提供了便利，同时也为我们在ASP.NET WebForm开发之外又提供了一种可选的方案。   
VTemplate也可以作为动态文本生成工具，生成HTML、XML、邮件、程序源代码或其它文本等。

## 开发环境说明
- 系统：windows 7 sp1(32位)
- 核心库：.net 4.0
- 开发工具：Visual Studio Ultimate 2012(版本 11.0.61219.00 Update 5)
- 数据库：Microsoft SQL Server(MS SQL) 2008 R2
- [视频教程](http://pan.baidu.com/s/1i5afPo5) 密码：oyaw（持续更新中...,由于工作原因更新不及时，还请谅解！！）


## 更新记录

### 持续更新中....

### 系统更新说明（v3.0.1）√

1. 修改添加用户密码强度校验
2. 修改系统登录记住密码bug
3. 修复抽奖管理->注册人员管理bug

### 系统更新说明（v3.0.0）√

1. 为了后期更好的维护、扩展，在三层基础上引入抽象工厂


### 系统更新说明（v2.7.0）√

1. 加入Memcached分布式缓存降低对数据库的压力，提高访问速度。（Session有部分修改，[详见代码](http://git.oschina.net/zj_admin/stwhCMS_BootStrap/blob/master/CMS/MemcachedProviders/Session/SessionStateProvider.cs)）
提示：网站部署的时候，服务器必须安装“Memcached服务端”，如果程序部署在虚拟主机上面，虚拟主机必须支持Memcached，否则无法运行。



### 系统更新说明（v2.6.6）√

1. PageBaseVT模板引擎中的GetArticleData标签加入showall属性



### 系统更新说明（v2.6.5）√

1. 优化菜单表、菜单功能表

2. 新增题库系统、抽奖系统、会员系统

3. 新增网站配置表（stwh_config、ProcAddConfig）

4. 商品表新增商品库存字段

5. 优化权限分配显示和使用

6. 优化后台登陆复杂度校验

7. 加入登陆失败处理功能（超过5次，10分钟内不允许操作）

8. 代码优化



### 系统更新说明（v2.5.5）√

1. 优化分页储存过程（修改ProcCustomePage）

2. 修改查询时加入loading插件（页面修改）

3. 修改模板引擎中的GetArticleData函数加入showall属性，通过showall属性来获取指定条件的所有数据

4. 代码优化




### 系统更新说明（v2.5.1）√

1. 增加文章视频链接、来源地址链接（model、dal、页面、数据库表、视图修改）

2. 修复角色权限问题（数据库视图ProcCheckRoleFunction修改）

3. 添加菜单功能显隐（model、dal、页面、数据库表、视图修改）

4. 修复菜单功能管理数据展现问题

5. 修改后台菜单导航和菜单显示（数据库菜单、功能表数据修改）

6. 删除基本信息设置下的返回按钮设置（页面修改、stwh_website修改）

7. 增加题库系统功能（model、dal、bll、页面、数据库表、视图）

5. 代码优化




### 系统更新说明（v2.4.0）√

1. 加入模板静态内容管理功能（model、dal、bll、页面、数据库表添加、模板引擎修改）

2. 修改文章标题加粗、变颜色功能（model、dal、页面、数据库表、视图修改）

3. 解决录入韩语、日语等其它语言在数据库显示乱码问题（数据库表修改（text更新为ntext））

4. 解决编辑器乱码、span、i、b标签被过滤问题

5. 代码优化





### 系统更新说明（v2.3.7）√

1. 修改招标信息管理后台模板（页面修改）

2. 修改招标信息数据库字段（数据库表修改）

3. 修改招标信息逻辑代码（model、dal代码修改）

4. 加入滚动条插件

5. web.config修改、新建用户控件stwhBT_ChildrenPageJS.ascs、每个子页面加入了新建用户控件

6. 加入天气插件

7. 代码优化





### 系统更新说明（v2.3.6）√

1. 加入手机模板功能，可以根据需求定制手机模板

2. 加入移动端预览

3. 修复系统导航路径bug

4. 修复编辑器读取内容时为空的bug

5. 优化系统部分代码


### 系统更新说明（v1.1.5）√
1. UEditor编辑器对在线管理新增删除图片功能,双击图片进行删除（删除图片不可恢复）
修改代码
1. ueditor/dialogs/image/image.js文件的614行
2. Handler/zjimageManager.ashx文件的58行



## 技术交流


* QQ：474197200
* QQ群：61825849（加群时，请输入“码云”）
* E-mail：a474197200@hotmail.com
* 码云：[http://git.oschina.net/zj_admin/stwhCMS_BootStrap](http://git.oschina.net/zj_admin/stwhCMS_BootStrap)

## 程序在线预览
后台地址：http://118.178.187.187/stwhcms/login  
后台用户：admin  
后台密码：123456  
前台（pc）地址：http://118.178.187.187/  
前台（mobile）地址：http://118.178.187.187/mobile/  
前台（shop）地址：http://118.178.187.187/shop/

## 图片预览
![login](https://git.oschina.net/uploads/images/2017/0902/171140_2a685a23_590393.jpeg "login.jpg")
![login_error](https://git.oschina.net/uploads/images/2017/0902/171150_77f753ac_590393.jpeg "login_error.jpg")
![update_pwd](https://git.oschina.net/uploads/images/2017/0902/171211_a2137c25_590393.jpeg "update_pwd.jpg")
![exit](https://git.oschina.net/uploads/images/2017/0902/171229_cf43a558_590393.jpeg "exit.jpg")
![choujiang](https://git.oschina.net/uploads/images/2017/0902/171239_e306e630_590393.jpeg "choujiang.jpg")
![menus](https://git.oschina.net/uploads/images/2017/0902/171251_e36c0ada_590393.jpeg "menus.jpg")
![order](https://git.oschina.net/uploads/images/2017/0902/171300_f26060bf_590393.jpeg "order.jpg")
![setting](https://git.oschina.net/uploads/images/2017/0902/171657_81e9a458_590393.jpeg "setting.jpg")
![tiku](https://git.oschina.net/uploads/images/2017/0902/171318_56cfccb6_590393.jpeg "tiku.jpg")
